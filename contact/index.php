<?php include 'sendemail.php'; ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba de formulario para envio de email</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
  </head>
  <body>

    <!--alert messages start-->
    <?php echo $alert; ?>
    <!--alert messages end-->

    <!--contact section start-->
    <div class="contact-section">
      
      <div class="contact-form">
        <h2>Prueba de formulario para envio de email</h2>
        <form class="contact" action="" method="post">
          <input type="text" name="name" class="text-box" placeholder="Tu Nombre" required>
          <input type="email" name="email" class="text-box" placeholder="Tu Email" required>
          <textarea name="message" rows="5" placeholder="Tu Mensanje" required></textarea>
          <input type="submit" name="submit" class="send-btn" value="Enviar">
        </form>
      </div>
    </div>
    <!--contact section end-->

    <script type="text/javascript">
    if(window.history.replaceState){
      window.history.replaceState(null, null, window.location.href);
    }
    </script>

  </body>
</html>