<?php
include "includes/header.php";

?>
<div class="main_column column" id="main_column">
<h4>Peticiones de Amistad</h4>
<?php
$query  =mysqli_query($con,"SELECT * FROM solicitudes_amigo WHERE usuario_a='$userLoggedIn'");
if(mysqli_num_rows($query)==0)
    echo "No tienes ninguna solicitud de amistad en este momento!";
else{
    while($row =mysqli_fetch_array($query) ){
        $user_from=$row['usuario_de'];
        $user_from_obj=new User($con,$user_from);

        echo $user_from_obj->getFirstAndLastName() ." Te envió una solicitud de amistad!";
        $user_from_friend_array=$user_from_obj->getFriendArray();
        if(isset($_POST['accept_request' . $user_from ])){
            $add_friend_query = mysqli_query($con, "UPDATE usuarios_comunidad SET array_amigo=CONCAT(array_amigo, '$user_from,') WHERE usuario='$userLoggedIn'");
			$add_friend_query = mysqli_query($con, "UPDATE usuarios_comunidad SET array_amigo=CONCAT(array_amigo, '$userLoggedIn,') WHERE usuario='$user_from'");

			$delete_query = mysqli_query($con, "DELETE FROM solicitudes_amigo WHERE usuario_a='$userLoggedIn' AND usuario_de='$user_from'");
			echo "Ahora son Amigos!";
			header("Location: solicitudes.php");
        }
        if(isset($_POST['ignore_request' . $user_from ])){
            $delete_query = mysqli_query($con, "DELETE FROM solicitudes_amigo WHERE usuario_a='$userLoggedIn' AND usuario_de='$user_from'");
            echo "Solicitud Ignorada!";
            header("Location: solicitudes.php"); 
        }
        ?>
        <form action="solicitudes.php" method="post">
    <input type="submit" name="accept_request<?php echo $user_from; ?>" id="accept_button" value="Aceptar">
    <input type="submit" name="ignore_request<?php echo $user_from; ?>" id="ignore_button" value="Ignorar">

</form>
        <?php
    }
}
?>
</div>