<?php
        require 'config/config.php';
        include ("includes/classes/usuario.php");
        include ("includes/classes/publicacion.php");
        include ("includes/classes/notificacion.php");


        if (isset($_SESSION['usuario'])) {
            $userLoggedIn=$_SESSION['usuario'];
            $user_detail_query=mysqli_query($con, "SELECT * FROM usuarios_comunidad WHERE usuario='$userLoggedIn'");
            $user=mysqli_fetch_array($user_detail_query);
        }else {
            header('Location:Registro.php');
        }
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title></title>
    <link rel="stylesheet" href="recursos/css/estilos.css">
</head>
<body>

    <style type="text/css">
    *{
        font-size: 12px;
        font-family: Arial,Helvetica,Sans-serif;
    }
    </style>
   
    <script>
        function toggle(){
            var element=document.getElementById("comment_section");
            if(element.style.display=="block")
                element.style.display="none";
            else
                element.style.display="block";    
        }
    </script>
    <?php
        if(isset($_GET['publicacion_id'])){
            $post_id=$_GET['publicacion_id'];
        }
        $user_query=mysqli_query($con,"SELECT anadido_por, usuario_a FROM publicaciones WHERE id='$post_id' ");
        $row=mysqli_fetch_array($user_query);

        $posted_to=$row['anadido_por'];
        $user_to=$row['usuario_a'];

        if(isset($_POST['postComment'.$post_id])){
            
            $post_body=$_POST['cuerpo_publicacion'];
            $post_body=mysqli_escape_string($con,$post_body);
            $date_time_now=date("Y-m-d H:i:s");

            $body_array = preg_split("/\s+/", $post_body);

			foreach($body_array as $key => $value) {

				if(strpos($value, "www.youtube.com/watch?v=") !== false) {

					$link = preg_split("!&!", $value);
					$value = preg_replace("!watch\?v=!", "embed/", $link[0]);
					$value = "<br><iframe width=\'420\' height=\'315\' src=\'" . $value ."\'></iframe><br>";
					$body_array[$key] = $value;

				}

			}
			$post_body = implode(" ", $body_array);


            $insert_post=mysqli_query($con,"INSERT INTO comentarios VALUES ('','$post_body','$userLoggedIn','$posted_to','$date_time_now','no','$post_id')") ;
            
            if($posted_to != $userLoggedIn){
                $notification =new Notification($con,$userLoggedIn);
                $notification->insertNotification($post_id,$posted_to,"comment");
            } 
            else if($user_to != 'none' && $user_to != $userLoggedIn){
                $notification =new Notification($con,$userLoggedIn);
                $notification->insertNotification($post_id,$user_to,"profile_comment");
            }

            $get_commenters=mysqli_query($con,"SELECT * FROM comentarios WHERE publicacion_id='$post_id'");
            $notified_users=array();
            while ($row = mysqli_fetch_array($get_commenters)){
                if($row['publicado_por'] != $posted_to && $row['publicado_por'] != $user_to
                    && $row['publicado_por'] != $userLoggedIn && !in_array($row['publicado_por'],$notified_users)){
                        $notification =new Notification($con,$userLoggedIn);
                        $notification->insertNotification($post_id,$row['publicado_por'],"comment_non_owner");
                        array_push($notified_users, $row['publicado_por']);
                }
            }
            
            echo "<p>Comentario publicado!</p>" ;        
        }


    ?>
    <form action="comentarios.php?publicacion_id=<?php echo $post_id;  ?>" id="comment_form" name="postComment<?php echo $post_id; ?>" method="POST">
        <textarea name="cuerpo_publicacion" ></textarea>
        <input type="submit" name="postComment<?php echo $post_id; ?>" value="Comentar">
    </form>

    <?php
    $get_comments =mysqli_query($con,"SELECT * FROM comentarios WHERE publicacion_id='$post_id' ORDER BY id ASC");
    $count=mysqli_num_rows($get_comments);

    if($count != 0 ){
            while($comment=mysqli_fetch_array($get_comments)){
                $comment_body=$comment['cuerpo_publicacion'];
                $posted_to=$comment['posteado_por'];
                $posted_by=$comment['publicado_por'];
                $date_added=$comment['fecha_agregada'];
                $removed=$comment['removido'];

                $date_time_now = date("Y-m-d H:i:s");
                $start_date = new DateTime($date_added); 
                $end_date = new DateTime($date_time_now); 
                $interval = $start_date->diff($end_date); 
                if($interval->y >= 1) {
                    if($interval == 1)
                        $time_message =   " Hace $interval->y Años"; 
                    else 
                        $time_message =  " Hace $interval->y Años Atras"; 
                }
                else if ($interval-> m >= 1) {
                    if($interval->d == 0) {
                        $days = " Hace";
                    }
                    else if($interval->d == 1) {
                        $days = $interval->d . " Hace un Dia";
                    }
                    else {
                        $days =   " Hace $interval->d Dias";
                    }
    
    
                    if($interval->m == 1) {
                        $time_message = $interval->m . " Mes". $days;
                    }
                    else {
                        $time_message = $interval->m . " Meses". $days;
                    }
    
                }
                else if($interval->d >= 1) {
                    if($interval->d == 1) {
                        $time_message = "Ayer";
                    }
                    else {
                        $time_message =   " Hace $interval->d dias";
                    }
                }
                else if($interval->h >= 1) {
                    if($interval->h == 1) {
                        $time_message =  " Hace $interval->h Hora";
                    }
                    else {
                        $time_message = "  $interval->h  Horas Atras";
                    }
                }
                else if($interval->i >= 1) {
                    if($interval->i == 1) {
                        $time_message =  " Hace un Minuto";
                    }
                    else {
                        $time_message = "Hace $interval->i Minutos";
                    }
                }
                else {
                    if($interval->s < 30) {
                        $time_message = "Justo Ahora";
                    }
                    else {
                        $time_message =   " Hace $interval->s Segundos";
                    }
                }

                 $user_obj =new User($con,$posted_by);   

                ?>
                <div class="comment_section">
                    <a href="<?php echo $posted_by ?>" target="_parent"><img src="<?php echo $user_obj->getProfilePic(); ?>" title="<?php echo $posted_by ?>" style="float:left;" height="30"></a>
                    <a href="<?php echo $posted_by; ?>" target="_parent"><b><?php echo $user_obj->getFirstAndLastName(); ?></b></a>
                    &nbsp;&nbsp;&nbsp;&nbsp; <?php echo $time_message . "<br>" . $comment_body; ?>
                    <hr>
                </div>
                <?php
            }
    }
        else{
            echo "<center><br><br>No tiene Comentarios !</center>";
        }

                ?>
    
</body>
</html>
