<?php
include("includes/header.php");
include("includes/controlador_formularios/controlador_ajustes.php");
?>
<div class="main_column column">
    <h4>Ajuste de la Cuenta</h4>
    <?php
    echo "<img src='".$user['foto']."' id='small_profile_pic'>";
    ?>
    <br>
    <a href="subir.php">Cambiar foto de perfil</a><br><br><br>

    Modificar los valores y haz click en actualizar datos 
    <?php
    $user_data_query=mysqli_query($con,"SELECT nombre, apellido, email FROM usuarios_comunidad WHERE usuario='$userLoggedIn'");
    $row=mysqli_fetch_array($user_data_query);

    $first_name = $row['nombre'];
    $last_name = $row['apellido'];
    $email = $row['email'];
    ?>
    <form action="ajustes.php" method="post">
        Nombre: <input type="text" name="first_name" value="<?php echo $first_name;?>" id="settings_input"><br>
        Apellido: <input type="text" name="last_name" value="<?php echo $last_name;?>" id="settings_input"><br>
       <input type="hidden" name="email" value="<?php echo $email;?>"><br>

        <?php echo $message; ?>
        <input type="submit" name="update_details" id="save_details" value="Actualizar Datos" class="info setting_submit">

    </form>
    <h4>Cambiar Contraseña</h4>
    <form action="ajustes.php" method="post">
        Contraseña Actual: <input type="password" name="old_password" id="settings_input"><br>
        Nueva Contraseña: <input type="password" name="new_password_1" id="settings_input"><br>
        Confirmar nueva Contraseña: <input type="password" name="new_password_2" id="settings_input"><br>
        
        <?php echo $password_message; ?>
        <input type="submit" name="update_password" id="save_details" value="Actualizar Contraseña" class="info setting_submit">

    </form>
    <h4>Cerrar Cuenta</h4>
    <form action="ajustes.php" method="post">
        <input type="submit" name="close_account" id="close_account" value="Cerrar Cuenta" class="danger setting_submit">
    </form>
</div>