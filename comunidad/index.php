<?php
include ("includes/header.php");


if (isset($_POST['post'])) {
	
		
		$uploadOk = 1;
	$imageName = $_FILES['fileToUpload']['name'];
	$errorMessage = "";

	if($imageName != "") {
		$targetDir="recursos/img/publicaciones/";
		$imageName = $targetDir . uniqid() . basename($imageName);
		$imageFileType = pathinfo($imageName, PATHINFO_EXTENSION);

		if($_FILES['fileToUpload']['size'] > 10000000) {
			$errorMessage = "Lo siento, tu imagen es demasiado grande";
			$uploadOk = 0;
		}

		if(strtolower($imageFileType) != "jpeg" && strtolower($imageFileType) != "png" && strtolower($imageFileType) != "jpg") {
			$errorMessage = "Lo sentimos, solo se permiten archivos jpeg, jpg y png";
			$uploadOk = 0;
		}

		if($uploadOk) {
			if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $imageName)) {
				
			}
			else {
				
				$uploadOk = 0;
			}
		}	

	}
	if($uploadOk){
    $post=new Post($con,$userLoggedIn);
    $post->submitPost($_POST['post_text'],'none',$imageName);
	header("Location: index.php");
	}
	else{
		echo "<div style='text-align: center;' class='alert alert-danger'>
				$errorMessage
			  </div>";
	}
}
//session_destroy();
?>
<div class="user_details column">
    <a href="<?php echo $userLoggedIn; ?>"><img src="<?php echo $user['foto']; ?>" ></a>
    <div class="user_detail_left_right">
        <a href="<?php echo $userLoggedIn; ?>">
        <?php echo $user['nombre'] . " " . $user['apellido'];?>
        </a>
        <br>
        <?php echo "Publicaciones: ".$user['num_posts']."<br>";
            echo "Likes: " .$user['num_likes']; 

        ?>
    
    </div>
</div>
    <div class="main_column column">
        <form class="post_form" action="index.php" method="POST" enctype="multipart/form-data">
			<label for="">Si Desea sube una imagen</label>
			<input type="file" name="fileToUpload" id="fileToUpload">
            <textarea name="post_text" id="post_text" placeholder="Tienes algo que decir?"></textarea>
            <input type="submit" name="post" id="post_button" value="Publicar"></input>
            <hr>
        </form>
      
		<div class="posts_area"></div>
        <img id="loading" src="recursos/img/icons/loading.gif" >
    </div>
    <script>
		var userLoggedIn = '<?php echo $userLoggedIn; ?>';

		$(document).ready(function() {

			$('#loading').show();

			 
			$.ajax({
				url: "includes/handlers/carga_publicacion_ajax.php",
				type: "POST",
				data: "page=1&userLoggedIn=" + userLoggedIn,
				cache:false,

				success: function(data) {
					$('#loading').hide();
					$('.posts_area').html(data);
				}
			});

			$(window).scroll(function() {
				var height = $('.posts_area').height();
				var scroll_top = $(this).scrollTop();
				var page = $('.posts_area').find('.nextPage').val();
				var noMorePosts = $('.posts_area').find('.noMorePosts').val();

				if ((document.body.scrollHeight == document.body.scrollTop + window.innerHeight) && noMorePosts == 'false') {
                    $('#loading').show();
                    alert("hello");

					var ajaxReq = $.ajax({
						url: "includes/handlers/carga_publicacion_ajax.php",
						type: "POST",
						data: "page=" + page + "&userLoggedIn=" + userLoggedIn,
						cache:false,

						success: function(response) {
							$('.posts_area').find('.nextPage').remove();  
							$('.posts_area').find('.noMorePosts').remove(); 
							$('#loading').hide();
							$('.posts_area').append(response);
						}
					});

				} 

				return false;

			}); 


		});

		</script>

    </div>
</body>
</html>