<?php
class Notification {
	private $user_obj;
	private $con;

	public function __construct($con, $user){
		$this->con = $con;
		$this->user_obj = new User($con, $user);
    }
    public function getUnreadNumber() {
		$userLoggedIn = $this->user_obj->getUsername();
		$query = mysqli_query($this->con, "SELECT * FROM notificaciones WHERE visto='no' AND usuario_a='$userLoggedIn'");
		return mysqli_num_rows($query);
	}


    public function getNotifications($data, $limit){
        $page=$data['page'];
        $userLoggedIn=$this->user_obj->getUsername();
        $return_string="";
        

        if($page==1){
          $start=0;}
        else{
            $start=($page-1)*$limit;
        }
        
        $set_viewed_quey=mysqli_query($this->con,"UPDATE notificaciones SET visto='yes' WHERE usuario_a='$userLoggedIn'");

        $query=mysqli_query($this->con,"SELECT * FROM notificaciones WHERE usuario_a='$userLoggedIn'  ORDER BY id DESC ");

        if(mysqli_num_rows($query)==0){
            echo "No tienes notificaciones!";
            return;
        }

        $num_iterations=0;
        $count=1;

        while($row=mysqli_fetch_array($query)){

            if($num_iterations++ < $start)
                continue;
            if($count>$limit)
                break;
            else
                $count++;
                
            $user_from = $row['usuario_de'];
            $user_data_query=mysqli_query($this->con,"SELECT * FROM usuarios_comunidad WHERE usuario='$user_from'");
            $user_data=mysqli_fetch_array($user_data_query);

            $date_time_now = date("Y-m-d H:i:s");
			$start_date = new DateTime($row['fecha']); 
			$end_date = new DateTime($date_time_now); 
			$interval = $start_date->diff($end_date); 
			if($interval->y >= 2) {
				if($interval == 2)
					$time_message =   " Hace $interval->y Años"; 
				else 
					$time_message =  " Hace $interval->y Años Atras"; 
			}
			else if ($interval-> m >= 1) {
				if($interval->d == 0) {
					$days = " Hace";
				}
				else if($interval->d == 1) {
					$days = $interval->d . " Hace un Dia";
				}
				else {
					$days =   " Hace $interval->d Dias";
				}


				if($interval->m == 1) {
					$time_message = $interval->m . " Mes". $days;
				}
				else {
					$time_message = $interval->m . " Meses". $days;
				}

			}
			else if($interval->d >= 1) {
				if($interval->d == 1) {
					$time_message = "Ayer";
				}
				else {
					$time_message =   " Hace $interval->d dias";
				}
			}
			else if($interval->h >= 1) {
				if($interval->h == 1) {
					$time_message =  " Hace $interval->h Hora";
				}
				else {
					$time_message = "  $interval->h  Horas Atras";
				}
			}
			else if($interval->i >= 1) {
				if($interval->i == 1) {
					$time_message =  " Hace un Minuto";
				}
				else {
					$time_message = "Hace $interval->i Minutos";
				}
			}
			else {
				if($interval->s < 30) {
					$time_message = "Justo Ahora";
				}
				else {
					$time_message =   " Hace $interval->s Segundos";
				}
			}
            
            

            $opened=$row['abierto'];
            $style=($row['abierto']  == 'no' ) ? "background-color: #DDEDFF;" : "";
            

            $return_string .="<a href='".$row['link']."'>
                                <div class='resultDisplay resultDisplayNotification' style='" .$style. "'>
                            <div class='notificationsProfilePic'>
                                <img src='".$user_data['foto']."'>
                            </div>
                            <p class='timestamp_smaller' id='grey'>" .$time_message. "</p>" .$row['mensaje']. "     
                            </div>
                            </a>";
        }

        if($count>$limit)
            $return_string .="<input type='hidden' class='nextPageDropDownData' value='".($page+1)."'><input type='hidden' class='noMoreDropdownData' value='false'>";
        else
            $return_string .="<input type='hidden' class='noMoreDropdownData' value='true'><p style='text-align:center;'>No tienes mas notificaciones!</p>";


        return $return_string;
    }


    public function insertNotification($post_id, $user_to, $type){
        $userLoggedIn=$this->user_obj->getUsername();
        $userLoggedInName   =$this->user_obj->getFirstAndLastName();
        $date_time=date("Y-m-d H:i:s");
        switch($type){
            case 'comment':
                $message =$userLoggedInName  . " comentó tu publicación";
            break;
            case 'like':
                $message=$userLoggedInName ." Le dio like a tu publicacion";
            break;
            case 'profile_post':
                $message=$userLoggedInName ." Publico en tu perfil";
            break;
            case 'comment_non_owner':
                $message=$userLoggedInName ." comentó en una publicación que usted comentó";
            break;
            case 'profile_comment':
                $message=$userLoggedInName ." Escribio en su perfil";
            break;
        }
        $link="publicaciones.php?id=" .$post_id;
        $insert_query=mysqli_query($this->con, "INSERT INTO notificaciones VALUES('','$user_to','$userLoggedIn','$message','$link','$date_time','no','no')");
    }



}
?>