<?php
class Post {
	private $user_obj;
	private $con;

	public function __construct($con, $user){
		$this->con = $con;
		$this->user_obj = new User($con, $user);
	}

	public function submitPost($body, $user_to, $imageName)  {
		$body = strip_tags($body);
        $body = mysqli_real_escape_string($this->con, $body);
		$check_empty = preg_replace('/\s+/', '', $body); 
      
		if($check_empty != "" ||  $imageName!="") {
			$body_array = preg_split("/\s+/", $body);

			foreach($body_array as $key => $value) {

				if(strpos($value, "www.youtube.com/watch?v=") !== false) {

					$link = preg_split("!&!", $value);
					$value = preg_replace("!watch\?v=!", "embed/", $link[0]);
					$value = "<br><iframe width=\'420\' height=\'315\' src=\'" . $value ."\'></iframe><br>";
					$body_array[$key] = $value;

				}

			}
			$body = implode(" ", $body_array);
			
			$date_added = date("Y-m-d H:i:s");
			
			$added_by = $this->user_obj->getUsername();

			
			if($user_to == $added_by) {
				$user_to = "none";
			}

			
			$query = mysqli_query($this->con, "INSERT INTO publicaciones VALUES('', '$body', '$added_by', '$user_to', '$date_added', 'no', 'no', '0','$imageName')");
			$returned_id = mysqli_insert_id($this->con);


			if($user_to != 'none'){
				$notification =new Notification($this->con,$added_by);
				$notification->insertNotification($returned_id,$user_to,"profile_post");
			}
			 
			$num_posts = $this->user_obj->getNumPosts();
			$num_posts++;
			$update_query = mysqli_query($this->con, "UPDATE usuarios_comunidad SET num_posts='$num_posts' WHERE usuario='$added_by'");

		}
	}

	public function loadPostsFriends($data, $limit){
		$page=$data['page'];
		$userLoggedIn= $this->user_obj->getUsername();

		if($page==1)
			$start=0;
		else
			$start=($page-1) * $limit;	
		

		$str="";
		$data_query=mysqli_query($this->con,"SELECT * FROM publicaciones WHERE eliminado='no' ORDER BY id DESC");
	
		if (mysqli_num_rows($data_query)>0) {
			$num_iterations=0;
			$count=1;
		
		while($row=mysqli_fetch_array($data_query)){
			$id=$row['id'];
			$body=$row['cuerpo'];
			$added_by=$row['anadido_por'];
			$date_time=$row['fecha_agregada'];
			$imagePath=$row['image'];

			if ($row['usuario_a']=="none") {
					$user_to="";
			}else{
				$user_to_obj= new User($this->con, $row['usuario_a']);
				$user_to_name=$user_to_obj->getFirstAndLastName();
				$user_to=" A <a href='" . $row['usuario_a'] ."'> ".$user_to_name ."</a>";
			}

			$added_by_obj =new User($this->con,$added_by);
			if ($added_by_obj->isClosed()) {
				continue;	
			}
			
			$user_logged_obj = new User($this->con, $userLoggedIn);
				if($user_logged_obj->isFriend($added_by)){

			if($num_iterations++ < $start){
				continue;
			}

			if ($count>$limit) {
			break;
			}	
			else{
				$count++;
			}
			if($userLoggedIn == $added_by)
				$delete_button = "<button class='delete_button btn-danger' id='post$id'>X</button>";
			else 
						$delete_button = "";

			$user_details_query=mysqli_query($this->con, "SELECT nombre, apellido, foto FROM usuarios_comunidad WHERE usuario='$added_by'");
			$user_row=mysqli_fetch_array($user_details_query);
			$first_name = $user_row['nombre'];
			$last_name = $user_row['apellido'];
			$profile_pic = $user_row['foto'];

			?>
			<script>
				function toggle<?php echo $id; ?>(){

					var target= $(event.target);
					if(!target.is("a")){
						var element=document.getElementById("toggleComment<?php echo $id; ?>");
					if(element.style.display=="block")
						element.style.display="none";
					else
						element.style.display="block"; 
					}
		 
                 }
			</script>
			<?php
			
			$comments_check=mysqli_query($this->con,"SELECT * FROM comentarios WHERE publicacion_id='$id'");
			$comments_check_num=mysqli_num_rows($comments_check);	 

			$date_time_now = date("Y-m-d H:i:s");
			$start_date = new DateTime($date_time); 
			$end_date = new DateTime($date_time_now); 
			$interval = $start_date->diff($end_date); 
			if($interval->y >= 2) {
				if($interval == 2)
					$time_message =   " Hace $interval->y Años"; 
				else 
					$time_message =  " Hace $interval->y Años Atras"; 
			}
			else if ($interval-> m >= 1) {
				if($interval->d == 0) {
					$days = " Hace";
				}
				else if($interval->d == 1) {
					$days = $interval->d . " Hace un Dia";
				}
				else {
					$days =   " Hace $interval->d Dias";
				}


				if($interval->m == 1) {
					$time_message = $interval->m . " Mes". $days;
				}
				else {
					$time_message = $interval->m . " Meses". $days;
				}

			}
			else if($interval->d >= 1) {
				if($interval->d == 1) {
					$time_message = "Ayer";
				}
				else {
					$time_message =   " Hace $interval->d dias";
				}
			}
			else if($interval->h >= 1) {
				if($interval->h == 1) {
					$time_message =  " Hace $interval->h Hora";
				}
				else {
					$time_message = "  $interval->h  Horas Atras";
				}
			}
			else if($interval->i >= 1) {
				if($interval->i == 1) {
					$time_message =  " Hace un Minuto";
				}
				else {
					$time_message = "Hace $interval->i Minutos";
				}
			}
			else {
				if($interval->s < 30) {
					$time_message = "Justo Ahora";
				}
				else {
					$time_message =   " Hace $interval->s Segundos";
				}
			}

			if($imagePath != ""){
				$imageDiv="<div class='postedImage'>
							<img src='$imagePath'>
							</div>";
			}
			else{
				$imageDiv="";
			}

			$str .= "<div class='status_post' onClick='javascript:toggle$id()'>
								<div class='post_profile_pic'>
									<img src='$profile_pic' width='50'>
								</div>

								<div class='posted_by' style='color:#ACACAC;'>
									<a href='$added_by'> $first_name $last_name </a> $user_to &nbsp;&nbsp;&nbsp;&nbsp;$time_message
									$delete_button
									</div>
								<div id='post_body'>
									$body
									<br>
									$imageDiv
									<br>
									<br>
								</div>

								<div class='newsfeedPostOptions'>
									Comentarios($comments_check_num)&nbsp;&nbsp;&nbsp;
									<iframe src='like.php?publicacion_id=$id' scrolling='no'></iframe>
								</div>

							</div>
							<div class='post_comment' id='toggleComment$id' style='display:none;'>
								<iframe src='comentarios.php?publicacion_id	=$id' id='comment_iframe' frameborder='0'></iframe>
							</div>	
							<hr>";
						}
						?>
		
		<script>

		$(document).ready(function() {

			$('#post<?php echo $id; ?>').on('click', function() {
				bootbox.confirm("Desea borrar esta publicacion?", function(result) {

					$.post("includes/controlador_formularios/borrar_publicacion.php?publicacion_id=<?php echo $id; ?>", {result:result});

					if(result)
						location.reload();

				});
			});


		});

</script>
		<?php

		}//fin del while
		if ($count > $limit) {
			$str .="<input type='hidden' class='nextPage' value='" . ($page + 1) . "'>
					<input type='hidden' class='noMorePosts' value='false'>";}
		else{		
		$str .= "<input type='hidden' class='noMorePosts' value='true'><p style='text-align: centre;'>No hay mas publicaciones! </p>";
		
	}		
		
	}
		echo $str;
	}

	

	public function loadProfilePosts($data, $limit) {
		$page = $data['page']; 
		$profileUser = $data['profileUsername'];
		$userLoggedIn = $this->user_obj->getUsername();
	
		if($page == 1) 
			$start = 0;
		else 
			$start = ($page - 1) * $limit;
	
	
		$str = ""; 
		$data_query = mysqli_query($this->con, "SELECT * FROM publicaciones WHERE eliminado='no' AND ((anadido_por='$profileUser' AND usuario_a='none') OR usuario_a='$profileUser')  ORDER BY id DESC");
	
		if (mysqli_num_rows($data_query)>0) {
			$num_iterations=0;
			$count=1;
		
		while($row=mysqli_fetch_array($data_query)){
			$id=$row['id'];
			$body=$row['cuerpo'];
			$added_by=$row['anadido_por'];
			$date_time=$row['fecha_agregada'];
			$imagePath=$row['image'];

			if($num_iterations++ < $start){
				continue;
			}

			if ($count>$limit) {
			break;
			}	
			else{
				$count++;
			}
			if($userLoggedIn == $added_by)
				$delete_button = "<button class='delete_button btn-danger' id='post$id'>X</button>";
			else 
						$delete_button = "";

			$user_details_query=mysqli_query($this->con, "SELECT nombre, apellido, foto FROM usuarios_comunidad WHERE usuario='$added_by'");
			$user_row=mysqli_fetch_array($user_details_query);
			$first_name = $user_row['nombre'];
			$last_name = $user_row['apellido'];
			$profile_pic = $user_row['foto'];

			?>
			<script>
				function toggle<?php echo $id; ?>(){

					var target= $(event.target);
					if(!target.is("a")){
						var element=document.getElementById("toggleComment<?php echo $id; ?>");
					if(element.style.display=="block")
						element.style.display="none";
					else
						element.style.display="block"; 
					}
		 
                 }
			</script>
			<?php
			
			$comments_check=mysqli_query($this->con,"SELECT * FROM comentarios WHERE publicacion_id='$id'");
			$comments_check_num=mysqli_num_rows($comments_check);	 

			$date_time_now = date("Y-m-d H:i:s");
			$start_date = new DateTime($date_time); 
			$end_date = new DateTime($date_time_now); 
			$interval = $start_date->diff($end_date); 
			if($interval->y >= 2) {
				if($interval == 2)
					$time_message =   " Hace $interval->y Años"; 
				else 
					$time_message =  " Hace $interval->y Años Atras"; 
			}
			else if ($interval-> m >= 1) {
				if($interval->d == 0) {
					$days = " Hace";
				}
				else if($interval->d == 1) {
					$days = $interval->d . " Hace un Dia";
				}
				else {
					$days =   " Hace $interval->d Dias";
				}


				if($interval->m == 1) {
					$time_message = $interval->m . " Mes". $days;
				}
				else {
					$time_message = $interval->m . " Meses". $days;
				}

			}
			else if($interval->d >= 1) {
				if($interval->d == 1) {
					$time_message = "Ayer";
				}
				else {
					$time_message =   " Hace $interval->d dias";
				}
			}
			else if($interval->h >= 1) {
				if($interval->h == 1) {
					$time_message =  " Hace $interval->h Hora";
				}
				else {
					$time_message = "  $interval->h  Horas Atras";
				}
			}
			else if($interval->i >= 1) {
				if($interval->i == 1) {
					$time_message =  " Hace un Minuto";
				}
				else {
					$time_message = "Hace $interval->i Minutos";
				}
			}
			else {
				if($interval->s < 30) {
					$time_message = "Justo Ahora";
				}
				else {
					$time_message =   " Hace $interval->s Segundos";
				}
			}
			if($imagePath != ""){
				$imageDiv="<div class='postedImage'>
							<img src='$imagePath'>
							</div>";
			}
			else{
				$imageDiv="";
			}


			$str .= "<div class='status_post' onClick='javascript:toggle$id()'>
								<div class='post_profile_pic'>
									<img src='$profile_pic' width='50'>
								</div>

								<div class='posted_by' style='color:#ACACAC;'>
									<a href='$added_by'> $first_name $last_name </a>  &nbsp;&nbsp;&nbsp;&nbsp;$time_message
									$delete_button
									</div>
								<div id='post_body'>
									$body
									<br>
									$imageDiv
									<br>
									<br>
								</div>

								<div class='newsfeedPostOptions'>
									Comentarios($comments_check_num)&nbsp;&nbsp;&nbsp;
									<iframe src='like.php?publicacion_id=$id' scrolling='no'></iframe>
								</div>

							</div>
							<div class='post_comment' id='toggleComment$id' style='display:none;'>
								<iframe src='comentarios.php?publicacion_id	=$id' id='comment_iframe' frameborder='0'></iframe>
							</div>	
							<hr>";
						
						?>
		
		<script>

		$(document).ready(function() {

			$('#post<?php echo $id; ?>').on('click', function() {
				bootbox.confirm("Desea borrar esta publicacion?", function(result) {

					$.post("includes/controlador_formularios/borrar_publicacion.php?publicacion_id=<?php echo $id; ?>", {result:result});

					if(result)
						location.reload();

				});
			});


		});

</script>
		<?php

		}//fin del while
		if ($count > $limit) {
			$str .="<input type='hidden' class='nextPage' value='" . ($page + 1) . "'>
					<input type='hidden' class='noMorePosts' value='false'>";}
		else{		
		$str .= "<input type='hidden' class='noMorePosts' value='true'><p style='text-align: centre;'>No hay mas publicaciones! </p>";
		
	}		
		
	}
		echo $str;
	}


	public function getSinglePost($post_id){
		$userLoggedIn= $this->user_obj->getUsername();

		$opened_query=mysqli_query($this->con,"UPDATE notificaciones SET abierto='yes' WHERE usuario_a='$userLoggedIn' AND link LIKE  '%=$post_id'");
		
		$str="";
		$data_query=mysqli_query($this->con,"SELECT * FROM publicaciones WHERE eliminado='no' AND id='$post_id'");
	
		if (mysqli_num_rows($data_query)>0) {
		
		
		$row=mysqli_fetch_array($data_query);
			$id=$row['id'];
			$body=$row['cuerpo'];
			$added_by=$row['anadido_por'];
			$date_time=$row['fecha_agregada'];

			if ($row['usuario_a']=="none") {
					$user_to="";
			}else{
				$user_to_obj= new User($this->con, $row['usuario_a']);
				$user_to_name=$user_to_obj->getFirstAndLastName();
				$user_to=" A <a href='" . $row['usuario_a'] ."'> ".$user_to_name ."</a>";
			}

			$added_by_obj =new User($this->con,$added_by);
			if ($added_by_obj->isClosed()) {
				return;	
			}
			
			$user_logged_obj = new User($this->con, $userLoggedIn);
				if($user_logged_obj->isFriend($added_by)){

			
			if($userLoggedIn == $added_by)
				$delete_button = "<button class='delete_button btn-danger' id='post$id'>X</button>";
			else 
						$delete_button = "";

			$user_details_query=mysqli_query($this->con, "SELECT nombre, apellido, foto FROM usuarios_comunidad WHERE usuario='$added_by'");
			$user_row=mysqli_fetch_array($user_details_query);
			$first_name = $user_row['nombre'];
			$last_name = $user_row['apellido'];
			$profile_pic = $user_row['foto'];

			?>
			<script>
				function toggle<?php echo $id; ?>(){

					var target= $(event.target);
					if(!target.is("a")){
						var element=document.getElementById("toggleComment<?php echo $id; ?>");
					if(element.style.display=="block")
						element.style.display="none";
					else
						element.style.display="block"; 
					}
		 
                 }
			</script>
			<?php
			
			$comments_check=mysqli_query($this->con,"SELECT * FROM comentarios WHERE publicacion_id='$id'");
			$comments_check_num=mysqli_num_rows($comments_check);	 

			$date_time_now = date("Y-m-d H:i:s");
			$start_date = new DateTime($date_time); 
			$end_date = new DateTime($date_time_now); 
			$interval = $start_date->diff($end_date); 
			if($interval->y >= 1) {
				if($interval == 1)
					$time_message =   " Hace $interval->y Años"; 
				else 
					$time_message =  " Hace $interval->y Años Atras"; 
			}
			else if ($interval-> m >= 1) {
				if($interval->d == 0) {
					$days = " Hace";
				}
				else if($interval->d == 1) {
					$days = $interval->d . " Hace un Dia";
				}
				else {
					$days =   " Hace $interval->d Dias";
				}


				if($interval->m == 1) {
					$time_message = $interval->m . " Mes". $days;
				}
				else {
					$time_message = $interval->m . " Meses". $days;
				}

			}
			else if($interval->d >= 1) {
				if($interval->d == 1) {
					$time_message = "Ayer";
				}
				else {
					$time_message =   " Hace $interval->d dias";
				}
			}
			else if($interval->h >= 1) {
				if($interval->h == 1) {
					$time_message =  " Hace $interval->h Hora";
				}
				else {
					$time_message = "  $interval->h  Horas Atras";
				}
			}
			else if($interval->i >= 1) {
				if($interval->i == 1) {
					$time_message =  " Hace un Minuto";
				}
				else {
					$time_message = "Hace $interval->i Minutos";
				}
			}
			else {
				if($interval->s < 30) {
					$time_message = "Justo Ahora";
				}
				else {
					$time_message =   " Hace $interval->s Segundos";
				}
			}

			$str .= "<div class='status_post' onClick='javascript:toggle$id()'>
								<div class='post_profile_pic'>
									<img src='$profile_pic' width='50'>
								</div>

								<div class='posted_by' style='color:#ACACAC;'>
									<a href='$added_by'> $first_name $last_name </a> $user_to &nbsp;&nbsp;&nbsp;&nbsp;$time_message
									$delete_button
									</div>
								<div id='post_body'>
									$body
									<br>
									<br>
									<br>
								</div>

								<div class='newsfeedPostOptions'>
									Comentarios($comments_check_num)&nbsp;&nbsp;&nbsp;
									<iframe src='like.php?publicacion_id=$id' scrolling='no'></iframe>
								</div>

							</div>
							<div class='post_comment' id='toggleComment$id' style='display:none;'>
								<iframe src='comentarios.php?publicacion_id	=$id' id='comment_iframe' frameborder='0'></iframe>
							</div>	
							<hr>";
						
						?>
		
		<script>

		$(document).ready(function() {

			$('#post<?php echo $id; ?>').on('click', function() {
				bootbox.confirm("Desea borrar esta publicacion?", function(result) {

					$.post("includes/controlador_formularios/borrar_publicacion.php?publicacion_id=<?php echo $id; ?>", {result:result});

					if(result)
						location.reload();

				});
			});


		});

</script>
		<?php		
		}
		else{
			echo "<p>Tu no eres amigo de este usuario.</p>";
			return;
		}
	}
	else{
		echo "<p>No se encontró ninguna publicación. si hizo clic en un enlace, es posible que esté roto</p>";
			return;
	}
		echo $str;
	}

}

?>