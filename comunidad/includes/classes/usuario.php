<?php
class User {
	private $user;
	private $con;

	public function __construct($con, $user){
		$this->con = $con;
		$user_details_query = mysqli_query($con, "SELECT * FROM usuarios_comunidad WHERE usuario='$user'");
		$this->user = mysqli_fetch_array($user_details_query);
	}

	public function getUsername() {
		return $this->user['usuario'];
	}

	public function getNumberOfFriendRequest(){
		$username = $this->user['usuario'];
		$query = mysqli_query($this->con, "SELECT * FROM solicitudes_amigo WHERE usuario_a='$username'");
		return mysqli_num_rows($query);
		
	}

	public function getNumPosts() {
		$username = $this->user['usuario'];
		$query = mysqli_query($this->con, "SELECT num_posts FROM usuarios_comunidad WHERE usuario='$username'");
		$row = mysqli_fetch_array($query);
		return $row['num_posts'];
	}

	public function getFirstAndLastName() {
		$username = $this->user['usuario'];
		$query = mysqli_query($this->con, "SELECT nombre, apellido FROM usuarios_comunidad WHERE usuario='$username'");
		$row = mysqli_fetch_array($query);
		return $row['nombre'] . " " . $row['apellido'];
	}

	public function getProfilePic() {
		$username = $this->user['usuario'];
		$query = mysqli_query($this->con, "SELECT foto FROM usuarios_comunidad WHERE usuario='$username'");
		$row = mysqli_fetch_array($query);
		return $row['foto'] ;
	}
	public function getFriendArray() {
		$username = $this->user['usuario'];
		$query = mysqli_query($this->con, "SELECT array_amigo FROM usuarios_comunidad WHERE usuario='$username'");
		$row = mysqli_fetch_array($query);
		return $row['array_amigo'] ;
	}

	public function isClosed(){
		$username=$this->user['usuario'];
		$query=mysqli_query($this->con,"SELECT cerrar_usuario FROM usuarios_comunidad WHERE usuario='$username'");
		$row=mysqli_fetch_array($query);

		if ($row['cerrar_usuario']=='si') 
			return true;
		else
			return false;
	}

	public function isFriend($username_to_check) {
		$usernameComma="," . $username_to_check . ",";
		if((strstr($this->user['array_amigo'],$usernameComma)|| $username_to_check == $this->user['usuario'])){
			return true;
		}
		else{
			return false;
		}
	}
	public function didReceiveRequest($user_from){
		$user_to=$this->user['usuario'];
		$check_request_query=mysqli_query($this->con,"SELECT * FROM solicitudes_amigo WHERE usuario_a='$user_to' AND usuario_de='$user_from'");
		if(mysqli_num_rows($check_request_query)>0){
			return true;
		}
		else{
			return false;	
		}
	}

	public function didSendRequest($user_to){
		$user_from=$this->user['usuario'];
		$check_request_query=mysqli_query($this->con,"SELECT * FROM solicitudes_amigo WHERE usuario_a='$user_to' AND usuario_de='$user_from'");
		if(mysqli_num_rows($check_request_query)>0){
			return true;
		}
		else{
			return false;	
		}
	}

	public function removeFriend($user_to_remove){
		$logged_in_user = $this->user['usuario'];

		$query = mysqli_query($this->con, "SELECT array_amigo FROM usuarios_comunidad WHERE usuario='$user_to_remove'");
		$row = mysqli_fetch_array($query);
		$friend_array_username = $row['array_amigo'];

		$new_friend_array = str_replace($user_to_remove . ",", "", $this->user['array_amigo']);
		$remove_friend = mysqli_query($this->con, "UPDATE usuarios_comunidad SET array_amigo='$new_friend_array' WHERE usuario='$logged_in_user'");

		$new_friend_array = str_replace($this->user['usuario'] . ",", "", $friend_array_username);
		$remove_friend = mysqli_query($this->con, "UPDATE usuarios_comunidad SET array_amigo='$new_friend_array' WHERE usuario='$user_to_remove'");
	}
	public function sendRequest($user_to) {
		$user_from = $this->user['usuario'];
		$query = mysqli_query($this->con, "INSERT INTO solicitudes_amigo VALUES('', '$user_to', '$user_from')");
	}

	public function getMutualFriends($user_to_check) {
		$mutualFriends = 0;
		$user_array = $this->user['array_amigo'];
		$user_array_explode = explode(",", $user_array);

		$query = mysqli_query($this->con, "SELECT array_amigo FROM usuarios_comunidad WHERE usuario='$user_to_check'");
		$row = mysqli_fetch_array($query);
		$user_to_check_array = $row['array_amigo'];
		$user_to_check_array_explode = explode(",", $user_to_check_array);

		foreach($user_array_explode as $i) {

			foreach($user_to_check_array_explode as $j) {

				if($i == $j && $i != "") {
					$mutualFriends++;
				}
			}
		}
		return $mutualFriends;

	}
		
}

?>