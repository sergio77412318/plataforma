<?php
class Message {
	private $user_obj;
	private $con;

	public function __construct($con, $user){
		$this->con = $con;
		$this->user_obj = new User($con, $user);
	}

    public function getMostRecentUser(){
        $userLoggedIn=$this->user_obj->getUsername();
        $query = mysqli_query($this->con, "SELECT usuario_a, usuario_de FROM mensajes_com WHERE usuario_a='$userLoggedIn' OR usuario_de='$userLoggedIn' ORDER BY id DESC LIMIT 1");

        if(mysqli_num_rows($query)==0)
            return false;
        $row = mysqli_fetch_array($query);
        $user_to=$row['usuario_a'];
        $user_from=$row['usuario_de'];
        if ($user_to!=$userLoggedIn)
            return $user_to;
        else
            return $user_from;

    }
    public function sendMessage($user_to, $body , $date) {
        if($body!=""){
            $userLoggedIn=$this->user_obj->getUsername();
            $query=mysqli_query($this->con,"INSERT INTO mensajes_com VALUES('','$user_to','$userLoggedIn','$body','$date','no','no','no')");

        }
    }

    public function getMessages($otherUser){
        $userLoggedIn=$this->user_obj->getUsername();
        $data="";
        $query=mysqli_query($this->con, "UPDATE mensajes_com SET abrio='yes' WHERE usuario_a='$userLoggedIn' AND usuario_de='$otherUser'  ");

        $get_messages_query=mysqli_query($this->con,"SELECT * FROM mensajes_com WHERE (usuario_a='$userLoggedIn' AND usuario_de='$otherUser')OR
        (usuario_de='$userLoggedIn'AND usuario_a='$otherUser' )");
        
        while($row=mysqli_fetch_array($get_messages_query)){
            $user_to=$row['usuario_a'];
            $user_from=$row['usuario_de'];
            $body=$row['cuerpo_mensaje'];

            $div_top=($user_to==$userLoggedIn)?"<div class='message' id='green'>" : "<div class='message' id='blue'>";
            $data=$data . $div_top . $body . "</div><br><br>";


        }
        return $data;
    }


    public function getLatestMessage($userLoggedIn,$user2){
        $details_array=array(); 
        $query=mysqli_query($this->con,"SELECT cuerpo_mensaje,usuario_a,fecha FROM mensajes_com WHERE (usuario_a='$userLoggedIn' AND usuario_de='$user2')OR 
        (usuario_a='$user2' AND usuario_de='$userLoggedIn') ORDER BY id DESC LIMIT 1");
        $row=mysqli_fetch_array($query);
        $sent_by=($row['usuario_a']==$userLoggedIn)? "Ellos dijeron: " : "Tu dijiste: ";

        $date_time_now = date("Y-m-d H:i:s");
        $start_date = new DateTime($row['fecha']); 
        $end_date = new DateTime($date_time_now); 
        $interval = $start_date->diff($end_date); 
        if($interval->y >= 2) {
            if($interval == 2)
                $time_message =   " Hace $interval->y Años"; 
            else 
                $time_message =  " Hace $interval->y Años Atras"; 
        }
        else if ($interval-> m >= 1) {
            if($interval->d == 0) {
                $days = " Hace";
            }
            else if($interval->d == 1) {
                $days = $interval->d . " Hace un Dia";
            }
            else {
                $days =   " Hace $interval->d Dias";
            }


            if($interval->m == 1) {
                $time_message = $interval->m . " Mes". $days;
            }
            else {
                $time_message = $interval->m . " Meses". $days;
            }

        }
        else if($interval->d >= 1) {
            if($interval->d == 1) {
                $time_message = "Ayer";
            }
            else {
                $time_message =   " Hace $interval->d dias";
            }
        }
        else if($interval->h >= 1) {
            if($interval->h == 1) {
                $time_message =  " Hace $interval->h Hora";
            }
            else {
                $time_message = "  $interval->h  Horas Atras";
            }
        }
        else if($interval->i >= 1) {
            if($interval->i == 1) {
                $time_message =  " Hace un Minuto";
            }
            else {
                $time_message = "Hace $interval->i Minutos";
            }
        }
        else {
            if($interval->s < 30) {
                $time_message = "Justo Ahora";
            }
            else {
                $time_message =   " Hace $interval->s Segundos";
            }
        }   

        array_push($details_array,$sent_by);  
        array_push($details_array,$row['cuerpo_mensaje']);  
        array_push($details_array,$time_message);
        return $details_array;  

    }

    public function getConvos(){
        $userLoggedIn=$this->user_obj->getUsername();
        $return_string="";
        $convos=array();

        $query=mysqli_query($this->con,"SELECT usuario_a, usuario_de FROM mensajes_com WHERE usuario_a='$userLoggedIn' OR usuario_de='$userLoggedIn' ORDER BY id DESC ");

        while($row=mysqli_fetch_array($query)){
            $user_to_push=($row["usuario_a"]!=$userLoggedIn)? $row['usuario_a'] : $row['usuario_de'];
            if(!in_array($user_to_push,$convos)){
                array_push($convos,$user_to_push);
            }
        }
        foreach($convos as $username){
            $user_found_obj= new User($this->con,$username);
            $latest_message_details=$this->getLatestMessage($userLoggedIn,$username);

            $dots=(strlen($latest_message_details[1])>=12) ? "..." : "";
            $split=str_split($latest_message_details[1],12);
            $split=$split[0] . $dots;

            $return_string .="<a href='mensajes.php?u=$username'><div class='user_found_messages'>
                              <img src='". $user_found_obj->getProfilePic() . "' style='border-radius: 5px; margin-right: 5px;'>
                              ".$user_found_obj->getFirstAndLastName()."
                              <span class='timestamp_smaller' id='grey'>".$latest_message_details[2]."</span>
                              <p id='grey' style='margin:0;'>".$latest_message_details[0].$split."</p>
                               </div>
                            </a>";
        }
        return $return_string;
    }

    public function getConvosDropdown($data, $limit){
        $page=$data['page'];
        $userLoggedIn=$this->user_obj->getUsername();
        $return_string="";
        $convos=array();

        if($page==1){
          $start=0;}
        else{
            $start=($page-1)*$limit;
        }
        
        $set_viewed_quey=mysqli_query($this->con,"UPDATE mensajes_com SET visto='yes' WHERE usuario_a='$userLoggedIn'");

        $query=mysqli_query($this->con,"SELECT usuario_a, usuario_de FROM mensajes_com WHERE usuario_a='$userLoggedIn' OR usuario_de='$userLoggedIn' ORDER BY id DESC ");

        while($row=mysqli_fetch_array($query)){
            $user_to_push=($row["usuario_a"]!=$userLoggedIn)? $row['usuario_a'] : $row['usuario_de'];
            if(!in_array($user_to_push,$convos)){
                array_push($convos,$user_to_push);
            }
        }

        $num_iterations=0;
        $count=1;

        foreach($convos as $username){

            if($num_iterations++ < $start)
                continue;
            if($count>$limit)
                break;
            else
                $count++;

            
            $is_unread_query=mysqli_query($this->con,"SELECT abrio FROM mensajes_com WHERE usuario_a='$userLoggedIn' AND usuario_de='$username' ORDER BY id DESC ");
            $row=mysqli_fetch_array($is_unread_query);
            $style=(isset($row['abrio']) && $row['abrio'] == 'no' ) ? "background-color: #DDEDFF;" : "";
            $user_found_obj= new User($this->con,$username);
            $latest_message_details=$this->getLatestMessage($userLoggedIn,$username);

            $dots=(strlen($latest_message_details[1])>=12) ? "..." : "";
            $split=str_split($latest_message_details[1],12);
            $split=$split[0] . $dots;

            $return_string .="<a href='mensajes.php?u=$username'>
                            <div class='user_found_messages' style='".$style."'>
                              <img src='". $user_found_obj->getProfilePic() . "' style='border-radius: 5px; margin-right: 5px;'>
                              ".$user_found_obj->getFirstAndLastName()."
                              <span class='timestamp_smaller' id='grey'>".$latest_message_details[2]."</span>
                              <p id='grey' style='margin:0;'>".$latest_message_details[0].$split."</p>
                               </div>
                            </a>";
        }

        if($count>$limit)
            $return_string .="<input type='hidden' class='nextPageDropDownData' value='".($page+1)."'><input type='hidden' class='noMoreDropdownData' value='false'>";
        else
            $return_string .="<input type='hidden' class='noMoreDropdownData' value='true'><p style='text-align:center;'>No tienes mas mensajes!</p>";


        return $return_string;
    }


    public function getUnreadNumber() {
		$userLoggedIn = $this->user_obj->getUsername();
		$query = mysqli_query($this->con, "SELECT * FROM mensajes_com WHERE visto='no' AND usuario_a='$userLoggedIn'");
		return mysqli_num_rows($query);
	}

}

?>