<?php
require 'config/config.php';
include ("includes/classes/usuario.php");
include ("includes/classes/publicacion.php");
include ("includes/classes/mensaje.php");
include ("includes/classes/notificacion.php");


if (isset($_SESSION['usuario'])) {
    $userLoggedIn=$_SESSION['usuario'];
    $user_detail_query=mysqli_query($con, "SELECT * FROM usuarios_comunidad WHERE usuario='$userLoggedIn'");
    $user=mysqli_fetch_array($user_detail_query);
}else {
    header('Location:Registro.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comunidad</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="recursos/js/bootstrap.js"></script>
    <script src="recursos/js/bootbox.min.js"></script>
    <script src="recursos/js/demo.js"></script>
    <script src="recursos/js/a.js"></script>
    <script src="recursos/js/jquery.jcrop.js"></script>
	<script src="recursos/js/jcrop_bits.js"></script>



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="recursos/css/bootstrap.css">
    <link rel="stylesheet" href="recursos/css/estilos.css">
    <link rel="stylesheet" href="recursos/css/jquery.Jcrop.css" type="text/css" />

</head>
<body>
    <div class="top_bar">
        <div class="logo">
            <a href="index.php">Comunidad!</a>
        </div>

        <div class="search">
            <form action="buscar.php" method="get" name="search_form">
            <input type="text" onkeyup="getLiveSearchUsers(this.value,'<?php echo $userLoggedIn;?>')" name="q" placeholder="Buscar....." autocomplete="off" id="search_text_input">
            <div class="button_holder">
                <img src="recursos/img/icons/search.png" >
            </div>
            </form>
            <div class="search_results">
            </div>

            <div class="search_results_footer_empty">
            </div>

        </div>


        <nav>
        <?php
            $messages=new Message($con,$userLoggedIn);
            $num_messages=$messages->getUnreadNumber();

            $notifications=new Notification($con,$userLoggedIn);
            $num_notifications=$notifications->getUnreadNumber();


            $user_obj=new User($con,$userLoggedIn);
            $num_requests=$user_obj->getNumberOfFriendRequest();
        ?>



        <a href="<?php echo $userLoggedIn; ?>">
                <?php echo $user['nombre']; ?>
            </a>
            <a href="index.php">
                <i class="fa fa-home fa-lg"> </i>
            </a>
            <a href="javascript:void(0);" onclick="getDropdownData('<?php echo $userLoggedIn; ?>','message')">
                <i class="fa fa-envelope fa-lg"> </i>
                <?php
				if($num_messages > 0)
				 echo '<span class="notification_badge" id="unread_message">' . $num_messages . '</span>';
				?>
            </a>
            <a href="javascript:void(0);" onclick="getDropdownData('<?php echo $userLoggedIn; ?>','notification')">
                <i class="fa fa-bell-o fa-lg"> </i>
                <?php
				if($num_notifications > 0)
				 echo '<span class="notification_badge" id="unread_notification">' . $num_notifications . '</span>';
				?>
            </a>
            <a href="solicitudes.php">
                <i class="fa fa-users fa-lg"> </i>
                <?php
				if($num_requests > 0)
				 echo '<span class="notification_badge" id="unread_requests">' . $num_requests . '</span>';
				?>
            </a>
            <a href="ajustes.php">
                <i class="fa fa-cog fa-lg"> </i>
            </a>
            <a href="includes/handlers/salir.php">
                <i class="fa fa-sign-out fa-lg"> </i>
            </a>

        </nav>
        <div class="dropdown_data_window"  style="height:0px;"></div>
        <input type="hidden" id="dropdown_data_type" value="">
    </div>

    <script>
	var userLoggedIn = '<?php echo $userLoggedIn; ?>';

	$(document).ready(function() {

		$('.dropdown_data_window').scroll(function() {
			var inner_height = $('.dropdown_data_window').innerHeight(); //Div containing data
			var scroll_top = $('.dropdown_data_window').scrollTop();
			var page = $('.dropdown_data_window').find('.nextPageDropDownData').val();
			var noMoreData = $('.dropdown_data_window').find('.noMoreDropdownData').val();

			if ((scroll_top + inner_height >= $('.dropdown_data_window')[0].scrollHeight) && noMoreData == 'false') {

				var pageName; 
				var type = $('#dropdown_data_type').val();


				if(type == 'notification')
					pageName = "cargar_notificacion_ajax.php";
				else if(type == 'message')
					pageName = "cargar_mensaje_ajax.php"


				var ajaxReq = $.ajax({
					url: "includes/handlers/" + pageName,
					type: "POST",
					data: "page=" + page + "&userLoggedIn=" + userLoggedIn,
					cache:false,

					success: function(response) {
						$('.dropdown_data_window').find('.nextPageDropDownData').remove(); 
						$('.dropdown_data_window').find('.noMoreDropdownData').remove(); 


						$('.dropdown_data_window').append(response);
					}
				});

			}  

			return false;

		}); 


	});

	</script>
    <div class="wrapper">
