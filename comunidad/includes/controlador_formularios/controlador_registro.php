<?php
$fnombre="";
$lnombre="";
$em="";
$em2="";
$password="";
$password2="";
$date="";
$error_array=array();
if (isset($_POST['boton_registro'])) {
 
    $fnombre=strip_tags($_POST['reg_fnombre']);
    $fnombre=str_replace(' ','',$fnombre);
    $fnombre=ucfirst(strtolower($fnombre));
    $_SESSION['reg_fnombre']=$fnombre;

    $lnombre=strip_tags($_POST['reg_lnombre']);
    $lnombre=str_replace(' ','',$lnombre);
    $lnombre=ucfirst(strtolower($lnombre));
    $_SESSION['reg_lnombre']=$lnombre;

    $em=strip_tags($_POST['reg_email']);
    $em=str_replace(' ','',$em);
    $em=ucfirst(strtolower($em));
    $_SESSION['reg_email']=$em;

    $em2=strip_tags($_POST['reg_email2']);
    $em2=str_replace(' ','',$em2);
    $em2=ucfirst(strtolower($em2));
    $_SESSION['reg_email2']=$em2;

    $password=strip_tags($_POST['reg_password']);
    $password2=strip_tags($_POST['reg_password2']);

    $date=date("Y-m-d");

    if($em==$em2){  
    if (filter_var($em,FILTER_VALIDATE_EMAIL)) {
        $em=filter_var($em,FILTER_VALIDATE_EMAIL);

        $e_check=mysqli_query($con,"SELECT email FROM usuarios_comunidad WHERE email='$em'");

        $num_rows=mysqli_num_rows($e_check);
        if ($num_rows>0) {
            array_push($error_array, "Correo electronico en uso<br>") ;
        }

    }else{
       array_push($error_array,"Formato Invalido del correo electronico<br>")  ;
    }


    }else{
        array_push($error_array,"Los Correos no coinciden<br>") ;
    }
    if (strlen($fnombre)>25 || strlen($fnombre)<2) {
       array_push($error_array,'Su nombre debe tener entre 2 y 25 caracteres<br>') ;
    }
    if (strlen($lnombre)>25 || strlen($lnombre)<2) {
        array_push($error_array,'Su apellido debe tener entre 2 y 25 caracteres<br>') ;
    }
    if ($password!=$password2) {
       array_push($error_array,'Sus contraseñas no coinciden<br>');
    }
    else{
        if (preg_match('/^[a-zA-Z0-9]+$/',$password)) {
           array_push($error_array,'Su contraseña debe contener letras, caracteres y numeros<br>');
        }
    }
    if (strlen($password)>30 || strlen($password)<5 ) {
       array_push($error_array,'Su contraseña debe tener entre 5 y 30 caracteres. <br>');
    }

    if(empty($error_array)){
        $password=md5($password);

        $username=strtolower($fnombre."_". $lnombre);
        $check_username_query=mysqli_query($con,"SELECT usuario FROM usuarios_comunidad WHERE usuario='$username'");

        $i=0;
        while (mysqli_num_rows($check_username_query)!=0) {
            $i++;
            $username=$username ."_" . $i;
            $check_username_query=mysqli_query($con,"SELECT usuario FROM usuarios_comunidad WHERE usuario='$username'");

        }
        $rand=rand(1,2);
        if($rand==1)
        $foto="recursos/img/usuarios/default/head_deep_blue.png";
        else if ($rand==2)
        $foto="recursos/img/usuarios/default/head_turqoise.png";   
        
        $query=mysqli_query($con,"INSERT INTO usuarios_comunidad VALUES('','$fnombre','$lnombre','$username','$em','$password','$date','$foto','0','0','no',',') ");
        array_push($error_array,"<span style='color:#14C800;'>Registro Exitoso !</span><br>");

        $_SESSION['reg_fnombre']="";
        $_SESSION['reg_lnombre']="";
        $_SESSION['reg_email']="";
        $_SESSION['reg_email2']="";
    }

}
?>