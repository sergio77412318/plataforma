<?php

if(isset($_POST['update_details'])) {

	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$email = $_POST['email'];

	$email_check = mysqli_query($con, "SELECT * FROM usuarios_comunidad WHERE email='$email'");
	$row = mysqli_fetch_array($email_check);
	$matched_user = $row['usuario'];

	if($matched_user == "" || $matched_user == $userLoggedIn) {
		$message = "Datos Actualizados!<br><br>";

		$query = mysqli_query($con, "UPDATE usuarios_comunidad SET nombre='$first_name', apellido='$last_name', email='$email' WHERE usuario='$userLoggedIn'");
	}
	else 
		$message = "Ese correo electrónico ya está en uso!<br><br>";
}
else 
    $message = "";



    if(isset($_POST['update_password'])) {

        $old_password = strip_tags($_POST['old_password']);
        $new_password_1 = strip_tags($_POST['new_password_1']);
        $new_password_2 = strip_tags($_POST['new_password_2']);
    
        $password_query = mysqli_query($con, "SELECT clave FROM usuarios_comunidad WHERE usuario='$userLoggedIn'");
        $row = mysqli_fetch_array($password_query);
        $db_password = $row['clave'];
    
        if(md5($old_password) == $db_password) {
    
            if($new_password_1 == $new_password_2) {
    
    
                if(strlen($new_password_1) <= 4) {
                    $password_message = "Lo sentimos, tu contraseña debe tener más de 4 caracteres<br><br>";
                }	
                else {
                    $new_password_md5 = md5($new_password_1);
                    $password_query = mysqli_query($con, "UPDATE usuarios_comunidad SET clave='$new_password_md5' WHERE usuario='$userLoggedIn'");
                    $password_message = "La contraseña ha sido actualizada!<br><br>";
                }
    
    
            }
            else {
                $password_message = "Tus dos nuevas contraseñas deben coincidir!<br><br>";
            }
    
        }
        else {
                $password_message = "La contraseña actual es incorrecta! <br><br>";
        }
    
    }
    else {
        $password_message = "";
    }
    
    if(isset($_POST['close_account'])){
        header("Location: cerrar_cuenta.php");
    }
    

?>