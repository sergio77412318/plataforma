<?php
include ("includes/header.php");
$message_obj=new Message($con,$userLoggedIn);

if(isset($_GET['profile_username'])){
    $username = $_GET['profile_username'];
    $user_detail_query=mysqli_query($con,"SELECT * FROM usuarios_comunidad WHERE usuario='$username'");
    $user_array=mysqli_fetch_array($user_detail_query);

    $num_friends=(substr_count($user_array['array_amigo'],","))-1;
}

if(isset($_POST['remove_friend'])){
    $user=new User($con,$userLoggedIn);
    $user->removeFriend($username);
}

if(isset($_POST['add_friend'])){
    $user=new User($con,$userLoggedIn);
    $user->sendRequest($username);
}
if(isset($_POST['respond_request'])){
 header("Location: solicitudes.php");
}
if(isset($_POST['post_message'])){
    if(isset($_POST['message_body'])){
        $body=mysqli_real_escape_string($con,$_POST['message_body']);    
        $date=date("Y-m-d H:i:s");
        $message_obj->sendMessage($username,$body,$date);

    }
    $link='#profileTabs a[href="#messages_div"]';
    echo "<script>
            $(function(){
                $('".$link."').tab('show');
            });
            </script>";
}
?>
<style type="text/css">
.wrapper{
    margin-left:0px;
    padding-left:0px;
}

</style>
<div class="profile_left">
    <img src="<?php echo $user_array['foto']; ?>" >
    <div class="profile_info">
        <p><?php echo "Publicaciones: ".$user_array['num_posts']; ?></p>
        <p><?php echo "Likes: ".$user_array['num_likes']; ?></p>
        <p><?php echo "Amigos: ".$num_friends; ?></p>
    </div>
        <form action="<?php echo $username ?>" method="POST">
            <?php $profile_user_obj=new User($con,$username);
                    if($profile_user_obj->isClosed()){ 
                        header("Location: cerrar_usuario.php");
                    }
                    $logged_in_user_obj=new User($con,$userLoggedIn);
                    if($userLoggedIn!=$username){
                        if($logged_in_user_obj->isFriend($username)){
                            echo '<input type="submit" name="remove_friend" class="danger" value="Eliminar Amigo"><br>';
                        }
                        else if($logged_in_user_obj->didReceiveRequest($username)){
                            echo '<input type="submit" name="respond_request" class="warning" value="Aceptar Amigo"><br>';
                        }
                        else if($logged_in_user_obj->didSendRequest($username)){
                            echo '<input type="submit" name="" class="default" value="Solicitud Enviada"><br>';
                        }
                        else
                        echo '<input type="submit" name="add_friend" class="success" value="Agregar Amigo"><br>';

                    }

                ?>
        </form>
        <input type="submit" class="deep_blue" data-toggle="modal" data-target="#post_form" value="Publicar Algo">
        <?php
        if($userLoggedIn !=$username){
            echo '<div class="profile_info_bottom">';
            echo $logged_in_user_obj->getMutualFriends($username)." Amigos en Comun";
            echo '</div>';
        }
        ?>
</div>
    <div class="profile_main_column column">
        <ul class="nav nav-tabs" role="tablist" id="profileTabs">
            <li role="presentation" class="active"><a href="#newsfeed_div" aria-controls="newsfeed_div" role="tab" data-toggle="tab" >Noticias</a></li>
         <!--   <li  role="presentation"><a href="#about_div" aria-controls="about_div" role="tab" data-toggle="tab" >Informacion</a></li> en desarrollo-->
            <li  role="presentation"><a href="#messages_div"  aria-controls="messages_div" role="tab" data-toggle="tab">Mensajes</a></li>
        </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="newsfeed_div">
            <div class="posts_area"></div>
            <img id="loading" src="recursos/img/icons/loading.gif" >
        </div>

       <!-- <div role="tabpanel" class="tab-pane fade" id="about_div">
        en desrrollo            
        </div>-->

        <div role="tabpanel" class="tab-pane fade" id="messages_div">
                <?php
                echo "<h4>Tu y <a href='".$username."'>".$profile_user_obj->getFirstAndLastName(). "</a></h4><hr><br>";
                echo "<div class='loaded_message' id='scroll_messages'>";
                echo $message_obj->getMessages($username);
                echo "</div>";
            ?>

            <div class="message_post">
                <form action="" method="post">
                  <textarea name='message_body' id='message_textarea' placeholder='Escribe tu mensaje ...'></textarea>
                     <input type='submit' name='post_message' class='info' id='message_submit' value='Enviar'>

                </form>
            </div>
            <script>
            var div=document.getElementById("scroll_messages");
            div.scrollTop= div.scrollHeight;
            </script>
    
        </div>


    </div>
 
    </div>




<!-- Modal -->
<div class="modal fade" id="post_form" tabindex="-1" role="dialog" aria-labelledby="postModalLabel">

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Publica Algo!</h4>
      </div>
      <div class="modal-body">
        <p>Esto aparecerá en la página de perfil del usuario y también en su suministro de noticias para que lo vean tus amigos!</p>
        <form class="profile_post" action="" method="post" >
      
             <div class="form-group">
                    <textarea class="form-control" name="post_body" style="width:100%;"></textarea>
                    
                    <input type="hidden" name="user_from" value="<?php echo $userLoggedIn; ?>">
                    <input type="hidden" name="user_to" value="<?php echo $username; ?>">

             </div>   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" name="post_button" id="submit_profile_post">Publicar</button>
      </div>
    </div>
  </div>
</div>


<script>
		var userLoggedIn = '<?php echo $userLoggedIn; ?>';
        var profileUsername = '<?php echo $username; ?>';
		$(document).ready(function() {

			$('#loading').show();

			 
			$.ajax({
				url: "includes/handlers/carga_ajax_prefil.php",
				type: "POST",
                data: "page=1&userLoggedIn=" + userLoggedIn + "&profileUsername=" + profileUsername,
				cache:false,

				success: function(data) {
					$('#loading').hide();
					$('.posts_area').html(data);
				}
			});

			$(window).scroll(function() {
				var height = $('.posts_area').height();
				var scroll_top = $(this).scrollTop();
				var page = $('.posts_area').find('.nextPage').val();
				var noMorePosts = $('.posts_area').find('.noMorePosts').val();

				if ((document.body.scrollHeight == document.body.scrollTop + window.innerHeight) && noMorePosts == 'false') {
                    $('#loading').show();
                    alert("hello");

					var ajaxReq = $.ajax({
						url: "includes/handlers/carga_ajax_prefil.php",
						type: "POST",
						data: "page=" + page + "&userLoggedIn=" + userLoggedIn + "&profileUsername=" + profileUsername,
						cache:false,

						success: function(response) {
							$('.posts_area').find('.nextPage').remove();  
							$('.posts_area').find('.noMorePosts').remove(); 
							$('#loading').hide();
							$('.posts_area').append(response);
						}
					});

				} 

				return false;

			}); 


		});

		</script>


    </div>
</body>
</html>
