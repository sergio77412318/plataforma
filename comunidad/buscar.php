<?php
include ("includes/header.php");

if(isset($_GET['q'])){
    $query=$_GET['q'];
}
else{
    $query="";
}

if(isset($_GET['type'])){
    $type=$_GET['type'];
}
else{
    $type="nombre";
}
?>

<div class="main_column column" id="main_column">
    <?php
    if($query=="")
        echo "Debes ingresar algo en el cuadro de búsqueda.";
    else{

        if($type=="username")
            $usersReturnedQuery=mysqli_query($con,"SELECT * FROM usuarios_comunidad WHERE usuario LIKE '$query%' AND cerrar_usuario='no' LIMIT 8");
        else{
            $names=explode(" ",$query);

             if(count($names)==3)
            $usersReturnedQuery=mysqli_query($con,"SELECT * FROM usuarios_comunidad WHERE (nombre LIKE '$names[0]%' AND apellido LIKE '$names[2]%') AND cerrar_usuario='no' ");
            else  if(count($names)==2)   
            $usersReturnedQuery=mysqli_query($con,"SELECT * FROM usuarios_comunidad WHERE (nombre LIKE '$names[0]%' AND apellido LIKE '$names[1]%') AND cerrar_usuario='no' ");
            else
            $usersReturnedQuery=mysqli_query($con,"SELECT * FROM usuarios_comunidad WHERE (nombre LIKE '$names[0]%' OR apellido LIKE '$names[0]%') AND cerrar_usuario='no' ");
            
        }
        
        if(mysqli_num_rows($usersReturnedQuery)==0)
            echo "No podemos encontrar a nadie con ese un ".$type. ": " .$query;
        else    
            echo mysqli_num_rows($usersReturnedQuery). " Resultados Encontrados: <br><br>";

        echo "<p id='grey'>Intenta buscar:</p>";
        echo "<a href='buscar.php?q=".$query."&type=name'>Nombres</a>,<a href='buscar.php?q=".$query."&type=username'>Usuarios</a><br><br><hr id='search_hr'>";
        while ($row=mysqli_fetch_array($usersReturnedQuery)) {
            $user_obj =new User($con,$user['usuario']);

            $button="";
            $mutual_friends= "";

            if($user['usuario']!=$row['usuario']) {
                if($user_obj->isFriend($row['usuario']))
                  $button="<input type='submit' name='".$row['usuario']."' class='danger' value='Eliminar Amigo'>";
                  else if($user_obj->didReceiveRequest($row['usuario'])) 
                    $button="<input type='submit' name='".$row['usuario']."' class='warning' value='Reponder la solicitud'>";
                    else if($user_obj->didSendRequest($row['usuario'])) 
                    $button="<input type='submit' class='default' value='Solicitud Enviada' >";
                    else
                    $button="<input type='submit' name='".$row['usuario']."' class='success' value='Agregar Amigo'>";

                $mutual_friends=$user_obj->getMutualFriends($row['usuario'])." Amigos en Comun"   ; 


                if(isset($_POST[$row['usuario']])){
                    if($user_obj->isFriend($row['usuario'])){
                        $user_obj->removeFriend($row['usuario']);
                        header("Location: http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
                    }
                    else if($user_obj->didReceiveRequest($row['usuario'])){
                        header("Location: solicitudes.php");
                    }
                    else if($user_obj->didSendRequest($row['usuario'])){

                    }
                    else{
                        $user_obj->sendRequest($row['usuario']);
                        header("Location: http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
                    }
                }
                                       
             }

             echo "<div class='search_result'>
                        <div class='searchPageFriendButtons'>
                            <form action='' method='post'>
                                ".$button."
                                <br>
                            </form>
                         </div>
                         
                     <div class='result_profile_pic'>   
                        <a href='".$row['usuario']."'><img src='".$row['foto']."' style='height:100px;'></a>
                     </div>    

                     <a href='".$row['usuario']."'>".$row['nombre']." ".$row['apellido']."
                     <p id='grey'>".$row['usuario']."</p>
                    </a>
                    <br>
                    ".$mutual_friends."<br>
                     
                    </div>
                    <hr id='search_hr'>";
        }

    }
    ?>

</div>