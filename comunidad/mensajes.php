<?php
include ("includes/header.php");

$message_obj=new Message($con,$userLoggedIn);

if(isset($_GET['u']))
    $user_to=$_GET['u'];
else{
    $user_to=$message_obj->getMostRecentUser();
    if($user_to==false)
        $user_to="new";
}    

if($user_to !="new")
    $user_to_obj=new User($con,$user_to);


if(isset($_POST['post_message'])){
    if(isset($_POST['message_body'])){
        $body=mysqli_real_escape_string($con, $_POST['message_body']);
        $date=date('Y-m-d H:i:s');
        $message_obj->sendMessage($user_to,$body,$date);
    }   
}
?>

<div class="user_details column">
    <a href="<?php echo $userLoggedIn; ?>"><img src="<?php echo $user['foto']; ?>" ></a>
    <div class="user_detail_left_right">
        <a href="<?php echo $userLoggedIn; ?>">
        <?php echo $user['nombre'] . " " . $user['apellido'];?>
        </a>
        <br>
        <?php echo "Publicaciones: ".$user['num_posts']."<br>";
            echo "Likes: " .$user['num_likes']; 

        ?>
    
    </div>
</div>

<div class="main_column column" id="main_column">
    <?php
    if($user_to!="new"){
        echo "<h4>Tu y <a href='$user_to'>".$user_to_obj->getFirstAndLastName(). "</a></h4><hr><br>";
        echo "<div class='loaded_message' id='scroll_messages'>";
        echo $message_obj->getMessages($user_to);
        echo "</div>";
    }
    else{
        echo "<h4>Nuevos Mensajes</h4>";
    }
    ?>

    <div class="message_post">
        <form action="" method="post">
            <?php
            if($user_to=="new"){
                echo "Seleccione el amigo al que le gustaría enviar un mensaje <br></br>";
                ?> 
                Para: <input type='text' onkeyup='getUser(this.value,"<?php echo $userLoggedIn;  ?>")' name='q' placeholder='Nombre' autocomplete='off' id='search_text_input' >
                <?php
                echo "<div class='results'></div>";
            }
            else{
                echo "<textarea name='message_body' id='message_textarea' placeholder='Escribe tu mensaje ...'></textarea>";
                echo "<input type='submit' name='post_message' class='info' id='message_submit' value='Enviar'>";
            } 
            ?>
        </form>
    </div>
    <script>
    var div=document.getElementById("scroll_messages");
    div.scrollTop= div.scrollHeight;
    </script>
    
</div>
<div class="user_details column" id="conversations">
    <h4>Conversaciones</h4>
    <div class="loaded_conversations">
            <?php echo $message_obj->getConvos(); ?>
    </div>
    <br>
        <a href="mensajes.php?u=new">Nuevo Mensaje</a>
    
    </div>