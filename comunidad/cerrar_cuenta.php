<?php
include("includes/header.php");

if(isset($_POST['cancel'])){
    header("Location: ajustes.php");
}
if(isset($_POST['close_account'])){
    $close_query = mysqli_query($con,"UPDATE usuarios_comunidad SET cerrar_usuario='si' WHERE usuario='$userLoggedIn'");
    session_destroy();
    header("Location:Registro.php");
}
?>
<div class="main_column column">
    <h4>Cerrar Cuenta</h4>
    ¿Está seguro de que desea cerrar su cuenta? <br> <br>
    Cerrar su cuenta ocultará su perfil y toda su actividad de otros usuarios. <br> <br>
    Puede volver a abrir su cuenta en cualquier momento simplemente iniciando sesión. <br> <br>
    <form action="cerrar_cuenta.php" method="post"> 
        <input type="submit" name="close_account" id="close_account" value="Si cierralo!" class="danger setting_submit">
        <input type="submit" name="cancel" id="update_details" value="De ninguna Manera!" class="info setting_submit">

    </form>
</div>