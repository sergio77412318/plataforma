<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="recursos/css/estilos.css">
</head>
<body>
    <style type="text/css">
    body {
        background-color: #fff;
    }
    form{
        position: absolute;
        top: 0;
        }

    </style>
<?php
        require 'config/config.php';
        include ("includes/classes/usuario.php");
        include ("includes/classes/publicacion.php");
        include ("includes/classes/notificacion.php");


        if (isset($_SESSION['usuario'])) {
            $userLoggedIn=$_SESSION['usuario'];
            $user_detail_query=mysqli_query($con, "SELECT * FROM usuarios_comunidad WHERE usuario='$userLoggedIn'");
            $user=mysqli_fetch_array($user_detail_query);
        }else {
            header('Location:Registro.php');
        }

        if(isset($_GET['publicacion_id'])){
            $post_id=$_GET['publicacion_id'];
        }

    $get_likes=mysqli_query($con,"SELECT likes, anadido_por FROM publicaciones WHERE id='$post_id' ");
    $row=mysqli_fetch_array($get_likes);
    $total_likes=$row['likes'];
    $user_liked=$row['anadido_por'];
    
    $user_details_query=mysqli_query($con,"SELECT * FROM usuarios_comunidad WHERE usuario='$user_liked'");
    $row=mysqli_fetch_array($user_details_query);
    $total_user_likes=$row['num_likes'];
    //boton me gusta
    if(isset($_POST['like_button'])){
        $total_likes++;
        $query=mysqli_query($con,"UPDATE publicaciones SET likes ='$total_likes' WHERE id='$post_id' ");
        $total_user_likes++;
        $user_likes=mysqli_query($con,"UPDATE usuarios_comunidad SET num_likes='$total_user_likes' WHERE usuario='$user_liked' ");
        $insert_user=mysqli_query($con,"INSERT INTO likes VALUES('','$userLoggedIn','$post_id') ");
        //insertar notificaciones
        if($user_liked != $userLoggedIn){
            $notification =new Notification($con,$userLoggedIn);
			$notification->insertNotification($post_id,$user_liked,"like");
        }
    }
    //boton no me gusta
    if(isset($_POST['unlike_button'])){
        $total_likes--;
        $query=mysqli_query($con,"UPDATE publicaciones SET likes ='$total_likes' WHERE id='$post_id' ");
        $total_user_likes--;
        $user_likes=mysqli_query($con,"UPDATE usuarios_comunidad SET num_likes='$total_user_likes' WHERE usuario='$user_liked' ");
        $insert_user=mysqli_query($con,"DELETE FROM likes WHERE usuario='$userLoggedIn' AND publicacion_id='$post_id'");
    }

    $check_query=mysqli_query($con,"SELECT * FROM likes WHERE usuario ='$userLoggedIn' AND publicacion_id='$post_id'");
    $num_rows=mysqli_num_rows($check_query);

    if($num_rows>0) {
        echo '<form action="like.php?publicacion_id='.$post_id .'" method="post">
                <input type="submit" class="comment_like" name="unlike_button" value="No me gusta">
                <div class="like_value">
                    '.$total_likes.' Me gusta
                </div>
                </form>';
    }
    else{
        echo '<form action="like.php?publicacion_id='.$post_id .'" method="post">
        <input type="submit" class="comment_like" name="like_button" value="Me gusta">
        <div class="like_value">
            '.$total_likes.' Me gusta  
        </div>
        </form>';
    }
    ?>
</body>
</html>