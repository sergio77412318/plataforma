<?php
require 'config/config.php';
require 'includes/controlador_formularios/controlador_registro.php';
require 'includes/controlador_formularios/controlador_login.php';


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bienvendios a la comunidad</title>
    <link rel="stylesheet" type="text/css" href="recursos/css/registro_estilo.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="recursos/js/registro.js"></script>
</head>
<body >
<?php  

if(isset($_POST['boton_registro'])) {
    echo '
    <script>

    $(document).ready(function() {
        $("#first").hide();
        $("#second").show();
    });

    </script>

    ';
}


?>
<div class="wrapper">
    
        <div class="login_box">
        <div class="login_header">
            <h1>Comunidad</h1>
            Inicia Sesion o Registrate!
        </div>
        <div id="first">
             <!--formulario del login-->
             <form action="Registro.php" method="post">
                <input type="email" name="log_email" placeholder="Correo Electronico" value='<?php
                        if (isset($_SESSION['log_email'])) {
                            echo $_SESSION['log_email'];
                        }
                        ?>' required="">
                <br>
                <input type="password" name="log_password" placeholder="Contraseña">
                <br>
                <?php if(in_array("Correro o Contraseña Incorrecta<br>",$error_array)) echo "Correro o Contraseña Incorrecta<br>"; ?>
                <input type="submit" name="boton_login" value="Login">
                <br>
                <a href="#" id="signup" class="signup">¿Necesitas una cuenta? ¡Registrarse aquí!</a>
                <br>
                <a href="../aula" id="signup" class="aula" class="signup">¡Volver al Aula!</a>

               
                
            </form>

        </div>
           <div id="second">
                <!--formulario del registro-->
                <form action="Registro.php" method="post">
                    <br>
                    <input type="text" name="reg_fnombre" placeholder="Ingrese su Nombre" value='<?php
                            if (isset($_SESSION['reg_fnombre'])) {
                                echo $_SESSION['reg_fnombre'];
                            }
                            ?>' required="">
                    <br>
                    <?php if (in_array('Su nombre debe tener entre 2 y 25 caracteres<br>',$error_array)) echo 'Su nombre debe tener entre 2 y 25 caracteres<br>'; ?>


                    <input type="text" name="reg_lnombre" placeholder="Ingrese su Apellido" value='<?php
                            if (isset($_SESSION['reg_lnombre'])) {
                                echo $_SESSION['reg_lnombre'];
                            }
                            ?>' required="">
                    <br>
                    <?php if (in_array('Su apellido debe tener entre 2 y 25 caracteres<br>',$error_array)) echo 'Su apellido debe tener entre 2 y 25 caracteres<br>'; ?>


                    <input type="email" name="reg_email" placeholder="Ingrese su Correro Electronico" value='<?php
                            if (isset($_SESSION['reg_email'])) {
                                echo $_SESSION['reg_email'];
                            }
                            ?>'  required="">
                    <br>
                    

                    <input type="email" name="reg_email2" placeholder="Confirmar Correro Electronico" value='<?php
                            if (isset($_SESSION['reg_email2'])) {
                                echo $_SESSION['reg_email2'];
                            }
                            ?>' required="">
                    <br>
                    <?php if (in_array("Correo electronico en uso<br>",$error_array)) echo "Correo electronico en uso<br>"; 
                    else if (in_array("Formato Invalido del correo electronico<br>",$error_array)) echo "Formato Invalido del correo electronico<br>"; 
                    else if (in_array("Los Correos no coinciden<br>",$error_array)) echo "Los Correos no coinciden<br>"; ?>
                


                    <input type="password" name="reg_password" placeholder="Ingrese su Contraseña" required="">
                    <br>
                    <input type="password" name="reg_password2" placeholder="Confirmar su Contraseña" required="">
                    <br>
                    <?php if (in_array('Sus contraseñas no coinciden<br>',$error_array)) echo 'Sus contraseñas no coinciden<br>'; 
                    else if (in_array('Su contraseña debe tener entre 5 y 30 caracteres. <br>',$error_array)) echo 'Su contraseña debe tener entre 5 y 30 caracteres. <br>'; 
                    else if (in_array('Su contraseña debe contener letras, caracteres y numeros<br>',$error_array)) echo'Su contraseña debe contener letras, caracteres y numeros<br>';?>

                    <input type="submit" name="boton_registro" value="Registro">
                    <br>
                    <?php if (in_array("<span style='color:#14C800;'>Registro Exitoso !</span><br>",$error_array)) echo "<span style='color:#14C800;'>Registro Exitoso !</span><br>"; ?>
                    <a href="#" id="signin" class="signin">¿Ya tienes una cuenta? ¡Inicia Sesion aquí!</a>


                    
                </form>
            </div>
         </div>
</div>
</body>
</html>
