$("#aulas").change(function() {
    $(".alert").remove();
    var aula = $(this).val();
    var datos=new FormData();
    datos.append("VerificarAula", aula);
    $.ajax({
         url:"Ajax/aulasvirtualesA.php",
         method: "POST",
         data:datos,
         dataType:"json",
         cache:false,
         contentType:false,
         processData:false,
         
         success:function(resultado){
             if(resultado){
                $("#aulas").parent().after('<div class="alert alert-danger">El Aula ya Existe.</div>');
                $("#aulas").val("");
            }
         }
    })
})