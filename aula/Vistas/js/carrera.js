$("#carrera").change(function() {
    $(".alert").remove();
    var carrera = $(this).val();
    var datos=new FormData();
    datos.append("VerificarCarrera", carrera);
    $.ajax({
         url:"Ajax/carreraA.php",
         method: "POST",
         data:datos,
         dataType:"json",
         cache:false,
         contentType:false,
         processData:false,
         
         success:function(resultado){
             if(resultado){
                $("#carrera").parent().after('<div class="alert alert-danger">La Carrera ya Existe.</div>');
                $("#carrera").val("");
            }
         }
    })
})