<?php
    $exp=explode("/",$_GET["url"]);

    $columna="id";
    $valor=$exp[1];
    $aula=AulasC::VerAulas2C($columna,$valor);

    if($_SESSION["rol"]=="Estudiante"){
        $columna="id_aula";
        $valor=$exp[1];
        $columna2="id_alumno";
        $valor2=$_SESSION["id"];

        $inscripto=EstudiantesC::VerInscriptoC($columna,$valor,$columna2,$valor2);
        if($inscripto==false){
            echo '<script>
            window.location="http://localhost/plataforma/aula/Aulas-Virtuales";  
        </script>';
        }
    }else if($_SESSION["rol"]=="Docente" && $_SESSION["id"]!=$aula["id_docente"]) {
        echo '<script>
        window.location="http://localhost/plataforma/aula/Mis-Aulas";  
    </script>';
    }
?>

<style>
.inicio{
    width: 0%;
}
.medio{
    width: 50%;
}
.final{
    width: 100%; 
}
.animacion{
    transition: all 2s ease .5s;
}
</style>
<div class="content-wrapper">
<section class="content-header">
<?php
echo '<h1>Aula Virtual de la Materia:<b>'.$aula["materia"].'</b></h1>';
?>
<!--<h2 id="titulo" class="display-4">Animacion de Barra</h2>
<div class="progress">
   <div id ="barra" class="progress-bar progress-bar-striped inicio animacion" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" ></div>
</div>-->
</section>
<section class="content">
<?php //echo '<button  id="boton1234" type="submit" class="btn btn-primary"> Seccion</button>';             ?>
    <?php
        if ($_SESSION["rol"]=="Docente"){
        echo '<form  method="post">
                <input type="hidden" name="id_aula" value="'.$exp[1].'">
                <button   type="submit" class="btn btn-primary">Agregar Nueva Seccion</button>';
                $crearS=new SeccionesC();
                $crearS->CrearSeccionesC();
                
                echo'</form>';
        }else{
            $columna="id";
            $valor=$aula["id_docente"];
            $docente=UsuariosC::VerUsuariosC($columna,$valor);
            echo '<h2>Docente: '.$docente["apellido"].'   '.$docente["nombre"].' 
            <h2>';
            /*<a href="http://localhost/plataforma/aula/Enviar-Mensaje/'.$docente["id"].'">Enviar Mensaje</a> */
        }


    if($_SESSION["rol"]=="Docente"){
       echo '<a href="http://localhost/plataforma/aula/Inscriptos/'.$exp[1].'">
         <button class="btn btn-success pull-right">Ver Estudiantes Inscriptos Y llamar lista </button>   
        </a>';


    }else if($_SESSION["rol"]=="Estudiante"){
        $exp=explode("/",$_GET["url"]);
 $columna="id";
 $valor=$exp[1];
 $aula = AulasC::VerAulas2C($columna,$valor);
        echo '<form method="post">
        <input type="hidden" name="id_alumno" value="'.$_SESSION["id"].'">
        <input type="hidden" name="id_aula" value="'.$exp[1].'">
        <button type="submit" class="btn btn-danger">Dar de Baja</button>
       <a href="http://localhost/plataforma/aula/Ver-Examenes/'.$aula["id"].'"> <button type="button" class="btn btn-primary">Ver Examenes</button></a>
    </form>';
    $darbaja=new EstudiantesC();
    $darbaja ->DarBajaC();
    }
    
    ?>
    <br><br>
    <?php
    $columna=null;
    $valor=null;
    $resultado=SeccionesC::VerSeccionesC($columna,$valor);

    foreach ($resultado as $key => $value) {
        if ($value["id_aula"]==$exp[1]){
            echo '<div class="box">
            <div class="box-header">';
            if($_SESSION["rol"]=="Docente"){
                echo '<form  method="post">
                <h3 class="box-title"><input type="text" name="nombre" class="form-control" value="'.$value["nombre"].'"></h3>
                <input type="hidden" name="id_seccion"  value="'.$value["id"].'">
                <button type="submit" class="btn btn-success"><i class="fa fa-pencil"></i></button>';

                $nombre=new SeccionesC();
                $nombre->EditarNombreSC();
            echo '</form>';
            }else {
                echo '<h3 class="box-title">'.$value["nombre"].'</h3>';
            }
            
            
                 echo   '<div class="box-tools pull-right">
                        <button type="button" class="btn" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>';
                       if ($_SESSION["rol"]=="Docente"){
                           echo '<button class="btn btn-danger BorrarSeccion" Sid="'.$value["id"].'" data-toggle="modal" data-target="#BorrarSeccion">
                           <i class="fa fa-times" ></i>
                       </button>';
                       }
                        
                   echo '</div>
            </div>
                <div class="box-body">';
                if($_SESSION["rol"]=="Docente"){
                    if($value["descripcion"]==""){
                          echo '<a href="http://localhost/plataforma/aula/D-S/'.$value["id"].'">
                          <button class="btn btn-success">Agregar Descripcion</button>
                          
                      </a>';  
                    }else{
                        echo ''.$value["descripcion"].'
                        <a href="http://localhost/plataforma/aula/D-S/'.$value["id"].'">
                        <button class="btn btn-success"><i class="fa fa-pencil"></i></button>
                        </a>';
                    }
                }else{
                    echo $value["descripcion"];
                }
                if($_SESSION["rol"]=="Docente"){
                   echo '<hr><button class="btn btn-primary AgregarArchivo" Sid="'.$value["id"].'" data-toggle="modal" data-target="#AgregarArchivo">Agregar Archivo</button>';
                echo '<br>
                        <h3><b>Archivos:</b></h3>';
                $columna="id_seccion";
                $valor=$value["id"];
                $archivos=SeccionesC::VerArchivosC($columna, $valor);
                foreach ($archivos as $key => $arch) {
                    echo '<form method="post">
                        '.$arch["nombre"].'- <a href="http://localhost/plataforma/aula/'.$arch["archivo"].'" download="'.$arch["nombre"].'">Descargar Archivo</a>
                        <input type="hidden" name="id" value="'.$arch["id"].'">
                        <input type="hidden" name="id_a" value="'.$exp[1].'">
                        <input type="hidden" name="archivo" value="'.$arch["archivo"].'">
                        <button class="btn btn-danger btn-xs" type="submit" data-toggle="tooltip" title="Eliminar Archivo"><i class="fa fa-trash"></i></button>

                    </form>
                    <br>';
                    $borrarArch=new SeccionesC();
                    $borrarArch->borrarArchivosC();

                    
                            
                }
                /*<a href="http://localhost/plataforma/aula/Grupal/'.$exp[1].'">
                    <button class="btn btn-success pull-right">Agregar Trabajo Grupal </button>   
                   </a> */
                echo    '<hr>
                    <h3><b>Tareas:</b></h3>
                    
                    <form method="post">
                        <input type="hidden" name="idSeccion" value="'.$value["id"].'">
                        <button class="btn btn-warning" type="submit">Agregar Tarea</button>
                      
                    </form> 
                   
                    <br>';
                $columna="id_seccion";
                $valor=$value["id"];
                $Tareas=TareasC::VerTareasC($columna,$valor);
                foreach ($Tareas as $key => $tarea) {
                   echo '<form  method="post">
                   <a href="http://localhost/plataforma/aula/Tarea/'.$tarea["id"].'">
                   <button type="button" class="btn btn-warning">'.$tarea["nombre"]. '<i class="fa fa-eye"></i></button> - ';
                   if ($tarea["fecha_limite"]=="") {
                       echo 'Sin Fecha Limite Aun';
                   }else {
                       echo $tarea["fecha_limite"];
                   }
                  echo '</a>
                  <input type="hidden" name="idT" value="'.$tarea["id"].'"> 
                  <input type="hidden" name="idAula" value="'.$exp[1].'">

                  <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar Tarea" >
                  <i class="fa fa-times"></i>
                  </button>';
                   $borrarTarea= new SeccionesC();
                   $borrarTarea->BorrarTareaC();
                 echo '</form> 
                  
                  <br><br>';
                }
            }else{
                echo '<hr>
                <h3><b>Archivos:</b></h3>';
                $columna="id_seccion";
                $valor=$value["id"];
                $archivos=SeccionesC::VerArchivosC($columna, $valor);
                foreach ($archivos as $key => $arch) {
                    echo '
                        '.$arch["nombre"].'- <a href="http://localhost/plataforma/aula/'.$arch["archivo"].'" download="'.$arch["nombre"].'">Descargar Archivo</a>
                        
                    
                    <br>';
                    
                }

                echo    '<hr>
                    <h3><b>Tareas:</b></h3>';
                $columna="id_seccion";
                $valor=$value["id"];
                $Tareas=TareasC::VerTareasC($columna,$valor);
                foreach ($Tareas as $key => $tarea) {
                   echo '<a href="http://localhost/plataforma/aula/Tarea/'.$tarea["id"].'">
                   <button type="button" class="btn btn-warning">'.$tarea["nombre"]. '<i class="fa fa-eye"></i></button> - ';
                   if ($tarea["fecha_limite"]=="") {
                       echo 'Sin Fecha Limite Aun';
                   }else {
                       echo $tarea["fecha_limite"];
                   }
                  echo '</a>
                    
                  <br><br>';
                }
            }
            echo '</div>
            </div>';
            }
               
        
    }
    ?>
    
</section>
</div>



<div class="modal fade" id="AgregarArchivo">
    <div class="modal-dialog">
        <div class="modal-content"> 
            <form method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <h2>Nombre del Archivo:</h2>
                            <input type="text" class="form-control input-lg" name="nombreA" required="">

                            <input type="hidden" id="idS" name="id_s" value="">
                            <?php
                               echo '<input type="hidden" name="id_a" value="'.$exp[1].'">';
                            ?>
                        </div>
                        <div class="form-group">
                            <h2>Archivo:</h2>
                            <input type="file" class="form-control input-lg" name="archivo" required="">
                        </div>
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                <?php
                    $agregar=new SeccionesC();
                    $agregar-> AgregarArchivoC();
                ?>
            </form>
        </div>
    </div>

</div>

<div class="modal fade" id="BorrarSeccion">
    <div class="modal-dialog">
        <div class="modal-content"> 
            <form method="post" >
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <h2>Eliminar esta Seccion?</h2>
                            <input type="hidden" class="form-control input-lg" id="idSE" name="idS" required="">

                            <?php
                               echo '<input type="hidden" name="id_a" value="'.$exp[1].'">';
                            ?>
                        </div>
                        
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Si</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                </div>
                <?php
                    $borrarSeccion=new SeccionesC();
                    $borrarSeccion-> BorrarSeccionC();
                ?>
            </form>
        </div>
    </div>

</div>
<?php
$tarea=new TareasC();
$tarea->AgregarTareaC();
?>