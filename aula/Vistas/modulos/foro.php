<?php

?>
<div class="content-wrapper">
<section class="content-header">
<h1>Creacion de Categorias de foros</h1>

</section>
<section class="content">
<div class="box">
    <div class="box-body">
        <form method="post" enctype="multipart/form-data">
            <h2>Nombre de la categoria:</h2>
            <input type="text" class="input-lg" id="aulas" name="category_name" required="">
           
            <br>
            <h2>Descripcion</h2>
            <textarea name="category_description" id="editor" required=""></textarea>
            
            <h2>Imagen</h2>
            <input type="file" name="imagen" required>
            <br>
            <button type="submit" class="btn btn-primary">Crear Categoria</button>
            <?php
                $solicitar=new AulasC();
                $solicitar-> CreacionForoC();
            ?>
        </form>
    
    </div>
</div>
</section>
</div>