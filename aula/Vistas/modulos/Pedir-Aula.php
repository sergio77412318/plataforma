<?php
if($_SESSION["rol"]!="Docente"){
    echo '<script>
    
    window.location="Inicio";
    </script>';
    return;
}

?>
<div class="content-wrapper">
<section class="content-header">
<h1>Solicitar Aulas Virtuales</h1>

</section>
<section class="content">
<div class="box">
    <div class="box-body">
        <form method="post">
            <h2>Nombre de la materia:</h2>
            <input type="text" class="input-lg" id="aulas" name="materia" required="">
            <?php
            echo   '<input type="hidden" class="input-lg" name="id_docente" value="'.$_SESSION["id"].'">';
            ?>
            <br>
            <h2>Observaciones</h2>
            <textarea name="observaciones" id="editor"></textarea>
            <br><br>
            <button type="submit" class="btn btn-primary">Solicitar</button>
            <?php
                $solicitar=new AulasC();
                $solicitar-> SolicitarAulaC();
            ?>
        </form>
    
    </div>
</div>
</section>
</div>