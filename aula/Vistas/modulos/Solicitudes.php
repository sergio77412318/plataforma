<?php
if($_SESSION["rol"]!="Docente" && $_SESSION["rol"]!="Administrador"){
    echo '<script>
    
    window.location="Inicio";
    </script>';
    return;
}

?>
<div class="content-wrapper">
<section class="content-header">

    <?php
        if ($_SESSION["rol"]=="Docente") {
            echo '<h1>Mis Solicitudes</h1>';
        }else{
            echo '<h1>Solicitudes de aulas</h1>';
        }
    ?>



</section>
<section class="content">
<div class="box">
    <div class="box-body">
        <?php
            if ($_SESSION["rol"]=="Docente"){
                echo '<h2>Aulas Aun no Aprobadas</h2>
                <table class="table table-bordered table-hover table-striped dt-responsive">
            <thead>
                <tr>
                    <th>Materia</th>
                    <th>Observaciones</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>';
             $resultado=AulasC::VerSolicitudesC();   
             foreach ($resultado as $key => $value) {
                 if($value["estado"]!="Aprobada" && $_SESSION["id"]==$value["id_docente"]){
                    echo '<tr>
                    <td>'.$value["materia"].'</td>
                    <td>'.$value["observaciones"].'</td>
                    <td><button class="btn btn-primary">'.$value["estado"].'</button></td>
         
                </tr>';
                 }
                 
             }
                
           echo '</tbody>
        </table>';


        echo '<h2>Aulas Aprobadas</h2>
        <table class="table table-bordered table-hover table-striped dt-responsive">
    <thead>
        <tr>
            <th>Materia</th>
            <th>Observaciones</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>';
     $resultado=AulasC::VerSolicitudesC();   
     foreach ($resultado as $key => $value) {
         if($value["estado"]=="Aprobada" && $_SESSION["id"]==$value["id_docente"]){
            echo '<tr>
            <td>'.$value["materia"].'</td>
            <td>'.$value["observaciones"].'</td>
            <td><button class="btn btn-success">'.$value["estado"].'</button></td>
 
        </tr>';
         }
         
     }
        
   echo '</tbody>
</table>';
            }else{
                echo '<h2>Aulas Aun no Aprobadas</h2>
                <table class="table table-bordered table-hover table-striped dt-responsive">
            <thead>
                <tr>
                    <th>Materia</th>
                    <th>Docente</th>
                    <th>Observaciones</th>
                    <th>Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>';
             $resultado=AulasC::VerSolicitudesC();   
             foreach ($resultado as $key => $value) {
                 if($value["estado"]!="Aprobada"){
                    echo '<tr>
                        <td>'.$value["materia"].'</td>';

                        $columna ="id";
                        $valor = $value["id_docente"];
                        $docente=UsuariosC::VerUsuariosC($columna,$valor);

                        echo '<td>'.$docente["apellido"].' '.$docente["nombre"].'</td>';

                    echo '<td>'.$value["observaciones"].'</td>
                    <td><button class="btn btn-primary">'.$value["estado"].'</button></td>

                    <td>
                        <button class="btn btn-success CrearAula" Soid="'.$value["id"].'" data-toggle="modal" data-target="#CrearAula">Aprobar</button>

                    </td>
         
                </tr>';
                 }
                 
             }
                
           echo '</tbody>
        </table>';


        echo '<h2>Aulas Aprobadas</h2>
        <table class="table table-bordered table-hover table-striped dt-responsive">
    <thead>
        <tr>
            <th>Materia</th>
            <th>Docente</th>
            <th>Observaciones</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>';
     $resultado=AulasC::VerSolicitudesC();   
     foreach ($resultado as $key => $value) {
         if($value["estado"]=="Aprobada" ){
            echo '<tr>
            <td>'.$value["materia"].'</td>';

            $columna ="id";
            $valor = $value["id_docente"];
            $docente=UsuariosC::VerUsuariosC($columna,$valor);

            echo '<td>'.$docente["apellido"].' '.$docente["nombre"].'</td>';

        echo '<td>'.$value["observaciones"].'</td>
            <td><button class="btn btn-success">'.$value["estado"].'</button></td>
 
        </tr>';
         }
         
     }
        
   echo '</tbody>
</table>'; 
            }
        ?>
        
    </div>
</div>
</section>
</div>

<div class="modal fade" id="CrearAula">
    <div class="modal-dialog">
            <div class="modal-content">
                <form  method="post">
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <h2>Carrera:</h2>
                                <select id="select2" class="form-control input-lg" style="width: 100%" name="id_carrera" required="">
                                    <option value="">Seleccionar...</option>
                                    <?php
                                        $resultado=CarrerasC::VerCarrerasC();
                                        foreach ($resultado as $key => $value) {
                                            echo '<option value="'.$value["id"].'">'.$value["nombre"].'</option>';         
                                        }

                                    ?>
                                
                                
                                
                                </select>
                            </div>
                            <div class="form-group">
                                        <input type="hidden" name="id_docente" id="id_docente" value="">
                                        <input type="hidden" name="id" id="id" value="">
                                        <h2>Materia :</h2>
                                        <input type="text" class="form-control input-lg" name="materia" id="materia" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Aprobar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <?php
                    $aula= new AulasC();
                    $aula->CrearAulaC();
                    $aula->ActualizarEstadosSC();
                    ?>
                </form>
            </div>
    </div>
</div>