<?php
    if($_SESSION["rol"]!= "Administrador" ){
       echo '<script>
        window.location ="Inicio";
       </script>';
       return ;
    }

?>
<div class="content-wrapper">
<section class="content-header">
<h1>Aulas Virtuales</h1>
<br>
<form method="post">
<div class="row">
    <div class="col-md-3">
        <h2>Carrera:</h2>
        <select name="id_carrera" id="select2" class="form-control" required="">
        <option value="">Seleccionar...</option>
        <?php
        $resultado=CarrerasC::VerCarrerasC();
        foreach ($resultado as $key => $value) {
            echo '<option value="'.$value["id"].'">'.$value["nombre"].'</option>';
        }

        ?>
        </select>
    </div>
    <div class="col-md-3">
        <h2>Docente:</h2>
        <select name="id_docente" id="select2-1" class="form-control" required="">
        <option value="">Seleccionar...</option>
        <?php
        
        $columna=null;
        $valor=null;

        $resultado=UsuariosC::VerUsuariosC($columna,$valor);
        foreach ($resultado as $key => $value) {
            if($value["rol"] == "Docente"){
               echo '<option value="'.$value["id"].'">'.$value["apellido"].' '.$value["nombre"].'</option>';
            }
        }

        ?>
        </select>
    </div>
    <div class="col-md-3">
    <h2>Materia</h2>
    <input type="text" class="form-control" name="materia" id="aulas" required="">
    </div>
</div>
<br>
<button class="btn btn-primary" type="submit">Crear Nueva Aula</button>
<?php
$crear= new AulasC();
$crear-> CrearAulaC();
?>

</form>



</section>
<section class="content">
<div class="box">
    <div class="box-body">
        <div class="row">
        <?php
        $columna=null;
        $valor=null;
        $resultado1=AulasC::VerAulas1C($columna,$valor);


        $resultado= AulasC::VerAulasC();
        foreach ($resultado as $key => $value) {
          
        echo '<div class="col-lg-3 col-xs-6">';
        
    
        if($value["id_carrera"]==11){
        echo '<div class="small-box bg-green">';
        }
        if($value["id_carrera"]==12){
            echo '<div class="small-box bg-yellow">';
        }
        if($value["id_carrera"]==13){
            echo '<div class="small-box bg-blue">';
        }
        if($value["id_carrera"]==83){
            echo '<div class="small-box bg-purple">';
        }
        if($value["id_carrera"]==84){
            echo '<div class="small-box bg-black">';
        }
       
        if($value["id_carrera"]==85){
            echo '<div class="small-box bg-red">';
        }
    
        echo'  <button class="btn btn-danger pull-right BorrarAula" Aid="'.$value["id"].'" data-toggle="tooltip" title="Eliminar"><i class="fa fa-times"></i></button>

            <div class="inner">
                <h4>'.$value["materia"].'</h4>';
                $columna="id";
                $valor=$value["id_docente"];
                $docente=UsuariosC::VerUsuariosC($columna,$valor);
             echo'   <p>Docente :   '.$docente["apellido"].'   '.$docente["nombre"].'</p>';
            echo '</div>
            <div class="row">
            <div class="col-md-6">
                    <a href="Aula/'.$value["id"].'">
                        <button class="btn btn-primary">Ir al Aula</button>
                    </a>
            </div> 
            </div>
        </div>
    </div>';
        }
        $borrar=new AulasC();
        $borrar->BorrarAulaC();

        ?>  
            
        </div>
    </div>
</div>
</section>
</div>