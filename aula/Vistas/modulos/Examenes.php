
<div class="content-wrapper">
<section class="content-header">
<h1>Gestor de Examenes </h1>
<br>
</section>
<section class="content">
<div class="box">
    <div class="box-body">
        <table class="table table-striped table-hover table-striped dt-responsive ">
            <thead>
                <tr>
                   
                    <th>Nombre del Aula</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            <?php
                  $resultado= AulasC::VerAulasC();
                 foreach($resultado as $key => $value) {
                    if($value["id_docente"]==$_SESSION["id"]){
                     echo ' <tr>
                    
                     <td>'.$value["materia"].'</td>
                     <td>
                     <div class="btn-group">
                         <a href="Ver-Examenes/'.$value["id"].'">
                             <button class="btn btn-success">Ver Examenes</button>
                         </a>                    
                         <a href="Crear-Examenes/'.$value["id"].'">
                             <button class="btn btn-primary">Crear Examenes</button>
                         </a>
                     </div>
                     
                     </td>
                     <td></td>
                 </tr>';
                 }
                }
            ?>
               
            </tbody>
        </table>
    </div>
</div>
</section>
</div>