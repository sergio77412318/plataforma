
<div class="login-box">
    <div class="login-logo">
        <h1>Clases Online</h1>
    </div>
    <div class="login-box-body">
    <p class="login-box-msg">Crear una Nueva Cuenta</p>
    <form method="post">
    <div class="form-group has-feedback">
        <select name="id_carrera" class="form-control" required="">
            <option value=""> Seleccionar su carrera</option>
                <?php
                    $resultado=CarrerasC::VerCarrerasC();
                    foreach ($resultado as $key => $value) {
                        echo '<option value="'.$value["id"].'">'.$value["nombre"].'</option>';
                    }
                    echo '<input type="hidden" name="link" class="form-control" value="Ingresar">';
                ?>
        </select>
    
    </div>
    <div class="form-group has-feedback">
    <input type="text" class="form-control" name="nombre" placeholder="Ingrese su Nombre"  required="">
    </div>
    <div class="form-group has-feedback">
    <input type="text" class="form-control" name="apellido" placeholder="Ingrese sus Apellidos"  required="">
    </div>
    <div class="form-group has-feedback">
    <input type="text" class="form-control" name="documento" placeholder="Ingrese su Documento"  required="">
    </div>
    <div class="form-group has-feedback">
    <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario"  required="">
    </div>
    <div class="form-group has-feedback">
    <input type="password" class="form-control" name="clave" placeholder="Contraseña"  required="">
    <input type="hidden" class="form-control" name="rol" value="Estudiante">

    </div>
    <div class="row">
        <div class="col-xs-6">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Crear Cuenta</button>
        </div>
        <div class="col-xs-6">
        <a href="Ingresar">
        <button type="button" class="btn btn-default btn-block btn-flat">Iniciar Sesion</button>

        </a>
        </div>
    </div>
    <?php
        $crear = new UsuariosC();
       $crear -> CrearUsuarioC();

        ?>
    </form>
    </div>
</div>
