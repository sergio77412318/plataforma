<?php
    $exp=explode("/",$_GET["url"]);

    $columna="id";
    $valor=$exp[1];
    $aula=AulasC::VerAulas2C($columna,$valor);

    if($_SESSION["rol"]=="Estudiante"){
        
            echo '<script>
            window.location="http://localhost/plataforma/aula/Inicio";  
        </script>';
        
    }else if($_SESSION["rol"]=="Docente" && $_SESSION["id"]!=$aula["id_docente"]) {
        echo '<script>
        window.location="http://localhost/plataforma/aula/Mis-Aulas";  
    </script>';
    }
?>


<div class="content-wrapper">
    <section class="content-header">
        <?php
echo '<h1>Listado de Estudiantes Inscriptos en la Materia:<b>'.$aula["materia"].'</b></h1>';
?>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form action="" method="Post">

                    <h2>Fecha :

                        <input type="date" id="" name="fecha" value="">

                    </h2>

                    <table class="table table-hover table-striped table-bordered dt-responsive">
                        <thead>
                            <tr>
                                <th>Apellidos</th>
                                <th>Nombre</th>
                                <th>Documentos</th>
                                <th>Estado</th>

                            </tr>
                        </thead>
                        <style>
                        .sinborde {
                            border: 0;
                        }
                        </style>
                        <tbody>
                            <?php
                    $columna="id_aula";
                    $valor=$exp[1];
                    $resultado=EstudiantesC::VerInscriptosC($columna,$valor);
                    foreach ($resultado as $key => $value) {
                        $columna="id";
                        $valor=$value["id_alumno"];
                        $alumno=UsuariosC::VerUsuariosC($columna,$valor);
                        echo' <tr>
                                <td>'.$alumno["apellido"].'</td>
                                <td>'.$alumno["nombre"].'</td>
                                <td>'.$alumno["documento"].'</td>
                                <td>
                                <select required name="estado[]" style="width:100%"  required>
    
                                <option value="Seleccionar">--Seleccionar--</option>

                           <option value="Presente">Presente</option>
                        
                          
                                <option value="Ausente">Ausente</option>
                        </select></td>
                                <input type="hidden" name="id_alumno[]"  value="'.$valor.'">
                                <input type="hidden" name="id_aula[]"  value="'.$exp[1].'">
                            </tr>';
                    }
                ?>

                        </tbody>

                    </table>
                    <button type="submit" class="btn btn-primary">Agregar Registro</button>

                </form>
                <?php
               echo '<a href="http://localhost/plataforma/aula/InformeA/'.$exp[1].'">
                    <button class="btn btn-success pull-right">Informe de asistencia </button>
                </a>';
                    ?>
                <?php
             $guaradarAsistencia=new TareasC();
             $guaradarAsistencia->AgregarAsistenciaC();
        ?>
            </div>
        </div>
    </section>
</div>

