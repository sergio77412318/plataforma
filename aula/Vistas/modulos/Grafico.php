<?php
$exp=explode("/",$_GET["url"]);

$columna="id";
$valor=$exp[1];


$tarea=TareasC::VerTareaC($columna,$valor);

$valor = $tarea["id_seccion"];

$seccion= SeccionesC::VerSeccionesC($columna,$valor);

$valor=$seccion["id_aula"];

$aula=AulasC::VerAulas2C($columna,$valor);

if ($_SESSION["rol"]  == "Docente" && $aula["id_docente"] !=$_SESSION["id"] ) {
   echo '<script>
            window.location="http://localhost/plataforma/aula/Mis-Aulas";
   </script>';
   return;
}else if($_SESSION["rol"]=="Estudiante"){
        
            echo '<script>
            window.location="http://localhost/plataforma/aula/Aulas-Virtuales";

        </script>';
        
}

?>

<div class="content-wrapper">
    <section class="content-header">
    <?php
    $hola=$exp[1];
    echo '<input type="hidden" value="'.$hola.'">';
    ?>
    <?php
    require_once "graf.php";
    $conexion=conexion();
    $sql="SELECT nota ,estado FROM notas WHERE id_tarea='$hola'  ";
    $result=mysqli_query($conexion,$sql);
    $valoresY=array();
    $valoresX=array();
    while($ver=mysqli_fetch_row($result)){
        $valoresY[]=$ver[1];
        $valoresX[]=$ver[0];
    }
    $datosX=json_encode($valoresX);
    $datosY=json_encode($valoresY);

    ?>

    <script type="text/javascript">
        function crearCadenaBarras(json){
            var parsed=JSON.parse(json);
            var arr=[];
            for(var x in parsed){
                arr.push(parsed[x]);
            }
            return arr;
        }
    </script>


        <?php
echo '<h1>Graficos de la Tarea: <b>'.$tarea["nombre"].'</b></h1>
<h2>Fecha Limite: <b>'.$tarea["fecha_limite"].'</b></h2>';
?>
        <script src="https://cdn.plot.ly/plotly-2.3.1.min.js"></script>

    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
     
                <div id='hola'></div>

            </div>
        </div>
    </section>
</div>



<script type="text/javascript">
    datosX=crearCadenaBarras('<?php echo $datosX ?>');
    datosY=crearCadenaBarras('<?php echo $datosY ?>');

    var data = [{
  values: datosX,
  labels: datosY,
  type: 'pie',
  textinfo: "label+percent"
}];

var layout = {
  height: 400,
  width: 500
  
};

   /*barra var data=[
        {
            x: datosY,
            y: datosX,
            type:'bar'
        }
    ];*/

    /*Lineas  var trace1 = {
  x: [1, 2, 3, 4],
  y: [10, 15, 13, 17],
  type: 'scatter'
};*/

/*var trace2 = {
  x: [1, 2, 3, 4],
  y: [16, 5, 11, 9],
  type: 'scatter'
};

var data = [trace1, trace2];*/
var layout ={
        title: 'Porcentaje de Estudiantes Aprobados y Reprobados de la tarea <?php echo '<b>'.$tarea["nombre"].'</b>'; ?> '

    };
Plotly.newPlot('hola', data,layout);
</script>