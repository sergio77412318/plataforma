


<div class="content-wrapper">

<section class="content">
<div class="box">
    <div class="box-body">
        <form  method="post">
        <?php
            $exp=explode("/",$_GET["url"]);
            $columna="id";
            $valor=$exp[1];
            $aula = AulasC::VerAulas2C($columna,$valor);

            echo '<h2>Crear una fecha de Examen para el aula de: <br><br> <b> '.$aula["materia"].'</b></h2>
                <input type="hidden" name="id_materia" value="'.$exp[1].'">
                <input type="hidden" name="estado" value="1">';
        ?>
        <div class="row">
            <div class="col-md-6 col-xs-12">
            <h2>Hora:</h2>
            <input type="text" class="input-lg" name="hora">

            <h2>Fecha Limite:
                <i class="fa fa-calendar"></i>
                <input type="text" id="datepicker" name="fecha" >
            </h2>
            </div> 

             <div class="col-md-6 col-xs-12">
        
             <?php
             $resultado= AulasC::VerAulasC();
             foreach ($resultado as $key => $value) {
                if($value["id_docente"]==$_SESSION["id"]){
                         $columna="id";
                         $valor=$value["id_docente"];
                         $docente=UsuariosC::VerUsuariosC($columna,$valor);
                }
             }
             
            echo '<input type="hidden" class="input-lg" name="docente_examen" value=" '.$docente["apellido"].'   '.$docente["nombre"].'"     >';
            ?>
          

            <br> <br>
            <button type="submit" class="btn btn-primary btn-lg">Crear Examen</button>
            </div>        
        </div>
        <?php
            $crearE= new ExamenesC();
            $crearE->CrearExamenC();
        ?>
        </form>    
    </div>
</div>
</section>
</div>