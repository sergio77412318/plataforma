<div class="content-wrapper">

<section class="content">
<div class="box">
    <div class="box-body">
        <h1>Certificados</h1>
        <div class="row">
            <div class="col-md-12 cols-xs-12">
                <h2>Solicitudes:</h2>
                <table class="table table-bordered table-hover table-striped dt-responsive">
                    <thead>
                        <tr>
                            <th>N*</th>
                            <th>Estudiante</th>
                            <th>Tipo</th>
                            <th>Estado</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $columna=null;
                            $valor=null;
                            $resultado=CertificadosC::VerCertificadosC($columna,$valor);
                            foreach($resultado as $key=>$value) {
                                if($value["estado"]=="No Impreso"){
                                    echo '<tr>
                                            <td>'.($key+1).'</td>';
                                        $columna="id";
                                        $valor=$value["id_estudiante"];
                                        $estudiante=UsuariosC::VerUsuariosC($columna,$valor);
                                      echo '<td>'.$estudiante["apellido"].' '.$estudiante["nombre"].'</td> 
                                            <td>'.$value["tipo"].'</td>
                                            <td>'.$value["estado"].'</td>';
                                        if($value["tipo"]=="Estudiante"){
                                            echo '<td>
                                                    <a href="http://localhost/plataforma/aula/tcpdf/pdf/C-Alumno.php/'.$estudiante["id"].'" target="_blank">
                                                        <button class="btn btn-primary">Descargar PDF</button>
                                                    </a>
                                                  </td>
                                            <td>
                                                <form method="post">
                                                    <input type="hidden" name="estado" value="Impreso">
                                                    <input type="hidden" name="Eid" value="'.$value["id"].'">
                                                    <button class="btn btn-success" type="submit">Listo</button>

                                                </form>
                                            </td>';
                                        }else{
                                            echo '<td>
                                            <a href="http://localhost/plataforma/aula/tcpdf/pdf/C-Materias.php/'.$estudiante["id"].'" target="_blank">
                                                <button class="btn btn-primary">Descargar PDF</button>
                                            </a>
                                          </td>
                                    <td>
                                        <form method="post">
                                            <input type="hidden" name="estado" value="Impreso">
                                            <input type="hidden" name="Eid" value="'.$value["id"].'">
                                            <button class="btn btn-success" type="submit">Listo</button>

                                        </form>
                                    </td>';
                                        }
                                       echo '
                                       </tr>';
                                }
                            }

                        ?>
                    </tbody>
                
                </table>
                <h2>Solicitudes Listas:</h2>
                <table class="table table-bordered table-hover table-striped dt-responsive">
                    <thead>
                        <tr>
                            <th>N*</th>
                            <th>Estudiante</th>
                            <th>Tipo</th>
                            <th>Estado</th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $columna=null;
                            $valor=null;
                            $resultado=CertificadosC::VerCertificadosC($columna,$valor);
                            foreach($resultado as $key=>$value) {
                                if($value["estado"]=="Impreso"){
                                    echo '<tr>
                                            <td>'.($key+1).'</td>';
                                        $columna="id";
                                        $valor=$value["id_estudiante"];
                                        $estudiante=UsuariosC::VerUsuariosC($columna,$valor);
                                      echo '<td>'.$estudiante["apellido"].' '.$estudiante["nombre"].'</td> 
                                            <td>'.$value["tipo"].'</td>
                                            <td>'.$value["estado"].'</td>';
                                        if($value["tipo"]=="Estudiante"){
                                            echo '<td>
                                                    <a href="http://localhost/plataforma/aula/tcpdf/pdf/C-Alumno.php/'.$estudiante["id"].'" target="_blank">
                                                        <button class="btn btn-primary">Descargar PDF</button>
                                                    </a>
                                                  </td>
                                            ';
                                        }else{
                                            echo '<td>
                                            <a href="http://localhost/plataforma/aula/tcpdf/pdf/C-Materias.php/'.$estudiante["id"].'" target="_blank">
                                                <button class="btn btn-primary">Descargar PDF</button>
                                            </a>
                                          </td>
                                    ';
                                        }
                                       echo '
                                       </tr>';
                                }
                            }

                        ?>
                    </tbody>
                            <?php
                                $estado=new CertificadosC();
                                $estado->CambiarEC();
                            ?>
                </table>
            </div>
        
        </div>
    </div>
</div>
</section>
</div>