<!--12 24-->
<?php
    if($_SESSION["rol"]!="Administrador"){
        echo '<script>
                window.location="Inicio";
                </script>';
                return;
    }
?>
<div class="content-wrapper">
    <section class="content">
        <div class="box">
            <div class="box-body">

            <?php
                $exp=explode("/",$_GET["url"]);
                $columna="id";
                $valor=$exp[1];
                $materia= AulasC::VerMateriasC($columna,$valor);

                echo '<h2>Horarios del Aula:</h2>
                <h1><b>'.$materia["materia"].'</b></h1>';
            ?>
                
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#Horario">Crear Horario</button>
                            <h2>Horarios</h2>
                            <table class="table table-striped table-hover table-striped dt-responsive T">
                                <thead>
                                    <tr>
                                       
                                        <th>Dias</th>
                                        <th>Horarios</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                    <tbody>
                                    <?php
                                        $columna="id_aula";
                                        $valor=$exp[1];
                                        $horarios=Aulasc::VerHorariosC($columna,$valor);
                                        foreach($horarios as $key=>$value){
                                            echo ' <tr>
                                            
                                            <td>'.$value["dias"].'</td> 
                                            <td>'.$value["horario"].'</td>
                                            <td>
                                            <a href="http://localhost/plataforma/aula/tcpdf/pdf/Inscriptos-Materia.php/'.$exp[1].'/'.$value["id"].'" target="blank">
                                                <button class="btn btn-primary">Generar PDF</button>
                                            </a>  
                                                <button class="btn btn-danger BorrarHorario" Cid="'.$value["id"].'" Mid="'.$exp[1].'">Borrar Horario</button>
                                            </td>
                                            <td></td>
                                       </tr> ';
                                        }
                                    ?>
                                      
                                    </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="Horario">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post">
                <div class="box-body">
                    <div class="form-group">
                        <h2>Dias</h2>
                        <input type="text" class="form-control input-lg" name="dias" required="">
                        <?php echo '<input type="hidden" class="form-control input-lg" name="id_aula" value="'.$exp[1].'" required="">'; ?>
                    </div> 
                    <div class="form-group">
                        <h2>Horario</h2>
                        <input type="text" class="form-control input-lg" name="horario" required="">
                    </div>                
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Crear</button>
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                </div>
                <?php
                    $CrearH= new AulasC();
                    $CrearH-> CrearHorario();
                ?>
            </form>
        </div>
    </div>
</div>
<?php
                    $BorrarH= new AulasC();
                    $BorrarH-> BorrarHorarioC();
                ?>