<?php
  $exp=explode("/",$_GET["url"]);

  $columna="id";
  $valor=$exp[1];
  $aula=AulasC::VerAulas2C($columna,$valor);

  if($_SESSION["rol"]=="Estudiante"){
      
          echo '<script>
          window.location="http://localhost/plataforma/aula/Inicio";  
      </script>';
      
  }else if($_SESSION["rol"]=="Docente" && $_SESSION["id"]!=$aula["id_docente"]) {
      echo '<script>
      window.location="http://localhost/plataforma/aula/Mis-Aulas";  
  </script>';
  }
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php
echo '<h1>Creacion de Ciclos para Maestria:<b>'.$aula["materia"].'</b></h1>';
?>

    </section>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <form method="post">
                <div class="col-md-3">
                <select name="materiaS" id="select2-1" class="form-control" required="">
                            <option value="">Seleccionar Ciclo</option>
                            <option value="Primer Ciclo">Primer Ciclo</option>
                            <option value="Segundo Ciclo">Segundo Ciclo</option>
                            <option value="Tercer Ciclo">Tercer Ciclo</option>
                            <option value="Cuarto Ciclo">Cuarto Ciclo</option>
                            <option value="Quinto Ciclo">Quinto Ciclo</option>
                            <option value="Sexto Ciclo">Sexto Ciclo</option>
                            <option value="Septimo Ciclo">Septimo Ciclo</option>
                            <option value="Octavo Ciclo">Octavo Ciclo</option>
                            <option value="Noveno Ciclo">Noveno Ciclo</option>
                            <option value="Decimo Ciclo">Decimo Ciclo</option>

                        </select>
                </div>
                    <div class="col-md-5 col-xs-12">

                   



                        <input type="text" class="form-control" name="semestre" id="semestre"
                            placeholder="Ingresar Nueva Materia">
                        <?php
                    echo '
                    <input type="hidden" name="id_aula"  value="'.$exp[1].'">';
                    ?>
                    </div>
                    <button type="submit" class="btn btn-primary">Agregar Materia</button>
                    <?php
                    $crear= new CarrerasC();
                    $crear-> CrearCicloC();
                   
                ?>
                </form>
                <?php
               echo '<a href="http://localhost/plataforma/aula/tcpdf/pdf/Ciclo.php/'.$exp[1].'" target="_blank">
                <button class="btn btn-success pull-right">Exportar a PDF plan de estudio </button>   
               </a>';
                ?>

            </div>
            <div class="box-body">
                <table class="table table-hover table-bordered table-striped dt-responsive">
                    <thead>
                        <tr>
                            <th>Ciclo</th>
                            <th>Materia</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?PHP /* $columna="id_aula";
                    $valor=$exp[1];
                    $resultado=EstudiantesC::VerInscriptosC($columna,$valor);
                    foreach ($resultado as $key => $value) {
                        $columna="id";
                        $valor=$value["id_alumno"];
                        $alumno=UsuariosC::VerUsuariosC($columna,$valor);
                        echo' <tr>
                                <td>'.$alumno["apellido"].'</td>
                                <td>'.$alumno["nombre"].'</td>
                                <td>'.$alumno["documento"].'</td>
                                <td>
                                <select required name="estado[]" style="width:100%"  required>
    
                                <option value="Seleccionar">--Seleccionar--</option>

                           <option value="Presente">Presente</option>
                        
                          
                                <option value="Ausente">Ausente</option>
                        </select></td>
                                <input type="hidden" name="id_alumno[]"  value="'.$valor.'">
                                <input type="hidden" name="id_aula[]"  value="'.$exp[1].'">
                            </tr>';
                    }*/
                    ?>

                        <?php
                    $columna="id_aula";
                    $valor=$exp[1];


                    $resultado=EstudiantesC::VerCicloC($columna,$valor);
                    foreach($resultado as $key=>$value){
                      echo ' <tr>
                            <td>'.$value["semestre"].'</td>
                            <td>'.$value["materia"].'</td>
                            <td>
                            <form method="post">
                            <div class="btn-group">
                                
                                
                                <input type="hidden" name="id_semestre" value="'.$value['id'].'">
                                <button class="btn btn-danger " type="submit" data-toggle="tooltip" title="Eliminar Ciclo">Eliminar Ciclo</button>
                               
                                
                               
                            </div>
                            </form>
                            
                           
                            </td>
                            <td>
                            <div class="btn-group">
                            <a href="../editar-C/'.$value["id"].'">
                            <button class="btn btn-success">Editar</button>
                        </a>
                  
                     
                </div>
                            </td>
                        </tr>';/*  <a href="../Plan/'.$value["id"].'">
                        <button class="btn btn-primary">Crear plan de estudio</button>
                    </a>*/
                    }
                    
                    $borrarArch=new SeccionesC();
                    $borrarArch->borrarCicloC();
                
                ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>