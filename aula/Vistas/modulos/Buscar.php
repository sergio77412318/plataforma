<div class="content-wrapper">
<section class="content-header">
<h1>Gestion de Aulas</h1>

</section>
<section class="content">
<div class="box">
    <div class="box-body">
    <table class="table table-bordered table-hover table-striped dt-responsive">
            <thead>
                <tr>
                <th>Docente</th>
                <th>Materia</th>
                <th>Carrera</th>
                <th>Foto</th>
                <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $exp=explode("/",$_GET["url"]);
                    $resultado= AulasC::VerAulasC();
                    foreach ($resultado as $key => $value) {
                      
                            $columna="id";
                            $valor=$value["id_docente"];
                            $docente=UsuariosC::VerUsuariosC($columna,$valor);
                            echo' <tr>  <td>   '.$docente["apellido"].'   '.$docente["nombre"].'</td>';
                            echo '

                            <td>'.$value["materia"].'</td>';
                            $carrera=CarrerasC::VerCarrerasC();
                            foreach ($carrera as $key => $c) {
                                if($c["id"]==$value["id_carrera"]){
                                    echo '<td>'.$c["nombre"].'</td>';
                                }
                            }
                            echo '<td> <img height="50px" class="card-img-top" src="'.$docente["foto"].'" alt="Image"></td>';
                           
                            echo '<td>
                            <button class="btn btn-danger  BorrarAula" Aid="'.$value["id"].'" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash"></i></button>
                            
                            <a href="Aula/'.$value["id"].'">
                            <button class="btn btn-primary" data-toggle="tooltip" title="Ver Aula"><i class="fa fa-eye"></i></button>
                                 </a>

                                 <a href="http://localhost/plataforma/aula/Horario/'.$value["id"].'">
                                 <button class="btn btn-warning  "  data-toggle="tooltip" title="Crear Horario"><i class="fa fa-address-book"></i></button>
                                 </a>

                                 
                                 
                                 

                                 <a href="http://localhost/plataforma/aula/calendario/calendario.php">
                                 <button class="btn btn-primary"  data-toggle="tooltip" title="Crear actividad"><i class="fa fa-calendar"></i></button>
                                 </a>

                                 <a href="http://localhost/plataforma/aula/modulos/'.$value["id"].'">
                                 <button class="btn btn-danger"  data-toggle="tooltip" title="Crear Modulos"><i class="fa fa-id-card"></i></button>
                                 </a>

                                 <a href="http://localhost/plataforma/aula/maestria/'.$value["id"].'">
                                 <button class="btn btn-info"  data-toggle="tooltip" title="Maestria"><i class="fa fa-book"></i></button>
                                 </a>

                                 <a href="http://localhost/plataforma/aula/estudio/'.$value["id"].'">
                                 <button class="btn btn-success"  data-toggle="tooltip" title="Crear plan de estudio"><i class="fa fa-globe"></i></button>
                                 </a>

                                 <a href="http://localhost/plataforma/aula/tcpdf/pdf/Todos.php/'.$value["id"].'"  target="_blank">
                                 <button class="btn btn-info"  data-toggle="tooltip" title="Ver los esrudiantes Inscriptos"><i class="fa fa-user"></i></button>
                                 </a>
                                 
                                 <a href="http://localhost/plataforma/aula/Grafica-Admin/'.$value["id"].'">
                                 <button class="btn btn-danger"  data-toggle="tooltip" title="Ver graficas del aula "><i class="fa fa-pie-chart"></i></button>
                                 </a>
                                </td>';

                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
</section>
</div>
