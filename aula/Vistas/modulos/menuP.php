<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <ul class="sidebar-menu">
            <li>
                <a href="http://localhost/plataforma/aula/Inicio">
                <i class="fa fa-home"></i>
                <span>Inicio</span>
                </a>
            </li>
            
            
            <li>
                <a href="http://localhost/plataforma/aula/Mis-Aulas">
                <i class="fa fa-university"></i>
                <span>Mis Aulas</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-ul"></i>
                    <span>Solicitar Aulas Viruales</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="http://localhost/plataforma/aula/Pedir-Aula">
                        <i class="fa fa-edit"></i>
                        <span>Pedir Nueva Aula</span>
                        </a>
                    </li>

                    <li>
                        <a href="http://localhost/plataforma/aula/Solicitudes">
                        <i class="fa fa-check-square-o"></i>
                        <span>Ver Solicitudes</span>
                        </a>
                    </li>
                </ul>
            </li>
            
           <!-- <li>
                <a href="http://localhost/plataforma/aula/">
                <i class="fa fa-envelope"></i>
                <span>Mensajes</span>
                </a>
            </li>-->
            <li>
            <a href="http://localhost/plataforma/aula/Examenes">
                <i class="fa fa-file"></i>
                <span>Examenes</span>
                </a>
            </li>

            <li>
            <a href="http://localhost/plataforma/aula/../comunidad/Registro.php">
                <i class="fa fa-users"></i>
                <span>Comunidad feedback</span>
                </a>
            </li>
            <li>
                <a href="http://localhost/plataforma/aula/Videoconferencia">
                <i class="fa fa-film"></i>    
                <span>Videoconferencia</span>
                </a>
            </li>
            <li>
    <a href="http://localhost/plataforma/aula/../examenes">
    <i class="fa fa-book"></i>    
    <span>Sistema de examenes</span>
    </a>
    </li>

            <li>
    <a href="http://localhost/plataforma/aula/foro1/index.php" >
    <i class="fa fa-coffee"></i>    
    <span>Ir al foro</span>
    </a>
    </li>

            <li>
                <a href="http://localhost/plataforma/aula/calendario1/calendario.php">
                <i class="fa fa-calendar"></i>    
                <span>Calendario</span>
                </a>
            </li>
           
           <!-- <li class="nav-item dropdown">
                      <a href="http://localhost/plataforma/aula/cts_qr/admin/people/index.php" class="nav-link nav-people">
                        <i class="fa fa-calendar"></i>
                        <span>Registro De asistencia</span>
                      </a>
                    </li>-->

    </ul>     
        
        </ul>     
    </section>
    <!-- /.sidebar -->
  </aside>