<div class="content-wrapper">
<section class="content-header">
<h1>Videoconferencia</h1>

</section>
<section class="content">
<div class="box">
    <div class="box-body">
            <button id="start" type="button" class="btn btn-primary" >Iniciar Videoconferencia</button>
                <div id="jitsi-container">
                </div>
                <script>
  var button = document.querySelector('#start');
  var container = document.querySelector('#jitsi-container');
  var api = null;
  
  button.addEventListener('click', () => {
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var stringLength = 30;
  
      function pickRandom() {
      return possible[Math.floor(Math.random() * possible.length)];
      }
  
  var randomString = Array.apply(null, Array(stringLength)).map(pickRandom).join('');
  
      var domain = "meet.jit.si";
      var options = {
          "roomName": randomString,
          "parentNode": container,
          "width": 600,
          "height": 600,
      };
      api = new JitsiMeetExternalAPI(domain, options);
  });
  
  </script>
    </div>
</div>
</section>
</div>