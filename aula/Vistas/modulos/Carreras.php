<?php
if($_SESSION["rol"]!="Administrador"){
    echo '<script>
    
    window.location="Inicio";
    </script>';
    return;
}

?>
<div class="content-wrapper">
<section class="content-header">
<h1>Gestor de Carreras</h1>

</section>
<section class="content">
<div class="box">
        <div class="box-header">
            <form  method="post" >
                <div class="col-md-6 col-xs-12">
                    <input type="text" class="form-control" name="carrera" id="carrera"  placeholder="Ingresar Nueva Carrera">
                </div>
                <button type="submit" class="btn btn-primary" >Agregar Carrera</button>
                <?php
                    $crear= new CarrerasC();
                    $crear-> CreaCarrerasC();
                   
                ?>
            </form>

        </div>
    <div class="box-body">
        <table class="table table-hover table-bordered table-striped dt-responsive">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $columna=null;
                    $valor=null;
                    $resultado1=CarrerasC::VerCarreras1C($columna,$valor);


                    $resultado=CarrerasC::VerCarrerasC();
                    foreach($resultado as $key=>$value){
                      echo ' <tr>
                            <td>'.$value["id"].'</td>
                            <td>'.$value["nombre"].'</td>
                            <td>
                            <div class="btn-group">
                                <a href="Editar-Carrera/'.$value["id"].'">
                                    <button class="btn btn-success">Editar</button>
                                </a>
                               
                                <a href="Carreras/'.$value["id"].'">
                                    <button class="btn btn-danger">Eliminar</button>
                                </a>
                               
                                <a href="Estudiantes/'.$value["id"].'">
                                    <button class="btn btn-primary">Estudiantes</button>
                                </a>
                            </div>

                            </td>
                        </tr>';
                    }
                    
                ?>
            </tbody>
        </table>
    </div>
</div>
</section>
</div>
<?php
$borrar= new CarrerasC();
$borrar->BorrarCarreraC();
?>