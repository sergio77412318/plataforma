<!-- Navbar -->
<?php
//Carreras
require_once "../Controladores/CarrerasC.php";
require_once "../Modelos/CarrerasM.php";
?>
  <nav class="main-header navbar navbar-expand navbar-primary navbar-dark bg-primary ">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
     
     <li class="nav-item">
     <div class="navbar-custom-menu">
       <ul class="nav navbar-nav">
       
         <li class="dropdown user user-menu">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <?php
               if($_SESSION["foto"]== ""){
                     echo '<img src="http://localhost/plataforma/aula/Vistas/img/1.jpg" class="user-image" alt="User Image">';

               }else{
                 echo '<img src="http://localhost/plataforma/aula/'.$_SESSION["foto"].'" class="user-image" alt="User Image">';
               }
               echo '<span class="hidden-xs text-white ">'. $_SESSION["apellido"].' '.$_SESSION["nombre"].'</span>';
           ?>
           </a>
           <ul class="dropdown-menu">
             <!-- User image -->
             <li class="user-header" style="height: 100px">
               <?php
                 echo '<p >
                 '.$_SESSION["apellido"].' '.$_SESSION["nombre"].'';
                 if($_SESSION["id_carrera"]==0){
                    echo '<small>'.$_SESSION["rol"].'</small>';
                 }else{
                   $resultado=CarrerasC::VerCarrerasC();
                   foreach ($resultado as $key => $value) {
                     if($value["id"]==$_SESSION["id_carrera"]){
                       echo '<small>'.$value["nombre"].'</small>';
                     }
                   }
                 }
               echo '</p>';
               ?>
               
             </li>
             
             <!-- Menu Footer-->
             <style>
               .pull-left {
 float: left !important;
}
.pull-right {
 float: right !important;
}
             </style>
             <li class="user-footer">
               <div class="pull-left">
                 <a href="http://localhost/plataforma/aula/Mis-Datos" class="btn btn-primary btn-flat">Mis datos</a>
               </div>
               <div class="pull-right">
                 <a href="http://localhost/plataforma/aula/Salir" class="btn btn-danger btn-flat">Salir</a>
               </div>
             </li>
           </ul>
         </li>
         
       </ul>
     </div>
     </li>
   </ul>
   
  </nav>
  <!-- /.navbar -->
