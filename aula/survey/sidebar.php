  <aside class="main-sidebar sidebar-dark-primary elevation-4 ">
  <style>
    .titulo{
        margin-bottom: 0;
  background-color: #FFFFFF;
  z-index: 9999;
  border: 0;
  font-size: 12px !important;
  line-height: 1.42857143 !important;
  letter-spacing: 4px;
  border-radius: 0;
  font-family: Montserrat, sans-serif;
}
  </style>
  <div class="dropdown titulo">
   	<a href="http://localhost/plataforma/aula/Inicio" class="brand-link "  >
        <center><span class=" brand-text font-weight-light">AULA VIRTUAL</span></center>

      </a>
     
    </div>
      <div class="sidebar">

          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu"
                  data-accordion="false">

                  <?php if($_SESSION['rol'] == "Administrador"): ?>
                  


                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Inicio" class="nav-link ">
                          <i class="nav-icon fa fa-home"></i>
                          <p>Inicio</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Usuarios" class="nav-link ">
                          <i class="nav-icon fa fa-user-circle"></i>
                          <p>Usuarios</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Carreras" class="nav-link ">
                          <i class="nav-icon fa fa-graduation-cap"></i>
                          <p>Carreras</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Estudiantes" class="nav-link ">
                          <i class="nav-icon fa fa-book"></i>
                          <p>Estudiantes</p>
                      </a>
                  </li>





                  <li class="nav-item">
                      <a href="#" class="nav-link nav-is-tree   ">
                      <i class="nav-icon fa fa-poll-h"></i>
                          <p>
                          Aulas Virtuales
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="http://localhost/plataforma/aula/Aulas" class="nav-link  tree-item">
                                  <i class="fa fa-university nav-icon"></i>
                                  <p>Crear Aulas</p>
                              </a>
                          </li>

                          <li class="nav-item">
                              <a href="http://localhost/plataforma/aula/Buscar" class="nav-link  tree-item">
                                  <i class="fa fa-check-square nav-icon"></i>
                                  <p>Gestion de Aulas</p>
                              </a>
                          </li>
                      </ul>
                  </li>










                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Solicitudes" class="nav-link ">
                          <i class="fa fa-edit nav-icon"></i>
                          <p>Solicitud de aulas</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Certificados" class="nav-link ">
                          <i class="fa fa-address-card nav-icon"></i>
                          <p>Certificados</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/../comunidad/Registro.php"
                          class="nav-link ">
                          <i class="fa fa-users nav-icon"></i>
                          <p>Comunidad feedback</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/Videoconferencia"
                          class="nav-link ">
                          <i class="fa fa-film nav-icon"></i>
                          <p>Videoconferencia</p>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link nav-is-tree  ">
                          <i class="nav-icon fa fa-poll-h"></i>
                          <p>
                          Foros
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                          <a href="http://localhost/plataforma/aula/foro" class="nav-link  tree-item">
                          <i class="fa fa-plus-circle nav-icon"></i>
                          <p>Crear categoria de Foros</p>
                              </a>
                          </li>
                          <li class="nav-item">
                          <a href="http://localhost/plataforma/aula/Foroestu" class="nav-link  tree-item">
                          <i class="fa fa-book nav-icon"></i>
                          <p>Lista de categorias de Foros</p>
                              </a>
                          </li>
                          <li class="nav-item">
                          <a href="http://localhost/plataforma/aula/foro1/index.php"  class="nav-link  tree-item">
                          <i class="fa fa-coffee nav-icon"></i>
                          <p>Ir al foro</p>
                              </a>
                          </li>
                      </ul>
                  </li>



                          













                  <li class="nav-item">
                      <a href="#" class="nav-link nav-is-tree   ">
                          <i class="nav-icon fa fa-question"></i>
                          <p>
                              Encuestas
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="./index.php?page=new_survey" class="nav-link  tree-item">
                                  <i class="fas fa-plus-circle nav-icon"></i>
                                  <p>Agregar nueva</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="./index.php?page=survey_list" class="nav-link  tree-item">
                                  <i class="fas fa-book nav-icon"></i>
                                  <p>Lista</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="./index.php?page=survey_report" class="nav-link  tree-item">
                                  <i class="fas fa-info nav-icon"></i>
                                  <p> Reporte de encuesta</p>
                              </a>
                          </li>
                      </ul>
                  </li>



                 

                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/../examenes" class="nav-link ">
                          <i class="fa fa-book nav-icon"></i>
                          <p>Sistema de examenes</p>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a href="http://localhost/plataforma/aula/backup/php/index.php" class="nav-link ">
                          <i class="fa fa-database nav-icon"></i>
                          <p>Backup de la base de datos</p>
                      </a>
                  </li>



                  <?php else: ?>
                    <li class="nav-item">
    <a href="http://localhost/plataforma/aula/Inicio" class="nav-link nav-survey_report">
    <i class=" nav-icon fa fa-home"></i>
    <p>Inicio</p>
    </a>
    </li>
    <li>
    </li>
    <li>
    
    </li>
    <li class="nav-item">
    <a href="http://localhost/plataforma/aula/Aulas-Virtuales" class="nav-link nav-survey_report">
    <i class="nav-icon fa fa-university"></i>
    <p>Aulas Virtuales</p>
    </a>
    </li>
    <li>
    <!--<a href="http://localhost/plataforma/aula/Mensajes">
    <i class="fa fa-envelope"></i>
    <span>Mensajes</span>
    <span class="pull-right-container">
        <?php
           // $resultado=MensajesC::SinLeerC

        ?>


        <small class="label pull-right bg-blue" data-toggle="tooltip" title="Sin leer"></small>
    </span>

    </a>-->
    </li>


    <li class="nav-item">
                      <a href="#" class="nav-link nav-is-tree nav-edit_survey nav-view_survey">
                          <i class="nav-icon fa fa-list-ul"></i>
                          <p>
                          Certificados
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item ">
                              <a href="http://localhost/plataforma/aula/Constancia-Estudiante" class="nav-link  tree-item">
                                  <i class="fa fa-edit nav-icon"></i>
                                  <p>Constancia de Estudiante</p>
                              </a>
                          </li>
                         
                      </ul>
                  </li>
    

    <li>
    

  






    <li class="nav-item">
    <a href="http://localhost/plataforma/aula/../comunidad/Registro.php" class="nav-link nav-survey_report">
    <i class="nav-icon fa fa-users"></i>
    <p>Comunidad feedback</p>
    </a>
    </li>
    <li class="nav-item">
    <a href="http://localhost/plataforma/aula/Videoconferencia" class="nav-link nav-survey_report">
    <i class=" nav-icon fa fa-film"></i>    
    <p>Videoconferencia</p>
    </a>
    </li>



    <li  class="nav-item">
    <a href="http://localhost/plataforma/aula/../examenes" class="nav-link nav-survey_report">
    <i class=" nav-icon fa fa-book"></i>    
    <p>Sistema de examenes</p>
    </a>
    </li>

    <li class="nav-item">
    <a href="http://localhost/plataforma/aula/foro1/index.php" class="nav-link " >
    <i class="nav-icon fa fa-coffee"></i>    
    <p>Ir al foro</p>
    </a>
    </li>


   
                  <li class="nav-item">
                      <a href="./index.php?page=survey_widget" class="nav-link  ">
                          <i class="nav-icon fas fa-book"></i>
                          <p>
                              Lista de Encuestas
                          </p>
                      </a>
                  </li>
                 
                  <?php endif; ?>
              </ul>
          </nav>
      </div>
  </aside>
  <script>
$(document).ready(function() {
    var page = '<?php echo isset($_GET['page']) ? $_GET['page'] : 'home' ?>';
    if ($('.nav-link.nav-' + page).length > 0) {
        $('.nav-link.nav-' + page).addClass('active')
        console.log($('.nav-link.nav-' + page).hasClass('tree-item'))
        if ($('.nav-link.nav-' + page).hasClass('tree-item') == true) {
            $('.nav-link.nav-' + page).closest('.nav-treeview').siblings('a').addClass('active')
            $('.nav-link.nav-' + page).closest('.nav-treeview').parent().addClass('menu-open')
        }
        if ($('.nav-link.nav-' + page).hasClass('nav-is-tree') == true) {
            $('.nav-link.nav-' + page).parent().addClass('menu-open')
        }

    }
    $('.manage_account').click(function() {
        uni_modal('Manage Account', 'manage_user.php?id=' + $(this).attr('data-id'))
    })
})
  </script>