<?php
session_start();

require_once('bdd.php');


$sql = "SELECT id, title,description, start, end, color FROM calendario ";

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Plataforma Virtual </title>
    <link rel="shortcut icon" href="../Vistas/img/2.png" />
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FullCalendar -->
    <link href='css/fullcalendar.css' rel='stylesheet' />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- Theme style -->
    <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <!-- jvectormap -->
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor 
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
-->
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="http://localhost/plataforma/aula/Vistas/sweetalert2/sweetalert2.all.js"></script>
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet"
        href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">






</head>

<body class="hold-transition skin-blue sidebar-mini login-page">
    <header class="main-header">
        <!-- Logo -->
        <a href="http://localhost/plataforma/aula/Inicio" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b class="fa fa-university"></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">Aula Virtual</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php
                if($_SESSION["foto"]== ""){
                      echo '<img src="http://localhost/plataforma/aula/Vistas/img/1.jpg" class="user-image" alt="User Image">';

                }else{
                  echo '<img src="http://localhost/plataforma/aula/'.$_SESSION["foto"].'" class="user-image" alt="User Image">';
                }
                echo '<span class="hidden-xs">'. $_SESSION["apellido"].' '.$_SESSION["nombre"].'</span>';
            ?>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header" style="height: 100px">
                                <?php
                  echo '<p>
                  '.$_SESSION["apellido"].' '.$_SESSION["nombre"].'';
                  if($_SESSION["id_carrera"]==0){
                     echo '<small>'.$_SESSION["rol"].'</small>';
                  }else{
                    $resultado=CarrerasC::VerCarrerasC();
                    foreach ($resultado as $key => $value) {
                      if($value["id"]==$_SESSION["id_carrera"]){
                        echo '<small>'.$value["nombre"].'</small>';
                      }
                    }
                  }
                echo '</p>';
                ?>

                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="http://localhost/plataforma/aula/Mis-Datos"
                                        class="btn btn-primary btn-flat">Mis datos</a>
                                </div>
                                <div class="pull-right">
                                    <a href="http://localhost/plataforma/aula/Salir"
                                        class="btn btn-danger btn-flat">Salir</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Calendario de actividades</h1>

        </section>
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <div id="calendar">
                    </div>
                </div>
        </section>
    </div>







    <!-- Modal -->
    <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" method="POST" action="editEventTitle.php">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Evento</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Descripcion del Evento</label>
                            <div class="col-sm-10">
                                <input type="text"  name="title" class="form-control" id="title" disabled>
                            </div>
                        </div>

                        <div class="form-group">
					<label for="description" class="col-sm-2 control-label">Descripcion</label>
					<div class="col-sm-10">
                    
					  <input type="text" name="description" class="form-control" id="description"  disabled>
					</div>
				  </div>  
                        <div class="form-group">

                            <div class="col-sm-10">

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">

                            </div>
                        </div>

                        <input type="hidden" name="id" class="form-control" id="id">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                    </div>
                </form>
            </div>
        </div>
    </div>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- FullCalendar -->
    <script src='js/moment.min.js'></script>
    <script src='js/fullcalendar/fullcalendar.min.js'></script>
    <script src='js/fullcalendar/fullcalendar.js'></script>
    <script src='js/fullcalendar/locale/es.js'></script>


    <script>
    $(document).ready(function() {

        var date = new Date();
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth() + 1).toString().length == 1 ? "0" + (date.getMonth() + 1).toString() : (date
            .getMonth() + 1).toString();
        var dd = (date.getDate()).toString().length == 1 ? "0" + (date.getDate()).toString() : (date.getDate())
            .toString();

        $('#calendar').fullCalendar({
            header: {
                language: 'es',
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay',

            },
            defaultDate: yyyy + "-" + mm + "-" + dd,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            selectable: true,
            selectHelper: true,
            select: function(start, end) {

                $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                $('#ModalAdd').modal('show');
            },
            eventRender: function(event, element) {
                element.bind('dblclick', function() {
                    $('#ModalEdit #id').val(event.id);
                    $('#ModalEdit #title').val(event.title);
                    $('#ModalEdit #description').val(event.description);
                    $('#ModalEdit #color').val(event.color);
                    $('#ModalEdit').modal('show');
                });
            },
            eventDrop: function(event, delta, revertFunc) { // si changement de position

                edit(event);

            },
            eventResize: function(event, dayDelta, minuteDelta,
            revertFunc) { // si changement de longueur

                edit(event);

            },
            events: [
                <?php foreach($events as $event): 
			
				$start = explode(" ", $event['start']);
				$end = explode(" ", $event['end']);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $event['start'];
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $event['end'];
				}
			?> {
                    id: '<?php echo $event['id']; ?>',
                    title: '<?php echo $event['title']; ?>',
                    description: '<?php echo $event['description']; ?>',
                    start: '<?php echo $start; ?>',
                    end: '<?php echo $end; ?>',
                    color: '<?php echo $event['color']; ?>',
                },
                <?php endforeach; ?>
            ]
        });

        function edit(event) {
            start = event.start.format('YYYY-MM-DD HH:mm:ss');
            if (event.end) {
                end = event.end.format('YYYY-MM-DD HH:mm:ss');
            } else {
                end = start;
            }

            id = event.id;

            Event = [];
            Event[0] = id;
            Event[1] = start;
            Event[2] = end;

            $.ajax({
                url: 'editEventDate.php',
                type: "POST",
                data: {
                    Event: Event
                },
                success: function(rep) {
                    if (rep == 'OK') {
                        alert('Evento se ha guardado correctamente');
                    } else {
                        alert('No se pudo guardar. Inténtalo de nuevo.');
                    }
                }
            });
        }

    });
    </script>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <ul class="sidebar-menu">
                <li>
                    <a href="http://localhost/plataforma/aula/Inicio">
                        <i class="fa fa-home"></i>
                        <span>Inicio</span>
                    </a>
                </li>


                <li>
                    <a href="http://localhost/plataforma/aula/Mis-Aulas">
                        <i class="fa fa-university"></i>
                        <span>Mis Aulas</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-list-ul"></i>
                        <span>Solicitar Aulas Viruales</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="http://localhost/plataforma/aula/Pedir-Aula">
                                <i class="fa fa-edit"></i>
                                <span>Pedir Nueva Aula</span>
                            </a>
                        </li>

                        <li>
                            <a href="http://localhost/plataforma/aula/Solicitudes">
                                <i class="fa fa-check-square-o"></i>
                                <span>Ver Solicitudes</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- <li>
                <a href="http://localhost/plataforma/aula/">
                <i class="fa fa-envelope"></i>
                <span>Mensajes</span>
                </a>
            </li>-->
                <li>
            <a href="http://localhost/plataforma/aula/Examenes">
                <i class="fa fa-file"></i>
                <span>Examenes</span>
                </a>
            </li>

                <li>
                    <a href="http://localhost/plataforma/aula/../comunidad/Registro.php">
                        <i class="fa fa-users"></i>
                        <span>Comunidad feedback</span>
                    </a>
                </li>
                <li>
                    <a href="http://localhost/plataforma/aula/Videoconferencia">
                        <i class="fa fa-film"></i>
                        <span>Videoconferencia</span>
                    </a>
                </li>
                <li>
    <a href="http://localhost/plataforma/aula/../examenes">
    <i class="fa fa-book"></i>    
    <span>Sistema de examenes</span>
    </a>
    </li>

                <li>
    <a href="http://localhost/plataforma/aula/foro1/index.php" >
    <i class="fa fa-coffee"></i>    
    <span>Ir al foro</span>
    </a>
    </li>

                <li>
                    <a href="http://localhost/plataforma/aula/calendario1/calendario.php">
                        <i class="fa fa-calendar"></i>
                        <span>Calendario</span>
                    </a>
                </li>




                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=""></i>
                        <span></span>
                    </a>
                </li>



            </ul>
        </section>

    </aside>

</body>

</html>






<script src="http://localhost/plataforma/aula/Vistas/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js">
</script>
-->
<!-- Slimscroll -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/jquery-slimscroll/jquery.slimscroll.min.js">
</script>
<!-- FastClick -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="http://localhost/plataforma/aula/Vistas/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="http://localhost/plataforma/aula/Vistas/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="http://localhost/plataforma/aula/Vistas/dist/js/demo.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net/js/jquery.dataTables.min.js">
</script>


<script src="http://localhost/plataforma/aula/Vistas/js/hola.js"></script>