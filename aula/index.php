<?php
require_once "Controladores/plantillaC.php";
//Usuarios
require_once "Controladores/UsuariosC.php";
require_once "Modelos/UsuariosM.php";
//Carreras
require_once "Controladores/CarrerasC.php";
require_once "Modelos/CarrerasM.php";
//aulas
require_once "Controladores/AulasC.php";
require_once "Modelos/AulasM.php";
//estudiantes
require_once "Controladores/EstudiantesC.php";
require_once "Modelos/EstudiantesM.php";
//Secciones
require_once "Controladores/SeccionesC.php";
require_once "Modelos/SeccionesM.php";
//Tareas
require_once "Controladores/TareasC.php";
require_once "Modelos/TareasM.php";
//examenes
require_once "Controladores/ExamenesC.php";
require_once "Modelos/ExamenesM.php";
//certificados
require_once "Controladores/CertificadosC.php";
require_once "Modelos/CertificadosM.php";
//mensajes
require_once "Controladores/MensajesC.php";
require_once "Modelos/MensajesM.php";
//Inicio
require_once "Controladores/InicioC.php";
require_once "Modelos/InicioM.php";


$plantilla =new Plantilla();
$plantilla -> LlamarPlantilla();

?>