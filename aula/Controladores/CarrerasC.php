<?php
class CarrerasC{
    public function CreaCarrerasC(){
        if(isset($_POST["carrera"])){
            $tablaBD="carreras";
            $carrera=$_POST["carrera"];
            $resultado=CarrerasM::CreaCarreraM($tablaBD,$carrera);

            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La Carrera se ha Creado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Carreras ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function CrearSemestreC(){
        if(isset($_POST["semestre"])){
            $tablaBD="semestre";
            $exp=explode("/",$_GET["url"]);
            $carrera=$_POST["semestre"];
            $materiaS=$_POST["materiaS"];
            $id_aula=$_POST["id_aula"];
           
            $resultado=CarrerasM::CrearSemestreM($tablaBD,$carrera,$materiaS,$id_aula);

            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"El semestre se ha Creado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/estudio/'.$exp[1].' ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function CrearModuloC(){
        if(isset($_POST["semestre"])){
            $tablaBD="modulos";
            $exp=explode("/",$_GET["url"]);
            $carrera=$_POST["semestre"];
            $materiaS=$_POST["materiaS"];
            $id_aula=$_POST["id_aula"];
           
            $resultado=CarrerasM::CrearModuloM($tablaBD,$carrera,$materiaS,$id_aula);

            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"El Modulo se ha Creado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/modulos/'.$exp[1].' ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function CrearCicloC(){
        if(isset($_POST["semestre"])){
            $tablaBD="maestria";
            $exp=explode("/",$_GET["url"]);
            $carrera=$_POST["semestre"];
            $materiaS=$_POST["materiaS"];
            $id_aula=$_POST["id_aula"];
           
            $resultado=CarrerasM::CrearCicloM($tablaBD,$carrera,$materiaS,$id_aula);

            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"El Ciclo se ha Creado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/maestria/'.$exp[1].' ";

                        }
                    })

                </script>';
            } 
        }
    }


    public function CrearPlanC(){
        if(isset($_POST["materia"])){
            $tablaBD="planesp";
            $exp=explode("/",$_GET["url"]);
            $carrera=$_POST["materia"];
            $id_aula=$_POST["id_semestre"];
            $resultado=CarrerasM::CrearPlanM($tablaBD,$carrera,$id_aula);

            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La materia se ha Creado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Plan/'.$exp[1].' ";

                        }
                    })

                </script>';
            } 
        }
    }



    public function AgregarAsistenciaC(){
        if(isset($_POST["id_alumno"])){
            $tablaBD="asistencia";
            $datosC=array("id_alumno"=>$_POST["id_alumno"],"id_aula"=>$_POST["id_aula"],"fecha"=>$_POST["fecha"], "estado"=>$_POST["estado"]);
            $resultado=CarrerasM::AgregarAsistenciaM($tablaBD,$datosC);

            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"Registro Exitoso",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Carreras ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function VerCarrerasC(){
        $tablaBD="carreras";
        $resultado=CarrerasM::VerCarrerasM($tablaBD);
        return $resultado; 
    }

    public function VerasistenciaC(){
        $tablaBD="asistencia";
        $resultado=CarrerasM::VerasistenciaM($tablaBD);
        return $resultado; 
    }

    static public function VerCarreras1C($columna,$valor){
        $tablaBD="carreras";
        $resultado=CarrerasM::VerCarreras1M($tablaBD,$columna,$valor);
        return $resultado;
    }
    
    
    public function EditarCarreraC(){
        $tablaBD="carreras";
        $exp=explode("/",$_GET["url"]);
        $id =$exp[1];
        $resultado=CarrerasM::EditarCarreraM($tablaBD,$id);
        echo '<div class="col-md-6 col-xs-12">
        <h2>Nombre de la Carrera:</h2>
        <input type="text" class="form-control input-lg" name="nombreE" value="'.$resultado["nombre"].'" required="">
        <input type="hidden" name="Cid" value="'.$resultado["id"].'">
        <br>
        <button type="submit" class="btn btn-success">Guardar Cambios</button>
    
    </div>';
    }

    public function EditarforoC(){
        $tablaBD="categories";
        $exp=explode("/",$_GET["url"]);
        $id =$exp[1];
        $resultado=CarrerasM::EditarforoM($tablaBD,$id);
        
        echo ' <div class="box-body">
        
        <h2>Nombre de la categoria:</h2>
        <input type="text" class="input-lg" id="aulas" name="category_name" value="'.$resultado["category_name"].'">
       
        <br>
        <h2>Descripcion</h2>
        <textarea name="category_description" id="editor"  >'.$resultado["category_description"].'</textarea>
        
        <h2>Imagen</h2>
        <img height="150px" class="card-img-top" src="../'.$resultado["imagen"].'" alt="hello">
        <br><br><br>    
       
        <br>
        
        <input type="hidden" name="Cid" value="'.$resultado["category_id"].'">
        <input type="hidden" class="input-lg" name="fotoActual" value="'.$resultado["imagen"].'">
        <br>
        <button type="submit" class="btn btn-success">Guardar Cambios</button>
    
    </div>';
    /* <input type="file" name="imagen" > */
    }



    public function EditarSemestreC(){
        $tablaBD="semestre";
        $exp=explode("/",$_GET["url"]);
        $id =$exp[1];
        $resultado=CarrerasM::EditarSemestreM($tablaBD,$id);
        echo '
        
        
        <div class="col-md-6 col-xs-12">
        
        <select name="materiaE" id="select2-1" class="form-control" required="">
                    <option value="'.$resultado["semestre"].'">'.$resultado["semestre"].'</option>
                    <option value="Primer Semestre">Primer Semestre</option>
                    <option value="Segundo Semestre">Segundo Semestre</option>
                    <option value="Tercer Semestre">Tercer Semestre</option>
                    <option value="Cuarto Semestre">Cuarto Semestre</option>
                    <option value="Quinto Semestre">Quinto Semestre</option>
                    <option value="Sexto Semestre">Sexto Semestre</option>
                    <option value="Septimo Semestre">Septimo Semestre</option>
                    <option value="Octavo Semestre">Octavo Semestre</option>
                    <option value="Noveno Semestre">Noveno Semestre</option>
                    <option value="Decimo Semestre">Decimo Semestre</option>

                </select>
        




        <h2>Nombre de la materia:</h2>
        <input type="text" class="form-control input-lg" name="nombreE" value="'.$resultado["materia"].'" required="">
        <input type="hidden" name="Cid" value="'.$resultado["id"].'">
        
        <br>
        <button type="submit" class="btn btn-success">Guardar Cambios</button>
    
    </div>';
    }


    public function EditarModuloC(){
        $tablaBD="modulos";
        $exp=explode("/",$_GET["url"]);
        $id =$exp[1];
        $resultado=CarrerasM::EditarModuloM($tablaBD,$id);
        echo '
        
        
        <div class="col-md-6 col-xs-12">
        
        <select name="materiaE" id="select2-1" class="form-control" required="">
                    <option value="'.$resultado["semestre"].'">'.$resultado["semestre"].'</option>
                    <option value="Primer Modulo">Primer Modulo</option>
                    <option value="Segundo Modulo">Segundo Modulo</option>
                    <option value="Tercer Modulo">Tercer Modulo</option>
                    <option value="Cuarto Modulo">Cuarto Modulo</option>
                    <option value="Quinto Modulo">Quinto Modulo</option>
                    <option value="Sexto Modulo">Sexto Modulo</option>
                    <option value="Septimo Modulo">Septimo Modulo</option>
                    <option value="Octavo Modulo">Octavo Modulo</option>
                    <option value="Noveno Modulo">Noveno Modulo</option>
                    <option value="Decimo Modulo">Decimo Modulo</option>

                </select>
        




        <h2>Nombre de la materia:</h2>
        <input type="text" class="form-control input-lg" name="nombreE" value="'.$resultado["materia"].'" required="">
        <input type="hidden" name="Cid" value="'.$resultado["id"].'">
        
        <br>
        <button type="submit" class="btn btn-success">Guardar Cambios</button>
    
    </div>';
    }

    public function EditarMaestriaC(){
        $tablaBD="maestria";
        $exp=explode("/",$_GET["url"]);
        $id =$exp[1];
        $resultado=CarrerasM::EditarMaestriaM($tablaBD,$id);
        echo '
        
        
        <div class="col-md-6 col-xs-12">
        
        <select name="materiaE" id="select2-1" class="form-control" required="">
                    <option value="'.$resultado["semestre"].'">'.$resultado["semestre"].'</option>
                    <option value="Primer Ciclo">Primer Ciclo</option>
                            <option value="Segundo Ciclo">Segundo Ciclo</option>
                            <option value="Tercer Ciclo">Tercer Ciclo</option>
                            <option value="Cuarto Ciclo">Cuarto Ciclo</option>
                            <option value="Quinto Ciclo">Quinto Ciclo</option>
                            <option value="Sexto Ciclo">Sexto Ciclo</option>
                            <option value="Septimo Ciclo">Septimo Ciclo</option>
                            <option value="Octavo Ciclo">Octavo Ciclo</option>
                            <option value="Noveno Ciclo">Noveno Ciclo</option>
                            <option value="Decimo Ciclo">Decimo Ciclo</option>

                </select>
        




        <h2>Nombre de la materia:</h2>
        <input type="text" class="form-control input-lg" name="nombreE" value="'.$resultado["materia"].'" required="">
        <input type="hidden" name="Cid" value="'.$resultado["id"].'">
        
        <br>
        <button type="submit" class="btn btn-success">Guardar Cambios</button>
    
    </div>';
    }







    public function ActualizarCarreraC(){
        if(isset($_POST["Cid"])){
            $tablaBD="carreras";
            $datosC=array("id"=>$_POST["Cid"],"carrera"=>$_POST["nombreE"]);
            $resultado=CarrerasM::ActualizarCarreraM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La Carrera se ha Actualizado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Carreras ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function ActualizarforoC(){
        if(isset($_POST["Cid"])){

           

            $tablaBD="categories";
            $datosC=array("id"=>$_POST["Cid"],"category_name"=>$_POST["category_name"],"category_description"=>$_POST["category_description"]);
            $resultado=CarrerasM::ActualizarforoM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La Categoria se ha Actualizado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Foroestu ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function ActualizarSemestreC(){
        if(isset($_POST["Cid"])){
            
            $tablaBD="semestre";
            $datosC=array("id"=>$_POST["Cid"],"carrera"=>$_POST["nombreE"],"semestre"=>$_POST["materiaE"]);
            $resultado=CarrerasM::ActualizarSemestreM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"El Semestre se ha Actualizado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Buscar ";

                        }
                    })

                </script>';
            } 
        }
    }

    public function ActualizarModuloC(){
        if(isset($_POST["Cid"])){
            
            $tablaBD="modulos";
            $datosC=array("id"=>$_POST["Cid"],"carrera"=>$_POST["nombreE"],"semestre"=>$_POST["materiaE"]);
            $resultado=CarrerasM::ActualizarModuloM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"El Modulo se ha Actualizado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Buscar ";

                        }
                    })

                </script>';
            } 
        }
    }


    public function ActualizarMaestriaC(){
        if(isset($_POST["Cid"])){
            
            $tablaBD="maestria";
            $datosC=array("id"=>$_POST["Cid"],"carrera"=>$_POST["nombreE"],"semestre"=>$_POST["materiaE"]);
            $resultado=CarrerasM::ActualizarMaestriaM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"El Ciclo se ha Actualizado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Buscar ";

                        }
                    })

                </script>';
            } 
        }
    }




    public function BorrarCarreraC(){
        $exp=explode("/",$_GET["url"]);
        $id=$exp[1];
        if(isset($id)){
            $tablaBD="carreras";
            $resultado=CarrerasM::BorrarCarreraM($tablaBD,$id);
            if($resultado== true){
              
                echo '<script>
                    swal({

                        type:"warning",
                        title:"La Carrera se ha Eliminado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Carreras ";

                        }
                    })

                </script>';
            }
        }
    }



    


    
}

?>