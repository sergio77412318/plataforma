<?php
class ExamenesC{

    public function CrearExamenC(){
        if(isset($_POST["estado"])){
            $tablaBD="examenes";

            $datosC=array("estado"=>$_POST["estado"],"id_materia"=>$_POST["id_materia"],
             "fecha"=>$_POST["fecha"],"hora"=>$_POST["hora"],"docente_examen"=>$_POST["docente_examen"]);
         $resultado=ExamenesM::CrearExamenM($tablaBD,$datosC);
         if($resultado==true){
                echo '<script>
                swal({

                    type:"success",
                    title:"El Exame fue Creado Correctamente",
                    showConfirmButton:true,
                    confirmButtonText:"Cerrar"
                }).then(function(resultado){
                     if(resultado.value){
                        window.location="http://localhost/plataforma/aula/Examenes";

                    }
                })

            </script>';
         }
        }
    }

    static public function VerExamenesC($columna,$valor){
        $tablaBD="examenes";
        $resultado=ExamenesM::VerExamenesM($tablaBD,$columna,$valor);
        return $resultado;
    }
    public function InscribirseExamenC(){
        if(isset($_POST["id_estudiante"])){
            $tablaBD="insc_examenes";
            $datosC=array("id_estudiante"=>$_POST["id_estudiante"],"id_examen"=>$_POST["id_examen"],"estado"=>"SI");
            $resultado=ExamenesM::InscribirseExamenM($tablaBD,$datosC);
            if($resultado ==true){
                echo '<script>
                swal({

                    type:"success",
                    title:"Inscripcion Correctamente",
                    showConfirmButton:true,
                    confirmButtonText:"Cerrar"
                }).then(function(resultado){
                     if(resultado.value){
                        window.location="http://localhost/plataforma/aula/Aulas-Virtuales";

                    }
                })
                       

            
            </script>';
            }
        }
    }

    public function DardebajaC(){
        if(isset($_POST["cambio"])){
            $id=$_SESSION["id"];
            $tablaBD="insc_examenes";
            $datosC=array("id"=>$id,"estado"=>"NO");
            $resultado=ExamenesM::DardebajaM($tablaBD,$datosC);
            if($resultado ==true){
                echo '<script>
                swal({

                    type:"success",
                    title:"Inscripcion Cancelada",
                    showConfirmButton:true,
                    confirmButtonText:"Cerrar"
                }).then(function(resultado){
                     if(resultado.value){
                        window.location="http://localhost/plataforma/aula/Aulas-Virtuales";

                    }
                })
                       

            
            </script>';
            }
        }
    }


    static public function VerInscExamenC($columna,$valor){
        $tablaBD="insc_examenes";
        $resultado=ExamenesM::VerInscExamenM($tablaBD,$columna,$valor);
        return $resultado;
    }

    static public function VerInscExamen1C($columna,$valor){
        $tablaBD="insc_examenes";
        $resultado=ExamenesM::VerInscExamen1M($tablaBD,$columna,$valor);
        return $resultado;
    }

}


?>