<?php
class AulasC{
        public function CrearAulaC(){
            if(isset($_POST["materia"])){
                $tablaBD="aulas";
                $exp=explode("/",$_GET["url"]);
                $datosC= array("materia"=>$_POST["materia"],"id_carrera"=>$_POST["id_carrera"],"id_docente"=>$_POST["id_docente"], );
                $resultado=AulasM::CrearAulaM($tablaBD,$datosC);
                if($resultado==true){
                    echo '<script>
                        swal({
    
                            type:"success",
                            title:"La Aula se ha Creado Correctamente",
                            showConfirmButton:true,
                            confirmButtonText:"Cerrar"
                        }).then(function(resultado){
                            if(resultado.value){
                                window.location="http://localhost/plataforma/aula/'.$exp[0].' ";
    
                            }
                        })
    
                    </script>';
                } 
            }
        }
    public function VerAulasC(){
        $tablaBD="aulas";
        $resultado=AulasM::VerAulasM($tablaBD);
        return $resultado;
    }

   static public function VerMateriasC($columna, $valor){
        $tablaBD="aulas";
        $resultado=AulasM::VerMateriasM($tablaBD, $columna, $valor);
        return $resultado;
    }

    static public function VerAulas1C($columna,$valor){
        $tablaBD="aulas";
        $resultado=AulasM::VerAulas1M($tablaBD,$columna,$valor);
        return $resultado;
    }
    



    static public function VerAulas2C($columna, $valor){
        $tablaBD="aulas";
        $resultado=AulasM::VerAulas2M($tablaBD,$columna, $valor);
        return $resultado;
    }

    static public function Versemestres2C($columna, $valor){
        $tablaBD="semestre";
        $resultado=AulasM::Versemestres2M($tablaBD,$columna, $valor);
        return $resultado;
    }





    public function BorrarAulaC(){
        if(isset($_GET["Aid"])){
            $tablaBD="aulas";
            $id=$_GET["Aid"];
            $resultado=AulasM::BorrarAulaM($tablaBD,$id);
            if($resultado==true){
             echo '<script> 
             window.location ="Aulas";
             </script>';

            }
        }

    }


    

    public function  SolicitarAulaC(){
        if(isset($_POST["materia"])){
            $tablaBD="solicitudes";
            $datosC=array("materia"=>$_POST["materia"],"id_docente"=>$_POST["id_docente"],"observaciones"=>$_POST["observaciones"],"estado"=>"Solicitado");
            $resultado=AulasM::SolicitarAulaM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La Solicitud se ha Enviado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="Solicitudes";

                        }
                    })

                </script>';
            } 
        }
    }

    public function VerSolicitudesC(){
        $tablaBD="solicitudes";
        $resultado=AulasM::VerSolicitudesM($tablaBD);
        return $resultado;
    }

   static public function VerSC($columna,$valor){
        $tablaBD="solicitudes";
        $resultado=AulasM::VerSM($tablaBD,$columna,$valor);
        return $resultado;
    }

    public function ActualizarEstadosSC(){
        if(isset($_POST["id"])){
            $tablaBD="solicitudes";
            $datosC=array("id"=>$_POST["id"],"estado"=>"Aprobada");
            $resultado=AulasM::ActualizarEstadosSM($tablaBD,$datosC);
            
        }
    }
    public function CrearHorario(){
        if(isset($_POST["id_aula"])){
            $tablaBD="horarios";
            $datosC=array("id_aula" => $_POST["id_aula"],"dias" => $_POST["dias"], "horario"=>$_POST["horario"]);
            $id_materia=$_POST["id_aula"];
            $resultado=AulasM::CrearHorarioM($tablaBD,$datosC);

            if($resultado==true){
                echo '<script>
                    window.location="http://localhost/plataforma/aula/Horario/'.$id_materia.'";
                    </script>';
            }
        }
    }
    static public function VerHorariosC($columna,$valor){
        $tablaBD="horarios";
        $resultado=AulasM::VerHorariosM($tablaBD,$columna,$valor);
        return $resultado;
    }

    static public function VerHorarios2C($columna,$valor){
        $tablaBD="horarios";
        $resultado=AulasM::VerHorarios2M($tablaBD,$columna,$valor);
        return $resultado;
    }
    public function BorrarHorarioC(){
        if(isset($_GET["Mid"])){
            $tablaBD="horarios";
            $id=$_GET["Cid"];
            $Mid=$_GET["Mid"];
            $resultado=AulasM::BorrarHorarioM($tablaBD,$id);
            if($resultado==true){
                echo '<script>
                window.location="http://localhost/plataforma/aula/Horario/'.$Mid.'";
                </script>';
            }
        }
    }

    public function  CreacionForoC(){
        if(isset($_POST["category_name"])){
            $rutaArchivo = "";
            if($_FILES["imagen"]["type"] == "image/jpeg"){
                 
                $nombre = mt_rand(10, 999);
             
                $rutaArchivo = "fotos/".$nombre.".jpg";
             
                move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaArchivo);
             
            }
            
            if($_FILES["imagen"]["type"] == "image/png"){
             
                $nombre = mt_rand(10, 999);
             
                $rutaArchivo = "fotos/".$nombre.".png";
             
                move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaArchivo);
             
            }


            $tablaBD="categories";
            $datosC=array("category_name"=>$_POST["category_name"],"category_description"=>$_POST["category_description"],"imagen"=>$rutaArchivo);
            $resultado=AulasM::CreacionForoM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La Categoria se ha Creado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="Foroestu";

                        }
                    })

                </script>';
            } 
        }
    }
   
}


?>