<?php
class EstudiantesC{
    public function InscribirmeC(){
        if(isset($_POST["id_alumno"])){
            $tablaBD="inscripciones";
            $datosC=array("id_alumno"=>$_POST["id_alumno"],"id_aula"=>$_POST["id_aula"]);
            $resultado=EstudiantesM::InscribirmeM($tablaBD,$datosC);
            if($resultado==true){

               echo '<script>
                    window.location="http://localhost/plataforma/aula/Aula/'.$_POST["id_aula"].'";
               </script>';
            }
        }
    }
    public function DarBajaC(){
        if(isset($_POST["id_aula"])){
            $tablaBD="inscripciones";
            $datosC=array("id_alumno"=>$_POST["id_alumno"],"id_aula"=>$_POST["id_aula"]);
            $resultado=EstudiantesM::DarBajaM($tablaBD,$datosC);
            if($resultado==true){

                echo '<script>
                     window.location="http://localhost/plataforma/aula/Aulas-Virtuales";
                </script>';
             }
        }
    }
    static public function VerInscriptosC($columna,$valor){
        $tablaBD="inscripciones";
        $resultado=EstudiantesM::VerInscriptosM($tablaBD,$columna,$valor);

        return $resultado;
    }

    static public function VersemestreC($columna,$valor){
        $tablaBD="semestre";
        $resultado=EstudiantesM::VersemestreM($tablaBD,$columna,$valor);

        return $resultado;
    }

    static public function VerModuloC($columna,$valor){
        $tablaBD="modulos";
        $resultado=EstudiantesM::VerModuloM($tablaBD,$columna,$valor);

        return $resultado;
    }



    static public function VerCicloC($columna,$valor){
        $tablaBD="maestria";
        $resultado=EstudiantesM::VerCicloM($tablaBD,$columna,$valor);

        return $resultado;
    }


    static public function VerplanpC($columna,$valor){
        $tablaBD="planesp";
        $resultado=EstudiantesM::VerplanpM($tablaBD,$columna,$valor);

        return $resultado;
    }

    static public function VerInscriptoC($columna,$valor,$columna2,$valor2){
        $tablaBD="inscripciones";
        $resultado=EstudiantesM::VerInscriptoM($tablaBD,$columna,$valor,$columna2,$valor2);
        return $resultado;
    }
    static public function VerasistenciaC($columna,$valor){
        $tablaBD="asistencia";
        $resultado=EstudiantesM::VerasistenciaM($tablaBD,$columna,$valor);

        return $resultado;
    }
  
}
?>