<?php
class InicioC{
    public function VerInicioC(){
            $tablaBD="inicio";
            $resultado=InicioM::VerInicioM($tablaBD);
            return $resultado;
    }
    public function GuardarDescripcionC(){
        if(isset($_POST["descripcion"])){
            $tablaBD="inicio";
            $descripcion=$_POST["descripcion"];
            $resultado=InicioM::GuardarDescripcionM($tablaBD,$descripcion);
            if($resultado==true) {
               echo '<script >
                    window.location="Inicio";
                </script>';
            }
        }
    }
    public function GuardarManualDocenteC(){
        if(isset($_POST["manualDocente"])){
            $rutaPDF=$_POST["manualDocente"];
            if(isset($_FILES["manualDocenteN"]["tmp_name"]) && !empty($_FILES["manualDocenteN"]["tmp_name"]) ){
                    if(!empty($_POST["manualDocente"])){
                        unlink($_POST["manualDocente"]);
                    }
                if($_FILES["manualDocenteN"]["type"]=="application/pdf"){
                    $rutaPDF ="Vistas/Manuales/Manual-Docente1.pdf";
                    move_uploaded_file($_FILES["manualDocenteN"]["tmp_name"],$rutaPDF);
                }
            }
            $tablaBD="inicio";
            $pdf =$rutaPDF;
            $resultado=InicioM::GuardarManualDocenteM($tablaBD,$pdf);
            if($resultado==true) {
                echo '<script >
                     window.location="Inicio";
                 </script>';
             }
        }
    }

    public function GuardarManualEstudianteC(){
        if(isset($_POST["manualEstudiante"])){
            $rutaPDF=$_POST["manualEstudiante"];
            if(isset($_FILES["manualEstudianteN"]["tmp_name"]) && !empty($_FILES["manualEstudianteN"]["tmp_name"]) ){
                    if(!empty($_POST["manualEstudiante"])){
                        unlink($_POST["manualEstudiante"]);
                    }
                if($_FILES["manualEstudianteN"]["type"]=="application/pdf"){
                    $rutaPDF ="Vistas/Manuales/Manual-Estudiante1.pdf";
                    move_uploaded_file($_FILES["manualEstudianteN"]["tmp_name"],$rutaPDF);
                }
            }
            $tablaBD="inicio";
            $pdf =$rutaPDF;
            $resultado=InicioM::GuardarManualEstudianteM($tablaBD,$pdf);
            if($resultado==true) {
                echo '<script >
                     window.location="Inicio";
                 </script>';
             }
        }
    }

}

?>