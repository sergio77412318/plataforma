<?php

class TareasC{
    public function AgregarTareaC(){
        if(isset($_POST["idSeccion"])){
            $tablaBD="tareas";
            $datosC=array("id_seccion"=>$_POST["idSeccion"],"nombre"=>"Nueva Tarea");

            $resultado=TareasM::AgregarTareaM($tablaBD,$datosC);
            if($resultado==true){
                $resultado2=TareasM::VerTareaIdM($tablaBD);
               echo '<script>
                    window.location="http://localhost/plataforma/aula/Tarea/'.$resultado2["id"].'";
               </script>';
            }

        }
    }
    static public function VerTareasC($columna,$valor){
        $tablaBD="tareas";
        $resultado=TareasM::VerTareasM($tablaBD,$columna,$valor);

        return $resultado;
    }
    static public function VerTareaC($columna,$valor){
        $tablaBD="tareas";
        $resultado=TareasM::VerTareaM($tablaBD,$columna,$valor);

        return $resultado;
    }
    public function GuardarTareaC(){
        if (isset($_POST["id"])) {
            $tablaBD="tareas";
            $datosC=array("id"=>$_POST["id"],"nombre"=>$_POST["nombre"],"fecha_limite"=>$_POST["fecha_limite"],"descripcion"=>$_POST["descripcion"]);
            $resultado=TareasM::GuardarTareaM($tablaBD,$datosC);
            if($resultado==true){
               echo '<script>
                    window.location="http://localhost/plataforma/aula/Tarea/'.$_POST["id"].'";
               </script>';
            }
        }
    }
    public function SubirTareaC(){
        if(isset($_POST["nombre"])){
            $rutaArchivo = "";

                if($_FILES["tarea"]["type"] == "application/pdf"){
 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Tareas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".pdf";
                 
                    move_uploaded_file($_FILES["tarea"]["tmp_name"], $rutaArchivo);
                 
                }
                 
                if($_FILES["tarea"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Tareas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".doc";
                 
                    move_uploaded_file($_FILES["tarea"]["tmp_name"], $rutaArchivo);
                    
                }
                 
                if($_FILES["tarea"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Tareas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".xlsx";
                 
                    move_uploaded_file($_FILES["tarea"]["tmp_name"], $rutaArchivo);
                 
                }
                if($_FILES["tarea"]["type"] == "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Tareas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".pptx";
                 
                    move_uploaded_file($_FILES["tarea"]["tmp_name"], $rutaArchivo);
                 
                }
                if($_FILES["tarea"]["type"] == "image/jpeg"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Tareas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".jpg";
                 
                    move_uploaded_file($_FILES["tarea"]["tmp_name"], $rutaArchivo);
                 
                }
                
                if($_FILES["tarea"]["type"] == "image/png"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Tareas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".png";
                 
                    move_uploaded_file($_FILES["tarea"]["tmp_name"], $rutaArchivo);
                 
                }
                $tablaBD="tarea";
                $datosC=array("nombre"=>$_POST["nombre"],"id_seccion"=>$_POST["id_seccion"],"id_tarea"=>$_POST["id_tarea"],"tarea"=>$rutaArchivo);
                $resultado=TareasM::SubirTareaM($tablaBD,$datosC);
                if($resultado==true){
                    echo '<script>
                         window.location="http://localhost/plataforma/aula/Tarea/'.$_POST["id_tarea"].'";
                    </script>';
                 }
        }
    }
    static public function VerTC($columna,$valor){
        $tablaBD="tarea";
        $resultado=TareasM::VerTM($tablaBD,$columna,$valor);
        return $resultado;
    }
    public function EntregarTareaC(){
        if(isset($_POST["id_tarea"])){
            $rutaArchivo = "";

                if($_FILES["tarea_alumno"]["type"] == "application/pdf"){
 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Entregas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".pdf";
                 
                    move_uploaded_file($_FILES["tarea_alumno"]["tmp_name"], $rutaArchivo);
                 
                }
                 
                if($_FILES["tarea_alumno"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Entregas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".doc";
                 
                    move_uploaded_file($_FILES["tarea_alumno"]["tmp_name"], $rutaArchivo);
                    
                }
                 
                if($_FILES["tarea_alumno"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Entregas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".xlsx";
                 
                    move_uploaded_file($_FILES["tarea_alumno"]["tmp_name"], $rutaArchivo);
                 
                }
                if($_FILES["tarea_alumno"]["type"] == "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Entregas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".pptx";
                 
                    move_uploaded_file($_FILES["tarea_alumno"]["tmp_name"], $rutaArchivo);
                 
                }
                if($_FILES["tarea_alumno"]["type"] == "image/jpeg"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Entregas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".jpg";
                 
                    move_uploaded_file($_FILES["tarea_alumno"]["tmp_name"], $rutaArchivo);
                 
                }
                
                if($_FILES["tarea_alumno"]["type"] == "image/png"){
                 
                    $nombre = mt_rand(10, 999);
                 
                    $rutaArchivo = "Vistas/Entregas/".$_POST["id_seccion"]."-".$_POST["id_tarea"]."-".$nombre.".png";
                 
                    move_uploaded_file($_FILES["tarea_alumno"]["tmp_name"], $rutaArchivo);
                 
                }
                $tablaBD="entregas";
                $datosC=array("id_alumno"=>$_POST["id_alumno"],"id_tarea"=>$_POST["id_tarea"],"id_seccion"=>$_POST["id_seccion"],"tarea_alumno"=>$rutaArchivo);
                $resultado=TareasM::EntregarTareaM($tablaBD,$datosC);
                $resultado2=TareasM::VerEntregaIDM($tablaBD);
                $tablaBD2="notas";
                $datosC2=array("id_entrega"=>$resultado2["id"],"estado"=>"Pendiente de Revision","id_tarea"=>$_POST["id_tarea"],"id_seccion"=>$_POST["id_seccion"]);
                $resultado3=TareasM::CrearNotaM($tablaBD2,$datosC2);
                if($resultado==true){
                    echo '<script>
                    swal({

                        type:"success",
                        title:"Sus Tarea ha sido Enviada Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                         if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Tarea/'.$_POST["id_tarea"].'";

                        }
                    })

                </script>';
                }
        }
    }
    static public function VerEntregasC($columna,$valor){
            $tablaBD="entregas";
            $resultado=TareasM::VerEntregasM($tablaBD,$columna,$valor);
            return $resultado;
    }
    public function VerNotasC(){
        $tablaBD="notas";
        $resultado=TareasM::VerNotasM($tablaBD);
        return $resultado;
    }

    static public function VerTodasNotasC($columna,$valor){
        $tablaBD="notas";
        $resultado=TareasM::VerTodasNotasC($tablaBD,$columna,$valor);
        return $resultado;
    }

    public function CambiarNotaC(){
        if  (isset($_POST["nota"])){
            $tablaBD="notas";
            $datosC=array("id"=>$_POST["id"],"nota"=>$_POST["nota"],"estado"=>$_POST["estado"],"id_aula"=>$_POST["id_aula"] );
            $resultado=TareasM::CambiarNotaM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                     window.location="http://localhost/plataforma/aula/Entregas/'.$_POST["id_tarea"].'";
                </script>';
             }
        }
    }
    public function  AgregarAsistenciaC(){
        if(isset($_POST["id_aula"])){
            require_once "asistencia.php";
            //$tablaBD="asistencia";
            $exp=explode("/",$_GET["url"]);
            $fecha=$_POST["fecha"];
           $estado=$_POST["estado"];
           $id_aula=$_POST["id_aula"];
           $id_alumno=$_POST["id_alumno"];
            //$resultado=TareasM::AgregarAsistenciaM($tablaBD,$fecha,$estado,$id_aula,$id_alumno);
            $cadena ="INSERT INTO asistencia (id_aula,id_alumno,fecha,estado) VALUES ";
for($i=0; $i < count($id_aula); $i++ ){
    $cadena.="('".$id_aula[$i]."','".$id_alumno[$i]."','".$fecha."','".$estado[$i]."'),";
}
$cadena_final=substr($cadena,0,-1);
$cadena_final.=";";
if($mysqli->query($cadena_final)):
    echo json_encode(array('error'=> false));
else:
    echo json_encode(array('error'=>true));
endif;
$mysqli->close();



            if($cadena_final==true){
                echo '<script>
                    swal({

                        type:"success",
                        title:"La Asistencia se ha Registrado Correctamente",
                        showConfirmButton:true,
                        confirmButtonText:"Cerrar"
                    }).then(function(resultado){
                        if(resultado.value){
                            window.location="http://localhost/plataforma/aula/Inscriptos/'.$exp[1].'";

                        }
                    })

                </script>';
            } 
        }
    }

    public function GrupalC(){
        if  (isset($_POST["id_alumno"])){
            $tablaBD="grupo";
            $exp=explode("/",$_GET["url"]);
            $datosC=array("id_alumno"=>$_POST["id_alumno"],"nombre"=>$_POST["nombre"]);
            $resultado=TareasM::GrupalM($tablaBD,$datosC);
            if($resultado==true){
                echo '<script>
                   
                            window.location="http://localhost/plataforma/aula/Grupal/'.$exp[1].' ";

                </script>';
            } 
        }
    }
    public function GrupalC1(){
        if  (isset($_POST["id_alumno"])){
            $tablaBD="grupo";
            $exp=explode("/",$_GET["url"]);
            $datosC=array("id_alumno"=>$_POST["id_alumno"],"grupo"=>$_POST["grupo"]);
            $resultado=TareasM::Grupal1M($tablaBD,$datosC);
            /*if($resultado==true){
                echo '<script>
                   
                            window.location="http://localhost/plataforma/aula/Grupal/'.$exp[1].' ";

                </script>';
            } */
        }
    }
    public function VerGrupos(){
        $tablaBD="grupo";
        $resultado=TareasM::VerGruposM($tablaBD);
        return $resultado;
    }

}

?>