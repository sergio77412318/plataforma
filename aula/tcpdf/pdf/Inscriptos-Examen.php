<?php

require_once "../../Controladores/AulasC.php";
require_once "../../Modelos/AulasM.php";

require_once "../../Controladores/UsuariosC.php";
require_once "../../Modelos/UsuariosM.php";

require_once "../../Controladores/ExamenesC.php";
require_once "../../Modelos/ExamenesM.php";

class pdfInscriptosExamen{

public function pdfInscriptos(){

require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);

$pdf->AddPage();

$link = $_SERVER['REQUEST_URI'];

$exp = explode("/", $link);

$columna = "id";
$valor = $exp[6];

$examen = ExamenesC::VerExamenesC($columna, $valor);

$columna = "id";
$valor = $examen["id_materia"];

$materia = AulasC::VerAulas2C($columna, $valor);

$html1 = <<<EOF

	
	<center><img src="images/2.png"></center>
	<br><br>

	<h2>Inscriptos para el Exámen de: $materia[materia]</h2>

	<h2>Fecha: $examen[fecha] - Hora: $examen[hora] </h2>

	<table style="border: 1px solid black; text-align:center; font-size:15px">

		<tr>

			<td style="border: 1px solid black width:115px;">Usuario</td>
			<td style="border: 1px solid black width:115px;">Documento</td>
			<td style="border: 1px solid black width:250px;">Apellidos y Nombres</td>

		</tr>

	</table>

EOF;


$pdf->writeHTML($html1, false, false, false, false, '');


$columna="id_examen";
$valor=$exp[6];
$insc=ExamenesC::VerInscExamenC($columna,$valor);

foreach ($insc as $key => $value) {
	
$columna = "id";
$valor = $value["id_estudiante"];

$alumnos = UsuariosC::VerUsuarios2C($columna, $valor);

foreach ($alumnos as $key => $v) {
	

$html2 = <<<EOF

	<table style="border: 1px solid black; text-align:center; font-size:15px">

		<tr>

			<td style="border: 1px solid black width:115px;">$v[usuario]</td>
			<td style="border: 1px solid black width:115px;">$v[documento]</td>
			<td style="border: 1px solid black width:250px;">$v[apellido], $v[nombre]</td>

		</tr>

	</table>

EOF;


$pdf->writeHTML($html2, false, false, false, false, '');

}

}


$pdf->Output('Inscriptos-Examen-'.$materia["materia"].'.pdf');


}


}

$a = new pdfInscriptosExamen();
$a -> pdfInscriptos();