<?php

   

require_once "../../Controladores/UsuariosC.php";

require_once "../../Modelos/UsuariosM.php";



class pdfCAlumnos{

public function pdfCA(){

require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);

$pdf->AddPage();

$link = $_SERVER['REQUEST_URI'];
$fechaActual = date('d-m-Y');
$exp = explode("/", $link);

$columna = "id";
$valor = $exp[6];

$alumno = UsuariosC::VerUsuariosC($columna, $valor);


$html1 = <<<EOF
	<style>
	.foto1 { padding: 10px; margin: 10px; border: 2px solid black; float: left; width: 100px; }
	.foto2 { padding: 10px; margin: 10px; border: 2px solid black; float: right; width: 250px; }
	.hola {text-align: center; position: absolute;
		top: 200000px;
		left: 20px;
		}
	</style>
	<img class="foto1" src="images/1.png">
	
	<br><br>

	<h2 style="text-align: center;">Certificado de Estudiante</h2><br>

	<p>La Autoridad que suscribe de la Universidad Privada Franz Tamayo, en uso de sus legitimas atribuciones conferidas por el Estatuto Orgnico de nuestra Institucion, a peticon de la parte interesada</p>
	<p>CERTIFICA:</p>
	<p>Que el Estudiante/a. <b>$alumno[apellido], $alumno[nombre]</b>, con CI. No. <b>$alumno[documento] </b> expedido en a ciudad de Cochabamba del Estado Plurinacional de Bolivia, se encontraba suscrito.<b></b></p><br><br><br><br>	
	<p>Es cuanto certifico en honor a la verdad, para fines consiguientes que podrian convenir a la parte interesada.</p><br><br>
	<br><br><br><br><br><br><br><br>
	<p class="hola">Cochabamba, $fechaActual </p>
	

EOF;


$pdf->writeHTML($html1, false, false, false, false, '');

$pdf->Output('Certificado-Alumno-'.$exp[5].'.pdf');


}

}

$c = new pdfCAlumnos();
$c -> pdfCA();