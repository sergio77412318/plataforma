<?php

require_once "../../Controladores/AulasC.php";
require_once "../../Modelos/AulasM.php";

require_once "../../Controladores/UsuariosC.php";
require_once "../../Modelos/UsuariosM.php";

require_once "../../Controladores/EstudiantesC.php";
require_once "../../Modelos/EstudiantesM.php";

require_once "../../Controladores/TareasC.php";
require_once "../../Modelos/TareasM.php";

require_once "../../Controladores/SeccionesC.php";
require_once "../../Modelos/SeccionesM.php";

class pdfInscriptosMateria{

public function pdfInscriptos(){

require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->setPrintHeader(false);

$pdf->AddPage();

$link = $_SERVER['REQUEST_URI'];

$exp = explode("/", $link);

$columna="id";
$valor=$exp[6];

$tarea=TareasC::VerTareaC($columna,$valor);

$valor = $tarea["id_seccion"];

$seccion= SeccionesC::VerSeccionesC($columna,$valor);

$valor=$seccion["id_aula"];

$aula=AulasC::VerAulas2C($columna,$valor);

$columna="id";
$valor=$aula["id_docente"];
$docente=UsuariosC::VerUsuariosC($columna,$valor);

$html1 = <<<EOF

<center><img src="images/2.png"></center>
<br><br>
	<h1>Lista de Reprobados del aula: <b>$aula[materia]</b></h1>
    <h2>Entregas de la Tarea: <b>$tarea[nombre]</b></h2>
	<h2>Docente: <b>$docente[apellido] $docente[nombre]</b></h2>
        <h3>Fecha Limite: <b>$tarea[fecha_limite]</b></h3>

	<table style="border: 1px solid black; text-align:center; font-size:15px">

		<tr>

			<td style="border: 1px solid black width:115px;">Apellidos</td>
			<td style="border: 1px solid black width:115px;">Nombre</td>
			<td style="border: 1px solid black width:250px;">Nota</td>
            <td style="border: 1px solid black width:250px;">Estado</td>


		</tr>

	</table>

EOF;


$pdf->writeHTML($html1, false, false, false, false, '');

$columna="id_tarea";
$valor=$exp[6];//6
$resultado=TareasC::VerEntregasC($columna,$valor);

foreach ($resultado as $key => $value) {
	
	$columna="id";
	$valor=$value["id_alumno"];
	$alumno=UsuariosC::VerUsuariosC($columna,$valor);
    $notas=TareasC::VerNotasC();
    foreach ($notas as $key => $nota) {
        if ($nota["id_entrega"]==$value["id"] && $nota["estado"]=="Reprobado") {




$html2 = <<<EOF

	<table style="border: 1px solid black; text-align:center; font-size:15px">

		<tr>

			<td style="border: 1px solid black width:115px;">$alumno[apellido]</td>
			<td style="border: 1px solid black width:115px;">$alumno[nombre]</td>
			<td style="border: 1px solid black width:250px;">$nota[nota]</td>
            <td style="border: 1px solid black width:250px;">$nota[estado]</td>
		</tr>

	</table>

EOF;


$pdf->writeHTML($html2, false, false, false, false, '');
        
} 
   


}
}//fin for each




$pdf->Output('Insc-Comision-.pdf');


}

}

$a = new pdfInscriptosMateria();
$a -> pdfInscriptos();