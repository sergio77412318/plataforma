<?php
require_once "ConexionBD.php";
class AulasM extends ConexionBD{
        static public function CrearAulaM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (materia,id_carrera,id_docente) VALUES 
            (:materia,:id_carrera,:id_docente)");
            $pdo->bindParam(":materia",$datosC["materia"],PDO::PARAM_STR);
            $pdo->bindParam(":id_carrera",$datosC["id_carrera"],PDO::PARAM_INT);
            $pdo->bindParam(":id_docente",$datosC["id_docente"],PDO::PARAM_INT);
            if($pdo->execute()){
                return true;
            }
            $pdo-> close();
            $pdo=null;


        }
        static public function VerAulasM($tablaBD){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD");
            $pdo->execute();
            return $pdo->fetchAll();
            $pdo->close();
            $pdo=null;
        
        }

        static public function VerMateriasM($tablaBD, $columna, $valor){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
            $pdo->bindParam(":".$columna,$valor,PDO::PARAM_INT);
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }

        static public function VerAulas1M($tablaBD,$columna,$valor){

            if($columna==null){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD");
            $pdo -> execute();
            return $pdo-> fetchAll();
            }else{
                $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
                $pdo->bindParam(":".$columna,$valor,PDO::PARAM_STR);
                $pdo -> execute();
                return $pdo-> fetch();
    
            }
            $pdo->close();
            $pdo =null;
        }

        static public function VerAulas2M($tablaBD,$columna,$valor){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
            $pdo->bindParam(":".$columna,$valor,PDO::PARAM_STR);

            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        
        }

        static public function Versemestres2M($tablaBD,$columna,$valor){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
            $pdo->bindParam(":".$columna,$valor,PDO::PARAM_STR);

            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        
        }




        static public function BorrarAulaM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("DELETE FROM $tablaBD WHERE id=$id");
            if($pdo->execute()){
                return true;
            }
            $pdo-> close();
            $pdo=null;
        }

        

        static public function SolicitarAulaM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (materia,id_docente,observaciones,estado) 
            VALUES (:materia,:id_docente,:observaciones,:estado)");
            $pdo->bindParam(":materia",$datosC["materia"],PDO::PARAM_STR);
            $pdo->bindParam(":id_docente",$datosC["id_docente"],PDO::PARAM_INT);
            $pdo->bindParam(":observaciones",$datosC["observaciones"],PDO::PARAM_STR);
            $pdo->bindParam(":estado",$datosC["estado"],PDO::PARAM_STR);

            if($pdo->execute()){
                return true;
            }
            $pdo-> close();
            $pdo=null;
        }

        static public function VerSolicitudesM($tablaBD){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD");
            $pdo->execute();
            return $pdo-> fetchAll();
            $pdo->close();
            $pdo=null;
        }
        
        static public function VerSM($tablaBD,$columna,$valor){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");

            $pdo->bindParam(":".$columna,$valor,PDO::PARAM_STR);
            $pdo->execute();
            return $pdo-> fetch();
            $pdo->close();
            $pdo=null;
        }
        static public function ActualizarEstadosSM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET estado=:estado WHERE id=:id");
            $pdo->bindParam(":id",$datosC["id"],PDO::PARAM_STR);
            $pdo->bindParam(":estado",$datosC["estado"],PDO::PARAM_STR);

            if($pdo->execute()){
                return true;
            }
            $pdo-> close();
            $pdo=null;
        }
        static public function CrearHorarioM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (id_aula,horario,dias) VALUES (:id_aula,:horario,:dias)");
            $pdo->bindParam(":id_aula",$datosC["id_aula"],PDO::PARAM_INT);
            $pdo->bindParam(":horario",$datosC["horario"],PDO::PARAM_STR);
            $pdo->bindParam(":dias",$datosC["dias"],PDO::PARAM_STR);

            if($pdo->execute()){
                return true;
            }
            $pdo-> close();
            $pdo=null;
        }
        static public function VerHorariosM($tablaBD,$columna,$valor){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna ORDER BY id ASC");
            $pdo->bindParam(":".$columna,$valor,PDO::PARAM_INT);
            $pdo->execute();
            return $pdo->fetchAll();
            $pdo->close();
            $pdo=null;
        }
        static public function VerHorarios2M($tablaBD,$columna,$valor){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna ");
            $pdo->bindParam(":".$columna,$valor,PDO::PARAM_INT);
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }
        static public function BorrarHorarioM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("DELETE FROM $tablaBD WHERE id=:id");
            $pdo-> bindParam(":id",$id,PDO::PARAM_INT);
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
        }

        static public function CreacionForoM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (category_name,category_description,imagen) 
            VALUES (:category_name,:category_description,:imagen)");
            $pdo->bindParam(":category_name",$datosC["category_name"],PDO::PARAM_STR);
            $pdo->bindParam(":category_description",$datosC["category_description"],PDO::PARAM_STR);
            $pdo->bindParam(":imagen",$datosC["imagen"],PDO::PARAM_STR);
            if($pdo->execute()){
                return true;
            }
            $pdo-> close();
            $pdo=null;
        }

        
     
}


?>