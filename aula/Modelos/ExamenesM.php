<?php
require_once "ConexionBD.php";

class ExamenesM extends ConexionBD{

    static public function CrearExamenM($tablaBD,$datosC){
        $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (hora,id_materia,estado,fecha,docente) VALUES (:hora,:id_materia,:estado,:fecha,:docente)");
            $pdo ->bindParam(":hora",$datosC["hora"],PDO::PARAM_STR);
            $pdo ->bindParam(":id_materia",$datosC["id_materia"],PDO::PARAM_INT);
            $pdo ->bindParam(":estado",$datosC["estado"],PDO::PARAM_INT);
            $pdo -> bindParam(":fecha",$datosC["fecha"],PDO::PARAM_STR);
            $pdo -> bindParam(":docente",$datosC["docente_examen"],PDO::PARAM_STR);
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
    }
    static public function VerExamenesM($tablaBD,$columna,$valor){
        if($columna==null){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD");
            $pdo -> execute();
            return $pdo->fetchAll();
        }else{
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
            $pdo ->bindParam(":".$columna,$valor,PDO::PARAM_STR);
            $pdo ->execute();
            return $pdo->fetch();
        }
        $pdo->close();
        $pdo=null;
    }
    static public function InscribirseExamenM($tablaBD,$datosC){
        $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (id_estudiante,id_examen,estado) VALUES (:id_estudiante,:id_examen,:estado)");
        $pdo ->bindParam(":id_estudiante",$datosC["id_estudiante"],PDO::PARAM_INT);
        $pdo ->bindParam(":id_examen",$datosC["id_examen"],PDO::PARAM_INT);
        $pdo ->bindParam(":estado",$datosC["estado"],PDO::PARAM_STR);
        if($pdo ->execute()){
            return true;
        }
        $pdo ->close();
        $pdo=null;

    }

    static public function DardebajaM($tablaBD,$datosC){
        $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET estado=:estado WHERE id_estudiante=:id_estudiante");

        $pdo->bindParam(":id_estudiante", $datosC["id"],PDO::PARAM_INT);
        $pdo ->bindParam(":estado",$datosC["estado"],PDO::PARAM_STR);

        if($pdo->execute()){
            return true;
        }
        $pdo->close();
        $pdo =null;
    }

    static public function VerInscExamenM($tablaBD,$columna,$valor){
        $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
        $pdo -> bindParam(":".$columna,$valor,PDO::PARAM_STR);
        $pdo ->execute();
        return $pdo->fetchAll();
        $pdo->close();
        $pdo=null;
    }

    static public function VerInscExamen1M($tablaBD,$columna,$valor){
        $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
        $pdo -> bindParam(":".$columna,$valor,PDO::PARAM_STR);
        $pdo ->execute();
        return $pdo->fetch();
        $pdo->close();
        $pdo=null;
    }


}

?>