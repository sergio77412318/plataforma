<?php
require_once "ConexionBD.php";
class CarrerasM extends ConexionBD{

        static public function CreaCarreraM($tablaBD,$carrera){
           


            

            $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (nombre) VALUES (:nombre)");
            $pdo ->bindParam(":nombre",$carrera,PDO::PARAM_STR);
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
            
        }

        static public function CrearSemestreM($tablaBD,$carrera,$materiaS,$id_aula){
           

            $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (semestre,materia,id_aula) VALUES (:semestre,:materia,:id_aula)");
            $pdo ->bindParam(":materia",$carrera,PDO::PARAM_STR);
            $pdo ->bindParam(":semestre",$materiaS,PDO::PARAM_STR);
            $pdo ->bindParam(":id_aula",$id_aula,PDO::PARAM_INT);

            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
            
        }

        static public function CrearModuloM($tablaBD,$carrera,$materiaS,$id_aula){
           

            $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (semestre,materia,id_aula) VALUES (:semestre,:materia,:id_aula)");
            $pdo ->bindParam(":materia",$carrera,PDO::PARAM_STR);
            $pdo ->bindParam(":semestre",$materiaS,PDO::PARAM_STR);
            $pdo ->bindParam(":id_aula",$id_aula,PDO::PARAM_INT);

            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
            
        }

        static public function CrearCicloM($tablaBD,$carrera,$materiaS,$id_aula){
           

            $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (semestre,materia,id_aula) VALUES (:semestre,:materia,:id_aula)");
            $pdo ->bindParam(":materia",$carrera,PDO::PARAM_STR);
            $pdo ->bindParam(":semestre",$materiaS,PDO::PARAM_STR);
            $pdo ->bindParam(":id_aula",$id_aula,PDO::PARAM_INT);

            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
            
        }


        static public function CrearPlanM($tablaBD,$carrera,$id_aula){
           


            

            $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (nombre,id_semestre) VALUES (:nombre,:id_semestre)");
            $pdo ->bindParam(":nombre",$carrera,PDO::PARAM_STR);
            $pdo ->bindParam(":id_semestre",$id_aula,PDO::PARAM_INT);
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
            
        }


        static public function AgregarAsistenciaM($tablaBD,$datosC){
           
            $pdo=ConexionBD::cBD()-> prepare("INSERT INTO $tablaBD (id_aula,id_alumno,fecha,estado) VALUES (:id_aula,:id_alumno,:fecha,:estado)");
            $pdo ->bindParam(":id_aula",$datosC,PDO::PARAM_INT);
            $pdo ->bindParam(":id_alumno",$datosC,PDO::PARAM_INT);
            $pdo ->bindParam(":fecha",$datosC,PDO::PARAM_STR);
            $pdo ->bindParam(":estado",$datosC,PDO::PARAM_STR);
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;
            
        }

        

        static public function VerCarrerasM($tablaBD){
            $pdo=ConexionBD::cBD()-> prepare("SELECT * FROM $tablaBD ");
            $pdo->execute();
            return $pdo-> fetchAll();
            $pdo->close();
            $pdo=null;

        }

        static public function VerasistenciaM($tablaBD){
            $pdo=ConexionBD::cBD()-> prepare("SELECT * FROM $tablaBD ");
            $pdo->execute();
            return $pdo-> fetchAll();
            $pdo->close();
            $pdo=null;

        }

        static public function VerCarreras1M($tablaBD,$columna,$valor){

            if($columna==null){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD");
            $pdo -> execute();
            return $pdo-> fetchAll();
            }else{
                $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE $columna=:$columna");
                $pdo->bindParam(":".$columna,$valor,PDO::PARAM_STR);
                $pdo -> execute();
                return $pdo-> fetch();
    
            }
            $pdo->close();
            $pdo =null;
        }
        static public function EditarCarreraM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE id=$id");
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }

        static public function EditarforoM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE category_id=$id");
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }

        static public function EditarSemestreM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE id=$id");
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }


        static public function EditarModuloM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE id=$id");
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }


        static public function EditarMaestriaM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("SELECT * FROM $tablaBD WHERE id=$id");
            $pdo->execute();
            return $pdo->fetch();
            $pdo->close();
            $pdo=null;
        }
        static public function ActualizarCarreraM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET nombre=:nombre WHERE id=:id");
            $pdo->bindParam(":id",$datosC["id"],PDO::PARAM_INT);
            $pdo->bindParam(":nombre",$datosC["carrera"],PDO::PARAM_STR);
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;


        }


        static public function ActualizarforoM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET category_name=:category_name,category_description=:category_description WHERE category_id=:category_id");
            $pdo->bindParam(":category_id",$datosC["id"],PDO::PARAM_INT);
            $pdo->bindParam(":category_name",$datosC["category_name"],PDO::PARAM_STR);
            $pdo->bindParam(":category_description",$datosC["category_description"],PDO::PARAM_STR);
           
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;


        }


        static public function ActualizarSemestreM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET semestre=:semestre,materia=:materia WHERE id=:id");
            $pdo->bindParam(":id",$datosC["id"],PDO::PARAM_INT);
            $pdo->bindParam(":semestre",$datosC["semestre"],PDO::PARAM_STR);
            $pdo->bindParam(":materia",$datosC["carrera"],PDO::PARAM_STR);

            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;


        }

        static public function ActualizarModuloM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET semestre=:semestre,materia=:materia WHERE id=:id");
            $pdo->bindParam(":id",$datosC["id"],PDO::PARAM_INT);
            $pdo->bindParam(":semestre",$datosC["semestre"],PDO::PARAM_STR);
            $pdo->bindParam(":materia",$datosC["carrera"],PDO::PARAM_STR);

            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;


        }

        static public function ActualizarMaestriaM($tablaBD,$datosC){
            $pdo=ConexionBD::cBD()->prepare("UPDATE $tablaBD SET semestre=:semestre,materia=:materia WHERE id=:id");
            $pdo->bindParam(":id",$datosC["id"],PDO::PARAM_INT);
            $pdo->bindParam(":semestre",$datosC["semestre"],PDO::PARAM_STR);
            $pdo->bindParam(":materia",$datosC["carrera"],PDO::PARAM_STR);

            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;


        }




        static public function BorrarCarreraM($tablaBD,$id){
            $pdo=ConexionBD::cBD()->prepare("DELETE FROM $tablaBD WHERE id=$id");
            if($pdo->execute()){
                return true;
            }
            $pdo->close();
            $pdo=null;

        }

        

}


?>  