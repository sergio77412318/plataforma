



<!--menu-->

<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Plataforma Virtual </title>
  <link rel="shortcut icon" href="../Vistas/img/2.png"/>
   <!-- Bootstrap Core CSS -->
   <link href="http://localhost/plataforma/aula/Vistas/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- FullCalendar -->
	<link href='http://localhost/plataforma/aula/Vistas/css/fullcalendar.css' rel='stylesheet' />

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor 
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
-->
<link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">
<link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script src="http://localhost/plataforma/aula/Vistas/sweetalert2/sweetalert2.all.js"></script>
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


</head>
<body class="hold-transition skin-blue sidebar-mini login-page">
<header class="main-header bg-primary">
    <!-- Logo -->
    <a href="http://localhost/plataforma/aula/Inicio" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b class="fa fa-university"></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Aula Virtual</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?php
                if($_SESSION["foto"]== ""){
                      echo '<img src="http://localhost/plataforma/aula/Vistas/img/1.jpg" class="user-image" alt="User Image">';

                }else{
                  echo '<img src="http://localhost/plataforma/aula/'.$_SESSION["foto"].'" class="user-image" alt="User Image">';
                }
                echo '<span class="hidden-xs">'. $_SESSION["apellido"].' '.$_SESSION["nombre"].'</span>';
            ?>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header" style="height: 100px">
                <?php
                  echo '<p>
                  '.$_SESSION["apellido"].' '.$_SESSION["nombre"].'';
                  if($_SESSION["id_carrera"]==0){
                     echo '<small>'.$_SESSION["rol"].'</small>';
                  }else{
                    $resultado=CarrerasC::VerCarrerasC();
                    foreach ($resultado as $key => $value) {
                      if($value["id"]==$_SESSION["id_carrera"]){
                        echo '<small>'.$value["nombre"].'</small>';
                      }
                    }
                  }
                echo '</p>';
                ?>
                
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="http://localhost/plataforma/aula/Mis-Datos" class="btn btn-primary btn-flat">Mis datos</a>
                </div>
                <div class="pull-right">
                  <a href="http://localhost/plataforma/aula/Salir" class="btn btn-danger btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>

  


<?php

?>
<?php

include './Connet.php';
$restorePoint=SGBD::limpiarCadena($_POST['restorePoint']);
$sql=explode(";",file_get_contents($restorePoint));
$totalErrors=0;
set_time_limit (60);
$con=mysqli_connect(SERVER, USER, PASS, BD);
$con->query("SET FOREIGN_KEY_CHECKS=0");
for($i = 0; $i < (count($sql)-1); $i++){
    if($con->query($sql[$i].";")){  }else{ $totalErrors++; }
}
$con->query("SET FOREIGN_KEY_CHECKS=1");
$con->close();
if($totalErrors<=0){
	echo  '<div class="content-wrapper">
	<section class="content-header">
	<h1>Copia de seguridad</h1>
	
	</section>
	<section class="content">
	<div class="box">
		<div class="box-body">
		<h1>Restauración completada con éxito</h1>
		<a href="http://localhost/plataforma/aula/Inicio">
		<button type="button" class="btn btn-success">Volver al Inicio</button>
		</a>
		</div>
	</div>
	</section>
	</div>';
}else{
	echo "Ocurrio un error inesperado, no se pudo hacer la restauración completamente";
}
?>



  <aside class="main-sidebar " >
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar ">
    <ul class="sidebar-menu">
    <li>
    <a href="http://localhost/plataforma/aula/Inicio">
    <i class="fa fa-home"></i>
    <span>Inicio</span>
    </a>
    </li>
    <li>
    <a href="http://localhost/plataforma/aula/Usuarios">
    <i class="fa fa-user-circle"></i>
    <span>Usuarios</span>
    </a>
    </li>
    <li>
    <a href="http://localhost/plataforma/aula/Carreras">
    <i class="fa fa-graduation-cap"></i>
    <span>Carreras</span>
    </a>
    </li>
    <li>
    <a href="http://localhost/plataforma/aula/Estudiantes">
    <i class="fa fa-book"></i>
    <span>Estudiantes</span>
    </a>
    </li>
        

        <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-ul"></i>
                    <span>Aulas Virtuales</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                          <a href="http://localhost/plataforma/aula/Aulas">
                            <i class="fa fa-university"></i>
                            <span>Crear Aulas</span>
                         </a>
                    </li>

                    <li>
                        <a href="http://localhost/plataforma/aula/Buscar">
                        <i class="fa fa-check-square-o"></i>
                        <span>Gestion de Aulas</span>
                        </a>
                    </li>
                </ul>
            </li>










    <li>
    <a href="http://localhost/plataforma/aula/Solicitudes">
    <i class="fa fa-edit"></i>
    <span>Solicitud de aulas</span>
    </a>
    </li>
    <li>
    <a href="http://localhost/plataforma/aula/Certificados">
    <i class="fa fa-address-card-o"></i>
    <span>Certificados</span>
    </a>
    </li>
    <li>
    <a href="http://localhost/plataforma/aula/../comunidad/Registro.php">
    <i class="fa fa-users"></i>
    <span>Comunidad feedback</span>
    </a>
    </li>
    <li>
    <a href="http://localhost/plataforma/aula/Videoconferencia">
    <i class="fa fa-film"></i>    
    <span>Videoconferencia</span>
    </a>
    </li>




    <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-ul"></i>
                    <span>Foros</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <li>
    <a href="http://localhost/plataforma/aula/foro">
    <i class="fa fa-plus-circle"></i>    
    <span>Crear categoria de Foros</span>
    </a>
    </li>

    <li>
    <a href="http://localhost/plataforma/aula/Foroestu">
    <i class="fa fa-book"></i>    
    <span>Lista de categorias de Foros</span>
    </a>
    </li>


    <li>
    <a href="http://localhost/plataforma/aula/foro1/index.php" >
    <i class="fa fa-coffee"></i>    
    <span>Ir al foro</span>
    </a>
    </li>
                </ul>
            </li>















    
   
    
    
    
    

      <li class="treeview">
                <a href="#">
                    <i class=" fa fa-question"></i>
                    <span>Encuestas</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <li>
    <a href="http://localhost/plataforma/aula/survey/index.php?page=new_survey">
    <i class="fa fa-plus-circle"></i>    
    <span>Agregar nueva</span>
    </a>
    </li>

    <li>
    <a href="http://localhost/plataforma/aula/survey/index.php?page=survey_list">
    <i class="fa fa-book"></i>    
    <span>Lista</span>
    </a>
    </li>

    <li>
    <a href="http://localhost/plataforma/aula/survey/index.php?page=survey_report">
    <i class="fa fa-info"></i>    
    <span>Ver Reporte</span>
    </a>
    </li>


    
                </ul>
            </li>

    
    
    
    
    
    
    
    
    <li>
    <a href="http://localhost/plataforma/aula/../examenes">
    <i class="fa fa-book"></i>    
    <span>Sistema de examenes</span>
    </a>
    </li>

    <li>
    <a href="http://localhost/plataforma/aula/backup/php/index.php">
    <i class="fa fa-database"></i>    
    <span>Backup de la base de datos</span>
    </a>
    </li>





    </ul>     
    </section>
    <!-- /.sidebar -->
  </aside>
<?php
?>


  
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/raphael/raphael.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap 
<script src="http://localhost/plataforma/aula/Vistas/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
-->
<!-- jQuery Knob Chart -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/moment/min/moment.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5
<script src="http://localhost/plataforma/aula/Vistas/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
 -->
<!-- Slimscroll -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="http://localhost/plataforma/aula/Vistas/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="http://localhost/plataforma/aula/Vistas/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="http://localhost/plataforma/aula/Vistas/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="http://localhost/plataforma/aula/Vistas/dist/js/demo.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/ckeditor/ckeditor.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/js/usuarios.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/js/aulas.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/js/hola.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/js/secciones.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/js/asistencia.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/js/date.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/js/sec.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/js/a.js"></script>
<script src='https://meet.jit.si/external_api.js'></script>
<script src="http://localhost/plataforma/aula/Vistas/js/carrera.js"></script>
<script src="http://localhost/plataforma/aula/Vistas/js/aulasvirtuales.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/js/horario.js"></script>

<script src="http://localhost/plataforma/aula/Vistas/js/exa.php"></script>


<!--calendar-->
<script src="http://localhost/plataforma/aula/Vistas/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="http://localhost/plataforma/aula/Vistas/js/bootstrap.min.js"></script>

<!-- FullCalendar -->
<script src='http://localhost/plataforma/aula/Vistas/js/moment.min.js'></script>
<script src='http://localhost/plataforma/aula/Vistas/js/fullcalendar/fullcalendar.min.js'></script>
<script src='http://localhost/plataforma/aula/Vistas/js/fullcalendar/fullcalendar.js'></script>
<script src='http://localhost/plataforma/aula/Vistas/js/fullcalendar/locale/es.js'></script>
<script src='http://localhost/plataforma/aula/Vistas/js/plotly-2.3.1.min.js'></script>
<script src='http://localhost/plataforma/aula/Vistas/js/graficotarea.js'></script>

<script src="https://cdn.plot.ly/plotly-2.3.1.min.js"></script>

</body>
</html>

