SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE IF NOT EXISTS plataforma;

USE plataforma;

DROP TABLE IF EXISTS answers;

CREATE TABLE `answers` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `survey_id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `answer` text NOT NULL,
  `question_id` int(30) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

INSERT INTO answers VALUES("1","1","2","Sample Only","4","2020-11-10 14:46:07");
INSERT INTO answers VALUES("2","1","2","[JNmhW],[zZpTE]","2","2020-11-10 14:46:07");
INSERT INTO answers VALUES("3","1","2","dAWTD","1","2020-11-10 14:46:07");
INSERT INTO answers VALUES("4","1","3","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in tempus turpis, sed fermentum risus. Praesent vitae velit rutrum, dictum massa nec, pharetra felis. Phasellus enim augue, laoreet in accumsan dictum, mollis nec lectus. Aliquam id viverra nisl. Proin quis posuere nulla. Nullam suscipit eget leo ut suscipit.","4","2020-11-10 15:59:43");
INSERT INTO answers VALUES("5","1","3","[qCMGO],[JNmhW]","2","2020-11-10 15:59:43");
INSERT INTO answers VALUES("6","1","3","esNuP","1","2020-11-10 15:59:43");
INSERT INTO answers VALUES("7","6","5","[Xsgml],[BZJzp],[WDLTF]","5","2021-08-09 21:00:31");
INSERT INTO answers VALUES("8","6","5","QPBRg","6","2021-08-09 21:00:31");
INSERT INTO answers VALUES("9","6","5","xUaGf","7","2021-08-09 21:00:31");
INSERT INTO answers VALUES("10","6","5","Deberian poner mas seriedad al momento del papeleo que no es muy eficaz","8","2021-08-09 21:00:31");
INSERT INTO answers VALUES("11","6","6","[Xsgml],[WDLTF]","5","2021-08-09 21:05:25");
INSERT INTO answers VALUES("12","6","6","PYnAO","6","2021-08-09 21:05:26");
INSERT INTO answers VALUES("13","6","6","xUaGf","7","2021-08-09 21:05:26");
INSERT INTO answers VALUES("14","6","6","Por favor que incuya todo el material","8","2021-08-09 21:05:26");
INSERT INTO answers VALUES("15","35","3","ASDASDASD","9","2021-08-10 17:19:40");
INSERT INTO answers VALUES("16","35","1","ohhhh","9","2021-08-10 17:29:55");
INSERT INTO answers VALUES("17","42","34","FOdtl","10","2021-08-10 21:29:33");
INSERT INTO answers VALUES("18","42","34","[XyMmq],[tqGpi]","11","2021-08-10 21:29:33");
INSERT INTO answers VALUES("19","42","34","Mas o menos el curso","12","2021-08-10 21:29:33");
INSERT INTO answers VALUES("20","42","44","FOdtl","10","2021-08-10 21:30:34");
INSERT INTO answers VALUES("21","42","44","[tqGpi],[RiLOy],[xlMQp]","11","2021-08-10 21:30:34");
INSERT INTO answers VALUES("22","42","44","Muy bueno el curso pero puede ser mejor","12","2021-08-10 21:30:34");
INSERT INTO answers VALUES("23","42","32","gXfjq","10","2021-08-19 18:12:05");
INSERT INTO answers VALUES("24","42","32","[XyMmq],[xlMQp]","11","2021-08-19 18:12:05");
INSERT INTO answers VALUES("25","42","32","Esta interesante ","12","2021-08-19 18:12:05");
INSERT INTO answers VALUES("26","44","70","JDcuU","14","2021-09-07 20:53:14");
INSERT INTO answers VALUES("27","44","70","[CnylP],[OVjGN]","15","2021-09-07 20:53:14");
INSERT INTO answers VALUES("28","44","70","Esta bueno el curso","16","2021-09-07 20:53:14");
INSERT INTO answers VALUES("29","43","70","[rsXDy]","13","2021-09-07 21:02:59");
INSERT INTO answers VALUES("30","45","44","[xKUPg]","17","2021-09-08 15:30:03");
INSERT INTO answers VALUES("31","42","33","FOdtl","10","2021-09-10 09:09:29");
INSERT INTO answers VALUES("32","42","33","[XyMmq],[RiLOy]","11","2021-09-10 09:09:29");
INSERT INTO answers VALUES("33","42","33","Todo bien con el aula","12","2021-09-10 09:09:29");
INSERT INTO answers VALUES("34","47","34","xUtAb","18","2021-10-01 15:37:14");



DROP TABLE IF EXISTS archivos1;

CREATE TABLE `archivos1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `id_seccion` int(11) NOT NULL,
  `archivo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4;

INSERT INTO archivos1 VALUES("49","html","7","Vistas/Archivos/7-455.pdf");
INSERT INTO archivos1 VALUES("50","cuestionario","8","Vistas/Archivos/8-992.doc");
INSERT INTO archivos1 VALUES("65","libros de apyo","45","Vistas/Archivos/45-225.pdf");
INSERT INTO archivos1 VALUES("68","html","50","Vistas/Archivos/50-883.pptx");
INSERT INTO archivos1 VALUES("69","libro","75","Vistas/Archivos/75-926.pdf");
INSERT INTO archivos1 VALUES("70","libros de apyo","74","Vistas/Archivos/74-842.pdf");
INSERT INTO archivos1 VALUES("71","libros de apyo","90","Vistas/Archivos/90-220.pdf");
INSERT INTO archivos1 VALUES("72","html","94","Vistas/Archivos/94-70.pdf");



DROP TABLE IF EXISTS asistencia;

CREATE TABLE `asistencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_aula` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `fecha` text NOT NULL,
  `estado` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4;

INSERT INTO asistencia VALUES("66","82","34","2021-08-31","Presente");
INSERT INTO asistencia VALUES("67","82","66","2021-08-31","Presente");
INSERT INTO asistencia VALUES("68","82","33","2021-08-31","Presente");
INSERT INTO asistencia VALUES("69","82","34","2021-09-01","Ausente");
INSERT INTO asistencia VALUES("70","82","66","2021-09-01","Ausente");
INSERT INTO asistencia VALUES("71","82","33","2021-09-01","Ausente");
INSERT INTO asistencia VALUES("72","85","34","2021-09-01","Ausente");
INSERT INTO asistencia VALUES("73","85","33","2021-09-01","Ausente");
INSERT INTO asistencia VALUES("74","82","34","2021-09-02","Presente");
INSERT INTO asistencia VALUES("75","82","66","2021-09-02","Presente");
INSERT INTO asistencia VALUES("76","82","33","2021-09-02","Presente");
INSERT INTO asistencia VALUES("77","85","34","2021-09-04","Presente");
INSERT INTO asistencia VALUES("78","85","33","2021-09-04","Presente");
INSERT INTO asistencia VALUES("79","82","34","2021-09-14","Presente");
INSERT INTO asistencia VALUES("80","82","66","2021-09-14","Presente");
INSERT INTO asistencia VALUES("81","82","33","2021-09-14","Ausente");
INSERT INTO asistencia VALUES("82","82","34","2021-09-28","Presente");
INSERT INTO asistencia VALUES("83","82","66","2021-09-28","Presente");
INSERT INTO asistencia VALUES("84","82","33","2021-09-28","Presente");
INSERT INTO asistencia VALUES("85","82","34","2021-10-01","Presente");
INSERT INTO asistencia VALUES("86","82","66","2021-10-01","Ausente");
INSERT INTO asistencia VALUES("87","82","33","2021-10-01","Presente");



DROP TABLE IF EXISTS auditoria;

CREATE TABLE `auditoria` (
  `operation` char(1) NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userid` text NOT NULL,
  `hostname` text NOT NULL,
  `id_aula` text NOT NULL,
  `materia` text NOT NULL,
  `id_carrera` text NOT NULL,
  `id_docente` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO auditoria VALUES("D","2021-10-07 13:19:41","root@localhost","DESKTOP-33SOV71","98","Programacion I","13","62");
INSERT INTO auditoria VALUES("I","2021-10-07 13:58:34","root@localhost","DESKTOP-33SOV71","0","asd","13","62");
INSERT INTO auditoria VALUES("D","2021-10-07 13:59:29","root@localhost","DESKTOP-33SOV71","99","asd","13","62");
INSERT INTO auditoria VALUES("I","2021-10-07 13:59:36","root@localhost","DESKTOP-33SOV71","0","Hola","11","69");
INSERT INTO auditoria VALUES("D","2021-10-07 13:59:59","root@localhost","DESKTOP-33SOV71","100","Hola","11","69");
INSERT INTO auditoria VALUES("I","2021-10-07 14:01:33","root@localhost","DESKTOP-33SOV71","101","Hola","12","41");
INSERT INTO auditoria VALUES("D","2021-10-07 14:01:36","root@localhost","DESKTOP-33SOV71","101","Hola","12","41");



DROP TABLE IF EXISTS auditoria_usuario;

CREATE TABLE `auditoria_usuario` (
  `operation` char(1) NOT NULL,
  `stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userid` text NOT NULL,
  `hostname` text NOT NULL,
  `id_usuario` text NOT NULL,
  `usuario` text NOT NULL,
  `nombre` text NOT NULL,
  `apellido` text NOT NULL,
  `documento` text NOT NULL,
  `rol` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO auditoria_usuario VALUES("I","2021-10-07 14:39:51","root@localhost","DESKTOP-33SOV71","72","Artwo2","Artueo","Barrios Gomez","12345","Administrador");
INSERT INTO auditoria_usuario VALUES("I","2021-10-07 14:46:11","root@localhost","DESKTOP-33SOV71","72","Artwo2","Arturo","Barrios Gomez","12345","Docente");
INSERT INTO auditoria_usuario VALUES("U","2021-10-07 14:47:14","root@localhost","DESKTOP-33SOV71","72","Artwo2","Arturo","Barrios Gomez","12345678","Docente");
INSERT INTO auditoria_usuario VALUES("D","2021-10-07 14:47:26","root@localhost","DESKTOP-33SOV71","72","Artwo2","Arturo","Barrios Gomez","12345678","Docente");
INSERT INTO auditoria_usuario VALUES("D","2021-10-08 09:40:02","root@localhost","DESKTOP-33SOV71","71","admin1","admin","admin","admin","Administrador");
INSERT INTO auditoria_usuario VALUES("U","2021-10-08 09:40:14","root@localhost","DESKTOP-33SOV71","3","admin","Sergio Rodrigo","Andia Fernandez","9318102","Administrador");
INSERT INTO auditoria_usuario VALUES("U","2021-10-08 09:40:30","root@localhost","DESKTOP-33SOV71","41","SIS1234","Rodrigo","Fernandez","123456","Docente");
INSERT INTO auditoria_usuario VALUES("U","2021-10-08 09:40:38","root@localhost","DESKTOP-33SOV71","62","psicologia","Maria","Fernandez","123","Docente");
INSERT INTO auditoria_usuario VALUES("U","2021-10-08 09:40:53","root@localhost","DESKTOP-33SOV71","34","SIS3732890","Grover","Andia ","3732890","Estudiante");



DROP TABLE IF EXISTS aulas;

CREATE TABLE `aulas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` text NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4;

INSERT INTO aulas VALUES("82","Programacion","11","41");
INSERT INTO aulas VALUES("84","Psicometria I","13","62");
INSERT INTO aulas VALUES("85","Simulacion y modelaje","11","41");
INSERT INTO aulas VALUES("86","Administracion","12","62");



DROP TABLE IF EXISTS calendario;

CREATE TABLE `calendario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `color` varchar(7) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

INSERT INTO calendario VALUES("6","master-en-frameworks-javascript-aprende-angular-react-vue-js","","#0071c5","2021-03-19 00:00:00","2021-03-20 00:00:00");
INSERT INTO calendario VALUES("7","Examen ","","#000","2021-03-24 00:00:00","2021-03-25 00:00:00");
INSERT INTO calendario VALUES("15","hola ","","#40E0D0","2021-03-10 00:00:00","2021-03-11 00:00:00");
INSERT INTO calendario VALUES("17","Examen Programacion","","#FF0000","2021-03-18 00:00:00","2021-03-19 00:00:00");
INSERT INTO calendario VALUES("19","Psicologia","","#FF8C00","2021-04-08 00:00:00","2021-04-09 00:00:00");
INSERT INTO calendario VALUES("21","Hackaton 2021","","#FF0000","2021-07-01 00:00:00","2021-07-02 00:00:00");
INSERT INTO calendario VALUES("22","Examen de programacion","","#40E0D0","2021-07-08 00:00:00","2021-07-09 00:00:00");
INSERT INTO calendario VALUES("23","PHP","","#0071c5","2021-07-02 00:00:00","2021-07-03 00:00:00");
INSERT INTO calendario VALUES("24","Presentacion del avance a las 4 de la tarde","Avance ","#FF0000","2021-08-11 00:00:00","2021-08-12 00:00:00");
INSERT INTO calendario VALUES("27","Descripcion","Esta es la descripcion","#008000","2021-08-05 00:00:00","2021-08-06 00:00:00");
INSERT INTO calendario VALUES("28","Aprende a Crear Sitios Web de Aulas Virtuales","esta es la descripcion","#40E0D0","2021-09-09 00:00:00","2021-09-10 00:00:00");
INSERT INTO calendario VALUES("29","Aprende a Crear Sitios Web de Aulas Virtuales","esta es la descripcion","#008000","2021-09-10 00:00:00","2021-09-11 00:00:00");
INSERT INTO calendario VALUES("30","HACKATON","Sera una experiencia inolvidable ven y participa del hackaton","#0071c5","2021-10-02 00:00:00","2021-10-03 00:00:00");



DROP TABLE IF EXISTS carreras;

CREATE TABLE `carreras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4;

INSERT INTO carreras VALUES("11","Ingenieria de Sistemas ");
INSERT INTO carreras VALUES("12","Ingenieria Comercial");
INSERT INTO carreras VALUES("13","Psicologia");
INSERT INTO carreras VALUES("83","Marketing");



DROP TABLE IF EXISTS categoria;

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO categoria VALUES("5","Programacion");
INSERT INTO categoria VALUES("6","Taller de grado");
INSERT INTO categoria VALUES("7","Administracion");



DROP TABLE IF EXISTS categories;

CREATE TABLE `categories` (
  `category_id` int(8) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `imagen` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

INSERT INTO categories VALUES("17","Java Script","<p><strong>JavaScript</strong>&nbsp;es el lenguaje de programaci&oacute;n&nbsp;<strong>que</strong>&nbsp;debes usar&nbsp;<strong>para</strong>&nbsp;a&ntilde;adir caracter&iacute;sticas interactivas a tu sitio web, (por ejemplo, juegos, eventos&nbsp;<strong>que</strong>&nbsp;ocurren cuando los botones son presionados o los datos son introducidos en los formularios, efectos de estilo din&aacute;micos, animaci&oacute;n, y mucho m&aacute;s).</p>\n","fotos/334.jpg","2021-09-29 20:25:01");
INSERT INTO categories VALUES("18","Contabilidad Basica ","<p>La&nbsp;<strong>contabilidad b&aacute;sica</strong>&nbsp;es la disciplina&nbsp;<strong>que</strong>&nbsp;trata de registrar todo y cada uno de los movimientos econ&oacute;micos y financieros de una empresa. En esencia, si bien existen distintos tipos de&nbsp;<strong>contabilidad</strong>&nbsp;(financiera, de costes, p&uacute;blica&hellip;), la&nbsp;<strong>b&aacute;sica</strong>&nbsp;re&uacute;ne los aspectos generales&nbsp;<strong>que</strong>&nbsp;poseen en com&uacute;n todos los tipos.</p>\n","fotos/388.jpg","2021-10-07 18:00:41");



DROP TABLE IF EXISTS category;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(511) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO category VALUES("2","JavaScript","");



DROP TABLE IF EXISTS certificados;

CREATE TABLE `certificados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estudiante` int(11) NOT NULL,
  `tipo` text NOT NULL,
  `estado` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

INSERT INTO certificados VALUES("1","34","Estudiante","Impreso");
INSERT INTO certificados VALUES("3","44","Estudiante","Impreso");
INSERT INTO certificados VALUES("6","33","Estudiante","No Impreso");



DROP TABLE IF EXISTS colegios;

CREATE TABLE `colegios` (
  `id_colegios` int(11) NOT NULL AUTO_INCREMENT,
  `colegio` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_colegios`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO colegios VALUES("1","UNIFRANZ");



DROP TABLE IF EXISTS comentarios;

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuerpo_publicacion` text NOT NULL,
  `publicado_por` varchar(60) NOT NULL,
  `posteado_por` varchar(60) NOT NULL,
  `fecha_agregada` datetime NOT NULL,
  `removido` varchar(3) NOT NULL,
  `publicacion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

INSERT INTO comentarios VALUES("18","HOLA","grover_andia","grover_andia","2020-11-14 11:15:42","no","67");
INSERT INTO comentarios VALUES("19","HOLA","sergio_andia","grover_andia","2020-11-14 11:16:14","no","67");
INSERT INTO comentarios VALUES("20","jejejejej","laiz_andia","grover_andia","2020-11-15 13:08:27","no","67");
INSERT INTO comentarios VALUES("21","asd","laiz_andia","grover_andia","2020-11-15 13:09:15","no","67");
INSERT INTO comentarios VALUES("22","buena","sergio_andia","grover_andia","2020-11-15 13:29:36","no","74");
INSERT INTO comentarios VALUES("23","sergio","grover_andia","grover_andia","2020-11-17 12:43:02","no","68");
INSERT INTO comentarios VALUES("24","BUENA HIJA","sergio_andia","grover_andia","2020-11-17 12:43:36","no","68");
INSERT INTO comentarios VALUES("25","https://www.youtube.com/watch?v=qlLeP1enFKo","sergio_andia","sergio_andia","2020-11-21 12:03:41","no","78");
INSERT INTO comentarios VALUES("26","https://www.youtube.com/watch?v=V6JZGn0zr1o","sergio_andia","sergio_andia","2020-11-21 12:22:04","no","79");
INSERT INTO comentarios VALUES("27","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/V6JZGn0zr1o\'></iframe><br>","sergio_andia","sergio_andia","2020-11-21 13:00:43","no","79");
INSERT INTO comentarios VALUES("28","funciona bien","sergio_andia","sergio_andia","2020-11-21 13:00:53","no","79");
INSERT INTO comentarios VALUES("29","<br><iframe width=\'100\' height=\'100\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","sergio_andia","2020-11-21 13:46:34","no","81");
INSERT INTO comentarios VALUES("30","<br><iframe width=\'100\' height=\'100\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","sergio_andia","2020-11-21 13:47:20","no","82");
INSERT INTO comentarios VALUES("31","hola","sergio_andia","sergio_andia","2020-11-21 13:47:26","no","82");
INSERT INTO comentarios VALUES("32","<br><iframe width=\'100\' height=\'100\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","sergio_andia","2020-11-21 13:47:29","no","82");
INSERT INTO comentarios VALUES("33","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","sergio_andia","2020-11-21 13:48:50","no","83");
INSERT INTO comentarios VALUES("34","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/U70OdBEmDvM\'></iframe><br>","sergio_andia","sergio_andia","2020-11-21 13:49:55","no","84");
INSERT INTO comentarios VALUES("35","BUENISIMA","grover_andia","grover_andia","2020-11-24 21:59:58","no","90");
INSERT INTO comentarios VALUES("36","BUENISIMA","grover_andia","sergio_andia","2020-11-24 22:00:54","no","105");
INSERT INTO comentarios VALUES("37","asd","grover_andia","sergio_andia","2020-11-24 22:01:08","no","83");
INSERT INTO comentarios VALUES("38","BUE","sergio_andia","sergio_andia","2020-11-24 22:01:41","no","83");
INSERT INTO comentarios VALUES("39","hey","sergio_andia","grover_andia","2020-12-10 16:05:38","no","90");
INSERT INTO comentarios VALUES("40","asd","sergio_andia","grover_andia","2020-12-10 22:27:03","no","67");
INSERT INTO comentarios VALUES("41","Buena","grover_andia","sergio_andia","2021-07-15 16:06:56","no","106");
INSERT INTO comentarios VALUES("42"," podrías usar un if con la condicion numero_por_evaluar%2==1(si su resto es 1 es impar) y por descartes else es par =)","sergio_andia","rodrigo_andia","2021-09-09 22:20:39","no","108");
INSERT INTO comentarios VALUES("43","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/U4MpJQhBvSM\'></iframe><br>","sergio_andia","grover_andia","2021-09-09 22:22:03","no","67");
INSERT INTO comentarios VALUES("44","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/U4MpJQhBvSM\'></iframe><br> Este video podria ayudarte\n","sergio_andia","sergio_andia","2021-09-09 22:22:24","no","71");
INSERT INTO comentarios VALUES("45","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/U4MpJQhBvSM\'></iframe><br> Este video Podria ayudarte ","sergio_andia","rodrigo_andia","2021-09-09 22:22:46","no","108");
INSERT INTO comentarios VALUES("46","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/_ao2u7F_Qzg\'></iframe><br> esto te podria ayudar","rodrigo_andia","rodrigo_andia","2021-09-10 17:48:37","no","108");
INSERT INTO comentarios VALUES("47","La composición de un libro diario sirve para observar y estructurar los datos y cuentas registradas con el fin de ver las ganancias y pérdidas de tu empresa en un determinado periodo de tiempo.\n\nCada una de sus partes ayuda a analizar las transacciones que llevas acabo, mejorando tus procesos de reconocimiento y búsqueda de tus actividades contables.","sergio_andia","rodrigo_andia","2021-09-29 21:03:23","no","110");
INSERT INTO comentarios VALUES("48","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/hX4cH1pozkM\'></iframe><br> este video tambien podria ayudarte","sergio_andia","rodrigo_andia","2021-09-29 21:03:57","no","110");



DROP TABLE IF EXISTS comment;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO comment VALUES("2","Esta muy mal brother","1","2","2021-07-21 16:33:38","2");



DROP TABLE IF EXISTS comments;

CREATE TABLE `comments` (
  `comment_id` int(8) NOT NULL AUTO_INCREMENT,
  `comment_content` text NOT NULL,
  `thread_id` int(8) NOT NULL,
  `comment_by` int(8) NOT NULL,
  `comment_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

INSERT INTO comments VALUES("1","esto es un comentario","1","1","2021-07-26 23:28:13");
INSERT INTO comments VALUES("2","hello","0","1","2021-07-26 21:40:59");
INSERT INTO comments VALUES("3","good","2","1","2021-07-26 23:29:25");
INSERT INTO comments VALUES("4","asdsdf","8","1","2021-09-08 15:11:08");
INSERT INTO comments VALUES("5","Bro, lo que tienes que ahcer es poner lo que esta dentro del script en un archivo javascript y mandarlo a llamar usando el\n&lt;script src=\"NombreArchivo.js\"&gt;&lt;/scrip&gt;\nY todo lo demas es igual.\nsaludos.","9","5","2021-09-29 20:30:44");
INSERT INTO comments VALUES("6","gracias estuvo interesante","9","1","2021-09-29 20:31:32");
INSERT INTO comments VALUES("7","Durante el proceso de elaboración de existencias, como es lógico, habría que ir añadiendo cuantos costes devengados fuesen surgiendo, hasta que se diese por finalizado totalmente el proceso, y las existencias ya estuviesen dispuestas para su venta.","10","5","2021-09-29 20:35:54");
INSERT INTO comments VALUES("8","Gracias mi bro ","10","1","2021-09-29 20:36:27");



DROP TABLE IF EXISTS cuestionarios;

CREATE TABLE `cuestionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ci` int(12) NOT NULL,
  `ap` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `am` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `nom` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` int(1) NOT NULL,
  `id_datos` int(10) NOT NULL,
  `id_examen` int(10) NOT NULL,
  `act1` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act2` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act3` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act4` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act5` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act6` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act7` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act8` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act9` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `act10` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `resp1` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp2` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp3` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp4` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp5` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp6` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp7` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp8` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp9` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp10` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `ran1` int(3) NOT NULL,
  `ran2` int(3) NOT NULL,
  `ran3` int(3) NOT NULL,
  `ran4` int(3) NOT NULL,
  `ran5` int(3) NOT NULL,
  `ran6` int(3) NOT NULL,
  `ran7` int(3) NOT NULL,
  `ran8` int(3) NOT NULL,
  `ran9` int(3) NOT NULL,
  `ran10` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO cuestionarios VALUES("10","9318102","Andia","Fernandez","Sergio","3","394","3","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("18","9318102","Andia","Fernandez","Sergio","3","398","4","10","","","","","","","","","","libertadores","","","","","","","","","","8","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("20","3732890","Andia","Fernandez","Sergio","3","406","6","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("21","3732890","Andia","Fernandez","Sergio","3","407","7","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("24","123456","123","123","123","3","408","9","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("27","3732890","Andia","Fernandez","Sergio","3","407","10","","","","","","","","","","","Seleccion","Puede cambiar de tipo si se pasa por valor","La llamada suma(5) devuelve 5","Depurar los errores del codigo en pasos sucesivos","Secuencia, seleccion y recursividad","Son visibles en su ambito y fuera","Se basa en la declaracion de hechos y reglas","Tres y Cinco son identificadores de tipo"," Hay una expresion del programa","A=B;","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("29","3732890","Andia","Fernandez","Sergio","3","407","11","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("30","111111","Rodriguez","Fernandez","Rodrigo","3","409","10","0","10","10","0","10","10","10","10","0","10","","hola","hola","mundo","hola","hola","hola","hola","mundo","hola","3","1","2","4","7","8","10","9","6","5");
INSERT INTO cuestionarios VALUES("31","2147483647","Fernandez","Andia","Sergio Rodrigo","3","411","13","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("32","2147483647","Fernandez","Andia","Sergio Rodrigo","3","411","12","","","","","","","","","","","","","","","","","","","","","0","0","0","0","0","0","0","0","0","0");
INSERT INTO cuestionarios VALUES("33","2147483647","Fernandez","Andia","Sergio Rodrigo","3","411","10","0","10","0","0","10","10","0","0","0","0","Es el que utiliza C/C++","No puede cambiar de tipo"," Reducir el tiempo de ejecucion de un programa en pasos sucesivos","La llamada suma(5) devuelve 0","Secuencia, seleccion e iteracion","for(int i=0; i<3; i++){ B[ i ] = A[ i ]; }"," Es una declaracion del modulo","Dos y Cinco son identificadores de tipo","Tupla","Son invisibles en su ambito y visibles fuera","6","4","1","10","9","3","8","7","2","5");
INSERT INTO cuestionarios VALUES("36","9318109","Andia","Fernandez","Laiz","3","413","14","0","10","0","10","0","0","0","0","0","10","Establecer las guías para especificar hacia donde debe dirigirse la investigación de campo sustentar la investigación","Se colocan los mismos datos que la ficha de resumen","es la exposición y análisis del contexto","Diseño de un esquema del marco teórico","No sirve para nada"," un sumario o resumen","Sirven para recopilar información que donde se registran los datos","acudir a bibliotecas"," Elaboración del marco teórico","Es la exposición y análisis de la teoría o grupo de teorías que sirven como fundamento para explicar los antecedentes e interpretar los resultados de la investigación.","5","10","7","6","9","3","8","4","2","1");
INSERT INTO cuestionarios VALUES("37","3732890","Andia","Rodriguez","Grover","3","414","15","0","0","0","0","0","0","0","0","0","10"," Es el estudio de las ordenes creadas por un programador."," Es la Teoría Básica de la Computacion"," Es aquella que tiene relación con el tratamiento de la Información y sus usos; es más cercana a las Personas y Computadoras"," Únicamente de bajo nivel y lenguajes de alto nivel."," Es aquella que tiene relación con el almacenamiento de la Información y sus usos; es más cercana a las Personas y Computadoras"," Es un entorno de desarrollo integrado libre, hecho principalmente para el lenguaje de programación Java."," Es un Programa que define un medio de comunicación compartido por un grupo de personas y la PC (por ejemplo: inglés o francés).","Es un programa gratuito que se ofrece al usuario final sin ningún ánimo de lucro."," Es el software básico de una computadora que provee una interfaz entre el resto de programas del ordenador, los dispositivos hardware y el usuario."," La diferencia es que uno es de datos y otro es conjunto de datos.","10","2","1","8","3","9","7","6","5","4");
INSERT INTO cuestionarios VALUES("41","12345678","Barboza","Suarez","Henry","3","416","15","0","10","10","10","10","10","10","10","10","10"," Es un lenguaje o Sfw diseñado para describir un conjunto de acciones consecutivas que un equipo debe ejecutar."," Es una parte de un sistema o una red que está diseñada para bloquear el acceso no autorizado, permitiendo al mismo tiempo comunicaciones autorizadas."," Es módulo ordenado de elementos para la computadora que se encuentran interrelacionados y que interactúan entre sí, en apoyo al usuario."," Es un conjunto de elementos orientados al tratamiento y administración de datos e información, organizados y listos para su uso posterior"," Es un Disco duro que sirve para Formateo de sistema operativo y para su uso posterior"," Es el estudio de los fundamentos teóricos de la información que procesan las computadoras, y las distintas implementaciones en forma de sistemas comp.","Es un lenguaje artificial e informal útil para el desarrollo de algoritmos, no es un leng. de progr. verdadero, puede ser solo texto"," La diferencia es que uno es de datos y otro es conjunto de datos.","Es la Representación Simbólica de un Algoritmo."," El lenguaje máquina, Lenguajes ensambladores y de lenguajes de alto nivel","7","6","5","2","3","1","9","4","10","8");
INSERT INTO cuestionarios VALUES("42","9318109","Andia","Fernandez","Laiz","3","413","16","0","10","0","0","0","0","0","10","0","0"," Mas reactiva"," Participante – no participante","Mas practica que la formacion en el trabajo al centrarse en lo importante","Esta dirigida a mejorar el clima social interno de la organizacion","Realizar una ampliacion del puesto, incorporando nuevas tareas","Verificar si los clientes son atendidos de forma correcta por el personal para asegurar el exito comercial de la empresa","Cuadro de sustitucion potencial","Subcontratar","Retener empleados deseables","","1","6","9","10","5","3","7","8","2","4");
INSERT INTO cuestionarios VALUES("43","3732890","Andia","Rodriguez","Grover","3","414","17","0","10","10","10","0","10","10","10","10","0"," # include <stdlib>","cin >> var1","CodeBlocks","Nivel de abstraccion y Forma de ejecucion","","int variable1;","Errores de compilacion y de ejecucion","Una constante","int","Una aplicacion para ver videos","1","2","9","7","3","4","5","6","10","8");
INSERT INTO cuestionarios VALUES("44","12345678","Barboza","Suarez","Henry","3","416","17","0","0","0","0","0","0","0","0","0","0","int variable1=25;","Errores de etrada y ejecucion","Console","Bajo nivel y Alto nivel","const string"," cout << \"biblioteca\";","Ninguna de las anteriores","char","Una aplicacion para ver videos","Console.writeline()","4","5","9","7","3","1","6","10","8","2");



DROP TABLE IF EXISTS dat_admin;

CREATE TABLE `dat_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ci` int(15) NOT NULL,
  `ap` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `am` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nom` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `grupo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` int(1) NOT NULL,
  `colegio` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nivel` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `curso` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `paralelo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `gestion` int(5) NOT NULL,
  `toque` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=417 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO dat_admin VALUES("1","123456","Andia","Fernandez","Grover","ADMINISTRADOR","4","UNIFRANZ","","","","2020","SI","acount user.png");
INSERT INTO dat_admin VALUES("399","9318102","Andia","Fernandez","Rodrigo","","4","UNIFRANZ","","","","2021","","foto.png");
INSERT INTO dat_admin VALUES("416","12345678","Barboza","Suarez","Henry","","3","UNIFRANZ","Ingenieria de Sistemas ","Programacion","A","2021","","foto.png");
INSERT INTO dat_admin VALUES("414","3732890","Andia","Rodriguez","Grover","","3","UNIFRANZ","Ingenieria de Sistemas ","Programacion","A","2021","","foto.png");
INSERT INTO dat_admin VALUES("413","9318109","Andia","Fernandez","Laiz","","3","UNIFRANZ","Ingenieria Comercial","Administracion","A","2021","","foto.png");



DROP TABLE IF EXISTS entregas;

CREATE TABLE `entregas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_tarea` int(11) NOT NULL,
  `tarea_alumno` text NOT NULL,
  `estado` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

INSERT INTO entregas VALUES("35","45","48","43","Vistas/Entregas/45-43-196.doc","");
INSERT INTO entregas VALUES("40","74","34","53","Vistas/Entregas/74-53-843.jpg","Enviado");
INSERT INTO entregas VALUES("41","74","34","53","Vistas/Entregas/74-53-93.jpg","Enviado");
INSERT INTO entregas VALUES("42","74","34","71","Vistas/Entregas/74-71-239.doc","");
INSERT INTO entregas VALUES("43","89","34","72","Vistas/Entregas/89-72-178.doc","");
INSERT INTO entregas VALUES("44","90","34","73","Vistas/Entregas/90-73-817.pdf","");
INSERT INTO entregas VALUES("45","90","66","73","Vistas/Entregas/90-73-249.pdf","");
INSERT INTO entregas VALUES("46","91","39","77","Vistas/Entregas/91-77-490.pdf","");
INSERT INTO entregas VALUES("47","91","32","77","Vistas/Entregas/91-77-182.pptx","");
INSERT INTO entregas VALUES("48","94","70","79","Vistas/Entregas/94-79-670.pdf","");
INSERT INTO entregas VALUES("49","94","70","79","Vistas/Entregas/94-79-153.pdf","");
INSERT INTO entregas VALUES("52","95","34","82","Vistas/Entregas/95-82-471.pdf","");
INSERT INTO entregas VALUES("53","95","33","82","Vistas/Entregas/95-82-184.png","");
INSERT INTO entregas VALUES("54","90","33","73","Vistas/Entregas/90-73-72.pdf","");



DROP TABLE IF EXISTS examen;

CREATE TABLE `examen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` int(10) NOT NULL,
  `categoria` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `consigna` varchar(210) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `preg1` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp1` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg2` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp2` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg3` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp3` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg4` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp4` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg5` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp5` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg6` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp6` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg7` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp7` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg8` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp8` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg9` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp9` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg10` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp10` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `colegio` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nivel` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `curso` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `paralelo` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `gestion` int(5) NOT NULL,
  `bimestre` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_final` date NOT NULL,
  `tiempo` int(2) NOT NULL,
  `inicio` datetime NOT NULL,
  `final` datetime NOT NULL,
  `final1` datetime NOT NULL,
  `final2` datetime NOT NULL,
  `final3` datetime NOT NULL,
  `final4` datetime NOT NULL,
  `final5` datetime NOT NULL,
  `final6` datetime NOT NULL,
  `final7` datetime NOT NULL,
  `final8` datetime NOT NULL,
  `final9` datetime NOT NULL,
  `final10` datetime NOT NULL,
  `rand` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO examen VALUES("16","9318102","Administracion","Administracion General","","Publicado","","","","","","","","","","","","","","","","","","","","","UNIFRANZ","Ingenieria Comercial","Administracion","A","2021","1BIM","2021-09-27","1","2021-09-30 18:53:20","0000-00-00 00:00:00","2021-09-30 18:54:20","2021-09-30 18:54:29","2021-09-30 18:54:33","2021-09-30 18:54:34","2021-09-30 18:54:36","2021-09-30 18:54:38","2021-09-30 18:54:41","2021-09-30 18:54:43","2021-09-30 18:54:46","2021-09-30 18:54:51","10");
INSERT INTO examen VALUES("17","9318102","Programacion","Programacion Avanzada","","Publicado","","","","","","","","","","","","","","","","","","","","","UNIFRANZ","Ingenieria de Sistemas ","Programacion","A","2021","1BIM","2021-09-28","1","2021-09-30 20:22:07","0000-00-00 00:00:00","2021-09-30 20:23:07","2021-09-30 20:23:18","2021-09-30 20:23:25","2021-09-30 20:23:32","2021-09-30 20:23:39","2021-09-30 20:23:53","2021-09-30 20:23:56","2021-09-30 20:24:00","2021-09-30 20:24:06","2021-09-30 20:24:23","10");



DROP TABLE IF EXISTS examenes;

CREATE TABLE `examenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  `docente` text NOT NULL,
  `hora` text NOT NULL,
  `fecha` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

INSERT INTO examenes VALUES("10","1","71"," Fernandez   Grover","17:00","03/27/2021");
INSERT INTO examenes VALUES("11","1","70"," Medrano   Bladimir","17:00","03/27/2021");
INSERT INTO examenes VALUES("12","1","44"," Medrano   Bladimir","17:45","03/31/2021");
INSERT INTO examenes VALUES("13","1","70"," Medrano   Bladimir","17:45","03/31/2021");
INSERT INTO examenes VALUES("14","1","72"," Medrano   Bladimir","17:00","04/15/2021");
INSERT INTO examenes VALUES("15","1","82"," Fernandez   Rodrigo","17:45","08/04/2021");
INSERT INTO examenes VALUES("16","1","82"," Fernandez   Rodrigo","17:00","03/10/2021");
INSERT INTO examenes VALUES("17","1","85"," Fernandez   Rodrigo","20:45","09/14/2021");



DROP TABLE IF EXISTS foros;

CREATE TABLE `foros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `categoria` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `titulo` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `consigna` varchar(220) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `gestion` int(4) NOT NULL,
  `colegio` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nivel` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `curso` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `paralelo` varchar(1) COLLATE utf8mb4_spanish_ci NOT NULL,
  `bimestre` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_final` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;




DROP TABLE IF EXISTS foros_lista;

CREATE TABLE `foros_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ci` int(20) NOT NULL,
  `ap` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `am` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cargo` int(1) NOT NULL,
  `id_foros` int(10) NOT NULL,
  `puntajes` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;




DROP TABLE IF EXISTS foros_participacion;

CREATE TABLE `foros_participacion` (
  `id_fp` int(11) NOT NULL AUTO_INCREMENT,
  `id_autor` int(11) NOT NULL,
  `id_foros` int(5) NOT NULL,
  `comentario` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `estrellas` int(2) NOT NULL,
  PRIMARY KEY (`id_fp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;




DROP TABLE IF EXISTS grupo;

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(11) NOT NULL,
  `nombre` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

INSERT INTO grupo VALUES("11","34","Grupo 1");
INSERT INTO grupo VALUES("13","33","Grupo 2");
INSERT INTO grupo VALUES("23","45","Grupo 1");
INSERT INTO grupo VALUES("24","59","Grupo 3");
INSERT INTO grupo VALUES("25","61","Grupo 1");
INSERT INTO grupo VALUES("26","60","");
INSERT INTO grupo VALUES("27","44","");
INSERT INTO grupo VALUES("28","34","");
INSERT INTO grupo VALUES("29","44","");
INSERT INTO grupo VALUES("30","34","");
INSERT INTO grupo VALUES("31","66","");
INSERT INTO grupo VALUES("32","39","");
INSERT INTO grupo VALUES("33","32","");
INSERT INTO grupo VALUES("34","34","");
INSERT INTO grupo VALUES("35","33","");
INSERT INTO grupo VALUES("36","44","");
INSERT INTO grupo VALUES("37","33","");
INSERT INTO grupo VALUES("38","33","");
INSERT INTO grupo VALUES("39","44","");
INSERT INTO grupo VALUES("40","70","");



DROP TABLE IF EXISTS horarios;

CREATE TABLE `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_aula` int(11) NOT NULL,
  `horario` text NOT NULL,
  `dias` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

INSERT INTO horarios VALUES("2","70","19:00 a 21:45","Martes");
INSERT INTO horarios VALUES("3","67","19:00 a 21:45","Martes");
INSERT INTO horarios VALUES("5","70","20:00 a 21:45","Miercoles");
INSERT INTO horarios VALUES("6","44","19:00 a 21:45","Martes");
INSERT INTO horarios VALUES("7","71","19:00 a 21:45","Lunes");
INSERT INTO horarios VALUES("8","75","13:00 a 14:45","Domingo");
INSERT INTO horarios VALUES("9","78","19:00 a 21:45","Miercoles");
INSERT INTO horarios VALUES("10","82","19:00 a 21:45","Martes");
INSERT INTO horarios VALUES("11","82","13:00 a 14:45","Domingo");
INSERT INTO horarios VALUES("14","90","19:00 a 21:45","Martes");



DROP TABLE IF EXISTS inicio;

CREATE TABLE `inicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `manualDocente` text NOT NULL,
  `manualEstudiante` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO inicio VALUES("1","<h3>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bienvenido a la Naci&oacute;n&nbsp;<strong>Unifranz</strong></h3>\n\n<p>Creemos en la educaci&oacute;n sin fronteras y en profesionales preparados para un mundo que cambia. Creemos en estudiantes con valores y principios que saben que son los responsables de un mejor futuro para todos.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"Desarrollamos el talento humano\" src=\"https://unifranz.edu.bo/wp-content/uploads/2020/01/desarrollamos-el-talento-humano.png\" /></p>\n\n<h3>Desarrollamos el talento humano</h3>\n\n<p>Siguiendo las nuevas tendencias y din&aacute;micas de aprendizaje, actualizamos todas nuestras carreras, implementamos el Modelo Educativo Basado en Competencias, dise&ntilde;ado para desarrollar conocimientos, habilidades, capacidades y talentos de forma pr&aacute;ctica, en un ambiente educativo de vanguardia y generando un equilibrio econ&oacute;mico y social.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"Profesores internacionales y nacionales con reconocimiento mundial\" src=\"https://unifranz.edu.bo/wp-content/uploads/2020/01/profesores-internacionales-y-nacionales-.png\" /></p>\n\n<h3>Profesores internacionales y nacionales con reconocimiento mundial</h3>\n\n<p>Nuestros profesores extranjeros y nacionales con reconocimiento internacional, comparten estos espacios de aprendizaje intercambiando experiencias en: conferencias, workshops, talleres, clases espejo, clases virtuales, e-learning, simulaciones y otros.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"Desarrollamos tu talento conectándote al mundo\" src=\"https://unifranz.edu.bo/wp-content/uploads/2020/01/desarrollamos-tu-talento-conectandote-al-mundo.png\" /></p>\n\n<h3>Desarrollamos tu talento conect&aacute;ndote al mundo</h3>\n\n<p>Creamos una cultura innovadora, &uacute;nica y con visi&oacute;n global. Formamos l&iacute;deres creativos, propositivos y capaces de afrontar los desaf&iacute;os de este siglo.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"Intercampus\" src=\"https://unifranz.edu.bo/wp-content/uploads/2020/01/intercampus.png\" /></p>\n\n<h3>Intercampus</h3>\n\n<p>Estamos en las 4 ciudades m&aacute;s importantes de Bolivia y con mayor desarrollo y crecimiento econ&oacute;mico. Nuestros estudiantes pueden enriquecer su experiencia formativa cursando uno o m&aacute;s semestres, en cualquiera de las sedes del pa&iacute;s.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img alt=\"Menciones que marcan la diferencia en el ámbito laboral\" src=\"https://unifranz.edu.bo/wp-content/uploads/2020/01/menciones-que-marcan-la-diferencia.png\" /></p>\n\n<h3>Menciones que marcan la diferencia en el &aacute;mbito laboral</h3>\n\n<p>Las menciones abren nuevas oportunidades y generan mayor valor en el mercado laboral, diferenciaci&oacute;n y un sinf&iacute;n de competencias que impactan en el futuro profesional.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img alt=\"Sistema de calidad total\" src=\"https://unifranz.edu.bo/wp-content/uploads/2020/01/sitema-de-calidad-total.png\" /></p>\n\n<h3>Sistema de calidad total</h3>\n\n<p>Desde el a&ntilde;o 2018, UNIFRANZ cuenta con la certificaci&oacute;n ISO 9001:2015 de la T&Uuml;V Rheinland (Alemania) en procesos administrativos y acad&eacute;micos, asegurando la calidad en todos sus servicios.</p>\n","Vistas/Manuales/Manual-Docente1.pdf","Vistas/Manuales/Manual-Estudiante1.pdf");



DROP TABLE IF EXISTS insc_examenes;

CREATE TABLE `insc_examenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estudiante` int(11) NOT NULL,
  `id_examen` int(11) NOT NULL,
  `estado` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

INSERT INTO insc_examenes VALUES("21","34","15","SI");
INSERT INTO insc_examenes VALUES("22","34","16","SI");



DROP TABLE IF EXISTS inscripciones;

CREATE TABLE `inscripciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(11) NOT NULL,
  `id_aula` int(11) NOT NULL,
  `grupo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4;

INSERT INTO inscripciones VALUES("25","33","1","Grupo 1");
INSERT INTO inscripciones VALUES("26","34","3","");
INSERT INTO inscripciones VALUES("33","1","9","");
INSERT INTO inscripciones VALUES("36","34","1","");
INSERT INTO inscripciones VALUES("37","42","10","");
INSERT INTO inscripciones VALUES("41","34","11","");
INSERT INTO inscripciones VALUES("42","48","13","");
INSERT INTO inscripciones VALUES("43","34","16","");
INSERT INTO inscripciones VALUES("44","34","23","");
INSERT INTO inscripciones VALUES("45","44","10","");
INSERT INTO inscripciones VALUES("46","44","46","");
INSERT INTO inscripciones VALUES("47","42","46","");
INSERT INTO inscripciones VALUES("49","44","66","");
INSERT INTO inscripciones VALUES("50","34","70","");
INSERT INTO inscripciones VALUES("51","34","44","");
INSERT INTO inscripciones VALUES("52","33","44","Grupo 1");
INSERT INTO inscripciones VALUES("53","33","70","Grupo 1");
INSERT INTO inscripciones VALUES("56","32","71","");
INSERT INTO inscripciones VALUES("61","59","44","");
INSERT INTO inscripciones VALUES("62","61","44","");
INSERT INTO inscripciones VALUES("63","60","78","");
INSERT INTO inscripciones VALUES("64","44","75","");
INSERT INTO inscripciones VALUES("65","34","81","");
INSERT INTO inscripciones VALUES("66","44","83","");
INSERT INTO inscripciones VALUES("67","34","82","");
INSERT INTO inscripciones VALUES("68","66","82","");
INSERT INTO inscripciones VALUES("69","39","84","");
INSERT INTO inscripciones VALUES("70","32","84","");
INSERT INTO inscripciones VALUES("71","34","85","");
INSERT INTO inscripciones VALUES("72","33","85","");
INSERT INTO inscripciones VALUES("75","33","82","");
INSERT INTO inscripciones VALUES("76","44","86","");
INSERT INTO inscripciones VALUES("77","70","90","");



DROP TABLE IF EXISTS likes;

CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(60) NOT NULL,
  `publicacion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4;

INSERT INTO likes VALUES("53","grover_andia","67");
INSERT INTO likes VALUES("59","angelo_andia","70");
INSERT INTO likes VALUES("83","sergio_andia","70");
INSERT INTO likes VALUES("84","grover_andia","71");
INSERT INTO likes VALUES("85","sergio_andia","72");
INSERT INTO likes VALUES("87","grover_andia","73");
INSERT INTO likes VALUES("88","sergio_andia","74");
INSERT INTO likes VALUES("90","grover_andia","68");
INSERT INTO likes VALUES("91","sergio_andia","68");
INSERT INTO likes VALUES("95","grover_andia","90");
INSERT INTO likes VALUES("96","grover_andia","105");
INSERT INTO likes VALUES("97","grover_andia","83");
INSERT INTO likes VALUES("98","sergio_andia","90");
INSERT INTO likes VALUES("99","sergio_andia","67");
INSERT INTO likes VALUES("100","sergio_andia","106");
INSERT INTO likes VALUES("101","grover_andia","106");
INSERT INTO likes VALUES("102","sergio_andia","108");
INSERT INTO likes VALUES("104","rodrigo_andia","108");



DROP TABLE IF EXISTS maestria;

CREATE TABLE `maestria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` text NOT NULL,
  `semestre` text NOT NULL,
  `id_aula` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;




DROP TABLE IF EXISTS mensajes;

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_destinatario` int(11) NOT NULL,
  `id_envia` int(11) NOT NULL,
  `asunto` text NOT NULL,
  `mensaje` text NOT NULL,
  `leido` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

INSERT INTO mensajes VALUES("1","41","34","Prueba mensaje","<p>mensjae de prueba</p>\n","No","2021-04-28 15:44:34");
INSERT INTO mensajes VALUES("2","41","34","hey","<p>mundo</p>\n","No","2021-04-28 16:35:12");



DROP TABLE IF EXISTS mensajes_com;

CREATE TABLE `mensajes_com` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_a` varchar(60) NOT NULL,
  `usuario_de` varchar(60) NOT NULL,
  `cuerpo_mensaje` text NOT NULL,
  `fecha` datetime NOT NULL,
  `abrio` varchar(3) NOT NULL,
  `visto` varchar(3) NOT NULL,
  `eliminado` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

INSERT INTO mensajes_com VALUES("1","sergio_andia","grover_andia","hola","2020-11-14 11:15:57","yes","yes","no");
INSERT INTO mensajes_com VALUES("2","grover_andia","sergio_andia","hola","2020-11-14 11:16:31","yes","yes","no");
INSERT INTO mensajes_com VALUES("3","laiz_andia","sergio_andia","hola wwer","2020-11-14 11:21:46","yes","no","no");
INSERT INTO mensajes_com VALUES("4","grover_andia","sergio_andia","escrito desde el perfil","2020-11-14 12:51:14","yes","yes","no");
INSERT INTO mensajes_com VALUES("5","grover_andia","sergio_andia","probando","2020-11-14 12:55:04","yes","yes","no");
INSERT INTO mensajes_com VALUES("6","grover_andia","sergio_andia","qwer","2020-11-14 12:57:19","yes","yes","no");
INSERT INTO mensajes_com VALUES("7","sergio_andia","grover_andia","funciona\n","2020-11-14 12:57:44","yes","yes","no");
INSERT INTO mensajes_com VALUES("8","angelo_andia","sergio_andia","hola","2020-11-14 15:36:08","yes","yes","no");
INSERT INTO mensajes_com VALUES("9","sergio_andia","grover_andia","JEJEJEJE","2020-11-14 15:59:09","yes","yes","no");
INSERT INTO mensajes_com VALUES("10","sergio_andia","grover_andia","dale","2020-11-15 09:12:18","yes","yes","no");
INSERT INTO mensajes_com VALUES("11","grover_andia","sergio_andia","POsi","2020-11-15 09:12:44","yes","yes","no");
INSERT INTO mensajes_com VALUES("12","laiz_andia","sergio_andia","qwer","2020-11-15 09:12:51","yes","no","no");
INSERT INTO mensajes_com VALUES("13","sergio_andia","grover_andia","asd","2020-11-15 12:45:08","yes","yes","no");
INSERT INTO mensajes_com VALUES("14","sergio_andia","grover_andia","as","2020-11-15 12:45:10","yes","yes","no");
INSERT INTO mensajes_com VALUES("15","sergio_andia","grover_andia","hoy ","2020-11-17 09:50:18","yes","yes","no");
INSERT INTO mensajes_com VALUES("16","sergio_andia","grover_andia","BUENA","2020-11-24 22:00:11","yes","yes","no");
INSERT INTO mensajes_com VALUES("17","grover_andia","sergio_andia","hola","2020-12-10 16:05:19","yes","yes","no");
INSERT INTO mensajes_com VALUES("18","grover_andia","sergio_andia","hola","2020-12-10 22:27:13","yes","yes","no");
INSERT INTO mensajes_com VALUES("19","sergio_andia","grover_andia","buena","2020-12-10 22:27:34","yes","yes","no");
INSERT INTO mensajes_com VALUES("20","rodrigo_andia","sergio_andia","holA necesito ayuda\n","2020-12-11 21:13:23","yes","yes","no");
INSERT INTO mensajes_com VALUES("21","rodrigo_andia","sergio_andia","hoal","2021-01-27 19:47:05","yes","yes","no");
INSERT INTO mensajes_com VALUES("22","sergio_andia","grover_andia","Hi ","2021-07-15 16:07:22","no","yes","no");



DROP TABLE IF EXISTS modulos;

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` text NOT NULL,
  `semestre` text NOT NULL,
  `id_aula` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

INSERT INTO modulos VALUES("16","Psicometria I","Primer Modulo","84");
INSERT INTO modulos VALUES("17","Psicometria I","Primer Modulo","90");
INSERT INTO modulos VALUES("18","Programicon","Primer Modulo","82");



DROP TABLE IF EXISTS notas;

CREATE TABLE `notas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL,
  `id_tarea` int(11) NOT NULL,
  `id_entrega` int(11) NOT NULL,
  `nota` text NOT NULL,
  `estado` text NOT NULL,
  `id_aula` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

INSERT INTO notas VALUES("35","45","43","35","100","Aprobado","0","0");
INSERT INTO notas VALUES("40","74","53","40","45","Reprobado","0","0");
INSERT INTO notas VALUES("41","74","53","41","54","Aprobado","0","0");
INSERT INTO notas VALUES("42","74","71","42","50","Reprobado","0","0");
INSERT INTO notas VALUES("43","89","72","43","51","Aprobado","0","0");
INSERT INTO notas VALUES("44","90","73","44","53","Aprobado","82","0");
INSERT INTO notas VALUES("45","90","73","45","23","Reprobado","82","0");
INSERT INTO notas VALUES("46","91","77","46","49","Reprobado","0","0");
INSERT INTO notas VALUES("47","91","77","47","52","Aprobado","0","0");
INSERT INTO notas VALUES("48","94","79","48","51","Aprobado","0","0");
INSERT INTO notas VALUES("49","94","79","49","50","Reprobado","0","0");
INSERT INTO notas VALUES("52","95","82","52","50","Reprobado","85","0");
INSERT INTO notas VALUES("53","95","82","53","51","Aprobado","85","0");
INSERT INTO notas VALUES("54","90","73","54","100","Aprobado","82","0");



DROP TABLE IF EXISTS notificaciones;

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_a` varchar(50) NOT NULL,
  `usuario_de` varchar(50) NOT NULL,
  `mensaje` text NOT NULL,
  `link` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL,
  `abierto` varchar(3) NOT NULL,
  `visto` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;

INSERT INTO notificaciones VALUES("13","sergio_andia","grover_andia","Grover Andia Publico en tu perfil","publicaciones.php?id=74","2020-11-15 12:44:50","yes","yes");
INSERT INTO notificaciones VALUES("14","grover_andia","laiz_andia","Laiz Andia comentó tu publicación","publicaciones.php?id=67","2020-11-15 13:08:27","yes","yes");
INSERT INTO notificaciones VALUES("15","grover_andia","laiz_andia","Laiz Andia comentó tu publicación","publicaciones.php?id=67","2020-11-15 13:09:15","yes","yes");
INSERT INTO notificaciones VALUES("16","sergio_andia","laiz_andia","Laiz Andia comentó en una publicación que usted comentó","publicaciones.php?id=67","2020-11-15 13:09:16","yes","yes");
INSERT INTO notificaciones VALUES("17","grover_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=74","2020-11-15 13:16:17","yes","yes");
INSERT INTO notificaciones VALUES("18","grover_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=74","2020-11-15 13:29:36","yes","yes");
INSERT INTO notificaciones VALUES("19","sergio_andia","grover_andia","Grover Andia Le dio like a tu publicacion","publicaciones.php?id=73","2020-11-15 14:06:57","yes","yes");
INSERT INTO notificaciones VALUES("20","grover_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=74","2020-11-17 10:47:13","yes","yes");
INSERT INTO notificaciones VALUES("21","grover_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=68","2020-11-17 12:42:36","yes","yes");
INSERT INTO notificaciones VALUES("22","grover_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=68","2020-11-17 12:43:29","yes","yes");
INSERT INTO notificaciones VALUES("23","grover_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=68","2020-11-17 12:43:36","yes","yes");
INSERT INTO notificaciones VALUES("24","grover_andia","sergio_andia","Sergio Andia Publico en tu perfil","publicaciones.php?id=75","2020-11-17 12:43:46","yes","yes");
INSERT INTO notificaciones VALUES("25","sergio_andia","grover_andia","Grover Andia comentó tu publicación","publicaciones.php?id=105","2020-11-24 22:00:54","yes","yes");
INSERT INTO notificaciones VALUES("26","sergio_andia","grover_andia","Grover Andia Le dio like a tu publicacion","publicaciones.php?id=105","2020-11-24 22:00:55","yes","yes");
INSERT INTO notificaciones VALUES("27","sergio_andia","grover_andia","Grover Andia Le dio like a tu publicacion","publicaciones.php?id=83","2020-11-24 22:01:07","yes","yes");
INSERT INTO notificaciones VALUES("28","sergio_andia","grover_andia","Grover Andia comentó tu publicación","publicaciones.php?id=83","2020-11-24 22:01:08","yes","yes");
INSERT INTO notificaciones VALUES("29","grover_andia","sergio_andia","Sergio Andia comentó en una publicación que usted comentó","publicaciones.php?id=83","2020-11-24 22:01:41","yes","yes");
INSERT INTO notificaciones VALUES("30","grover_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=90","2020-12-10 16:05:31","yes","yes");
INSERT INTO notificaciones VALUES("31","grover_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=90","2020-12-10 16:05:38","yes","yes");
INSERT INTO notificaciones VALUES("32","grover_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=67","2020-12-10 22:26:57","no","yes");
INSERT INTO notificaciones VALUES("33","grover_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=67","2020-12-10 22:27:03","no","yes");
INSERT INTO notificaciones VALUES("34","laiz_andia","sergio_andia","Sergio Andia comentó en una publicación que usted comentó","publicaciones.php?id=67","2020-12-10 22:27:04","no","no");
INSERT INTO notificaciones VALUES("35","sergio_andia","grover_andia","Grover Andia Le dio like a tu publicacion","publicaciones.php?id=106","2021-07-15 16:06:50","no","yes");
INSERT INTO notificaciones VALUES("36","sergio_andia","grover_andia","Grover Andia comentó tu publicación","publicaciones.php?id=106","2021-07-15 16:06:56","no","yes");
INSERT INTO notificaciones VALUES("37","rodrigo_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=108","2021-09-09 22:20:39","yes","yes");
INSERT INTO notificaciones VALUES("38","rodrigo_andia","sergio_andia","Sergio Andia Le dio like a tu publicacion","publicaciones.php?id=108","2021-09-09 22:20:42","yes","yes");
INSERT INTO notificaciones VALUES("39","grover_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=67","2021-09-09 22:22:03","no","no");
INSERT INTO notificaciones VALUES("40","laiz_andia","sergio_andia","Sergio Andia comentó en una publicación que usted comentó","publicaciones.php?id=67","2021-09-09 22:22:03","no","no");
INSERT INTO notificaciones VALUES("41","grover_andia","sergio_andia","Sergio Andia Escribio en su perfil","publicaciones.php?id=71","2021-09-09 22:22:24","no","no");
INSERT INTO notificaciones VALUES("42","rodrigo_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=108","2021-09-09 22:22:46","yes","yes");
INSERT INTO notificaciones VALUES("43","sergio_andia","rodrigo_andia","Rodrigo Andia comentó en una publicación que usted comentó","publicaciones.php?id=108","2021-09-10 17:48:37","no","yes");
INSERT INTO notificaciones VALUES("44","rodrigo_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=110","2021-09-29 21:03:23","no","no");
INSERT INTO notificaciones VALUES("45","rodrigo_andia","sergio_andia","Sergio Andia comentó tu publicación","publicaciones.php?id=110","2021-09-29 21:03:57","no","no");



DROP TABLE IF EXISTS planesp;

CREATE TABLE `planesp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `id_semestre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

INSERT INTO planesp VALUES("7","Psicometria I","24");
INSERT INTO planesp VALUES("8","Introduccion a la Psicologia","24");
INSERT INTO planesp VALUES("9","Psicologia I","24");
INSERT INTO planesp VALUES("10","Psicologia II","25");
INSERT INTO planesp VALUES("11","Psicometria II","25");
INSERT INTO planesp VALUES("12","Introduccion al psicoanalisis","25");



DROP TABLE IF EXISTS post;

CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `brief` varchar(511) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `post_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO post VALUES("1","JavaScript","","Strings in JavaScript are contained within a pair of either single quotation marks \'\' or double quotation marks \". Both quotes represent Strings but be sure to choose one and STICK WITH IT. If you start with a single quote, you need to end with a single quote. There are pros and cons to using both IE single quotes tend to make it easier to write HTML within Javascript as you don’t have to escape the line with a double quote.","2020-08-29_22-19-56-34edb5c592b0cdd6ce8fce6e6ea0f1c6.jpg","2021-07-20 19:43:57","1","2","1");
INSERT INTO post VALUES("2","Atributos JavaScript","","Veremos como los atributos son funcionales","aguilar-2.0.png","2021-07-20 19:47:21","1","2","2");



DROP TABLE IF EXISTS preguntas;

CREATE TABLE `preguntas` (
  `id_preguntas` int(11) NOT NULL AUTO_INCREMENT,
  `id_examen` int(10) NOT NULL,
  `A` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `B` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `C` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `D` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `resp` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `preg` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`id_preguntas`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO preguntas VALUES("86","16","Mas proactiva"," Mas reactiva","Mas operacional","Mas inflexible con las necesidades del personal de la empresa","Mas proactiva","Con el paso del tiempo, la gestion de los recursos humanos se ha vuelto mas:","1");
INSERT INTO preguntas VALUES("87","16","Atraer candidatos","Retener empleados deseables","Motivar empleados","Incrementar la Productividad","Incrementar la Productividad","Uno de los objetivos implicitos de la funcion de recursos humanos es:","2");
INSERT INTO preguntas VALUES("88","16","Ejercer un ferreo control sobre el personal de la empresa para aumentar la productividad","Incrementar la productividad de los trabajadores via motivacion, flexibilidad e integracion","Verificar si los clientes son atendidos de forma correcta por el personal para asegurar el exito comercial de la empresa","Ajustar los procesos de produccion a los perfiles de trabajadores de la empresa","Incrementar la productividad de los trabajadores via motivacion, flexibilidad e integracion","La actual direccion de recursos humanos se dedica principalmente a:","3");
INSERT INTO preguntas VALUES("89","16","Produce problemas de coordinacion de tareas","Al ser puestos muy especializados necesitan mucha formacion","Produce baja productividad","Dificulta el reclutamiento y la seleccion ","Produce problemas de coordinacion de tareas","Una de las desventajas de la especializacion horizontal en el diseno de puestos de trabajo es que:","4");
INSERT INTO preguntas VALUES("90","16","Realizar una ampliacion del puesto, incorporando nuevas tareas","Realizar un enriquecimiento del puesto ","Realizar rotacion entre puestos","Ejercer un mayor control sobre los trabajadores","Ejercer un mayor control sobre los trabajadores","Cual de las soluciones que se ofrecen a continuacion no sirve para solucionar los problemas derivados de una elevada especializacion:","5");
INSERT INTO preguntas VALUES("91","16"," Participante – no participante","Abierta o encubierta","Restringida o holistica","Asistematica o sistematica"," Participante – no participante"," Segun la implicacion del observador, la observacion directa puede ser:","6");
INSERT INTO preguntas VALUES("92","16","Cuadro de sustitucion potencial","Sumario de sustituciones","Matrices de transicion","El analisis del mercado laboral","El analisis del mercado laboral","La oferta de mano de obra externa viene determinada a partir de:","7");
INSERT INTO preguntas VALUES("93","16","Despedir","Subcontratar","Jubilar","Reducir los salarios","Subcontratar","Marque la respuesta incorrecta. Cuando en una empresa la oferta de personal es mayor que la demanda se puede:","8");
INSERT INTO preguntas VALUES("94","16","Mas practica que la formacion en el trabajo al centrarse en lo importante","Mas rapida y sin interrupciones","Mas barata","Mas lenta y precisa","Mas rapida y sin interrupciones","La formacion fuera del lugar de trabajo es:","9");
INSERT INTO preguntas VALUES("95","16","Fomenta la versatilidad de los empleados","Inhibe la creatividad de los trabajadores"," Esta dirigida a afrontar situaciones de crisis","Esta dirigida a mejorar el clima social interno de la organizacion"," Esta dirigida a afrontar situaciones de crisis","La formacion polivalente:","10");
INSERT INTO preguntas VALUES("96","17"," # include (stdlib)"," # include <stdlib>"," cin >> variable1;"," cout << \"biblioteca\";","# include <stdlib>","Para incluir una biblioteca en c++ se utiliza la siguiente sentencia:","1");
INSERT INTO preguntas VALUES("97","17","cin >> var1","cout<< var1","Console.writeline()","Console.Read()","cin >> var1","se utiliza para ingresar un valor a una variable desde teclado","2");
INSERT INTO preguntas VALUES("98","17"," cout<<","cin >>","const int ","const string"," cout<<","La sentencia Escribir en Pseudocodigo equivale en el lenguaje C++","3");
INSERT INTO preguntas VALUES("99","17","include variable1;","const int variable1=25;","int variable1;","int variable1=25;","int variable1;","La definicion de una variable se realiza de la siguiente manera","4");
INSERT INTO preguntas VALUES("100","17","Errores de compilacion y de ejecucion","Errores de entrada y salida","Errores de etrada y ejecucion","Errores de compilacion y salida","Errores de compilacion y de ejecucion","En el proceso de Compilar, pueden aparecer 2 tipos de errores:","5");
INSERT INTO preguntas VALUES("101","17","Una variable","Una constante","Una biblioteca","Ninguna de las anteriores","Una constante","La sentencia en C++: const float PI=3.14; hace referencia a:","6");
INSERT INTO preguntas VALUES("102","17","Tiempo de Compilacion y tiempo de Ejecucion","Bajo nivel y Alto nivel","Nivel de maquina y nivel medio","Nivel de abstraccion y Forma de ejecucion","Nivel de abstraccion y Forma de ejecucion","Los lenguajes de programacion se clasifican de forma general de acuerdo a:","7");
INSERT INTO preguntas VALUES("103","17","Un programa para hacer peliculas","Un lenguaje de secuencias de comandos del lado del cliente","Un lenguaje de programacion general","Una aplicacion para ver videos","Un lenguaje de programacion general","¿Que es c++?","8");
INSERT INTO preguntas VALUES("104","17","GAC","GNU GCC","CodeBlocks","Console","CodeBlocks","¿Cual de los siguientes es un compilador de C++?","9");
INSERT INTO preguntas VALUES("105","17","bool","float","int","char","int","¿Cual es el tipo de dato para almacenar enteros?","10");



DROP TABLE IF EXISTS publicaciones;

CREATE TABLE `publicaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuerpo` text NOT NULL,
  `anadido_por` varchar(60) NOT NULL,
  `usuario_a` varchar(60) NOT NULL,
  `fecha_agregada` datetime NOT NULL,
  `usuario_cerrado` varchar(3) NOT NULL,
  `eliminado` varchar(3) NOT NULL,
  `likes` int(11) NOT NULL,
  `image` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4;

INSERT INTO publicaciones VALUES("67","HEY\n","grover_andia","none","2020-11-14 11:15:33","no","no","2","");
INSERT INTO publicaciones VALUES("68","qwert","grover_andia","none","2020-11-14 11:17:24","no","no","2","");
INSERT INTO publicaciones VALUES("69","hey","sergio_andia","grover_andia","2020-11-14 11:18:10","no","si","0","");
INSERT INTO publicaciones VALUES("70","hello","sergio_andia","none","2020-11-14 11:28:29","no","si","2","");
INSERT INTO publicaciones VALUES("71","probando","sergio_andia","grover_andia","2020-11-15 11:45:56","no","no","1","");
INSERT INTO publicaciones VALUES("72","funciona","grover_andia","sergio_andia","2020-11-15 11:47:46","no","si","1","");
INSERT INTO publicaciones VALUES("73","po","sergio_andia","grover_andia","2020-11-15 12:43:09","no","no","1","");
INSERT INTO publicaciones VALUES("74","qwe","grover_andia","sergio_andia","2020-11-15 12:44:49","no","no","1","");
INSERT INTO publicaciones VALUES("75","viendo","sergio_andia","grover_andia","2020-11-17 12:43:46","no","no","0","");
INSERT INTO publicaciones VALUES("76","https://www.youtube.com/watch?v=QrmClm8FKBU&t=489s","sergio_andia","none","2020-11-21 11:42:10","no","si","0","");
INSERT INTO publicaciones VALUES("77","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/QrmClm8FKBU&t=489s\'></iframe><br>","sergio_andia","none","2020-11-21 11:53:50","no","si","0","");
INSERT INTO publicaciones VALUES("78","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/QrmClm8FKBU\'></iframe><br>","sergio_andia","none","2020-11-21 12:03:18","no","si","0","");
INSERT INTO publicaciones VALUES("79","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/QrmClm8FKBU\'></iframe><br>","sergio_andia","none","2020-11-21 12:11:44","no","no","0","");
INSERT INTO publicaciones VALUES("80","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/V6JZGn0zr1o\'></iframe><br>","sergio_andia","none","2020-11-21 12:22:13","no","si","0","");
INSERT INTO publicaciones VALUES("81","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","none","2020-11-21 13:46:28","no","si","0","");
INSERT INTO publicaciones VALUES("82","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","none","2020-11-21 13:47:16","no","si","0","");
INSERT INTO publicaciones VALUES("83","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/3sq8lSDMZ_M\'></iframe><br>","sergio_andia","none","2020-11-21 13:48:46","no","no","1","");
INSERT INTO publicaciones VALUES("84","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/U70OdBEmDvM\'></iframe><br>","sergio_andia","none","2020-11-21 13:49:51","no","si","0","");
INSERT INTO publicaciones VALUES("85","probando subida ","sergio_andia","none","2020-11-23 16:18:26","no","no","0","recursos/img/publicaciones/5fbc1912e4541download.png");
INSERT INTO publicaciones VALUES("86","go","sergio_andia","none","2020-11-23 16:18:45","no","si","0","recursos/img/publicaciones/5fbc1925b6cab569032.jpg");
INSERT INTO publicaciones VALUES("87","FUNCIONA","laiz_andia","none","2020-11-23 16:23:21","no","no","0","recursos/img/publicaciones/5fbc1a3962415monochrome-photo-of-building-1816798.jpg");
INSERT INTO publicaciones VALUES("88","resuelto","sergio_andia","none","2020-11-23 16:27:39","no","si","0","");
INSERT INTO publicaciones VALUES("89","hey","sergio_andia","none","2020-11-23 16:30:11","no","si","0","");
INSERT INTO publicaciones VALUES("90","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/6cCtfUat1rQ\'></iframe><br>","grover_andia","none","2020-11-23 16:52:05","no","no","2","");
INSERT INTO publicaciones VALUES("91","a","grover_andia","none","2020-11-23 16:56:24","no","si","0","");
INSERT INTO publicaciones VALUES("92","a","grover_andia","none","2020-11-23 16:57:48","no","si","0","");
INSERT INTO publicaciones VALUES("93","Q","rodrigo_andia","none","2020-11-23 16:58:49","no","si","0","recursos/img/publicaciones/5fbc2289aa5ad123.jpg");
INSERT INTO publicaciones VALUES("94","<br><iframe width=\'420\' height=\'315\' src=\'buenisima\nhttps://www.youtube.com/embed/qlLeP1enFKo\'></iframe><br>","rodrigo_andia","none","2020-11-23 16:59:17","no","si","0","");
INSERT INTO publicaciones VALUES("95","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/qlLeP1enFKo\'></iframe><br>","rodrigo_andia","none","2020-11-23 16:59:33","no","no","0","");
INSERT INTO publicaciones VALUES("96","asd","rodrigo_andia","none","2020-11-23 16:59:54","no","si","0","");
INSERT INTO publicaciones VALUES("97","asd","rodrigo_andia","none","2020-11-23 17:03:50","no","si","0","");
INSERT INTO publicaciones VALUES("98","asd","rodrigo_andia","none","2020-11-23 17:08:40","no","si","0","");
INSERT INTO publicaciones VALUES("99","asd","rodrigo_andia","none","2020-11-23 17:09:22","no","si","0","");
INSERT INTO publicaciones VALUES("100","asdd","rodrigo_andia","none","2020-11-23 17:10:46","no","si","0","");
INSERT INTO publicaciones VALUES("101","asd","rodrigo_andia","none","2020-11-23 17:13:02","no","si","0","");
INSERT INTO publicaciones VALUES("102","","sergio_andia","none","2020-11-23 17:24:58","no","si","0","recursos/img/publicaciones/5fbc28aab04f5569032.jpg");
INSERT INTO publicaciones VALUES("103","ads","sergio_andia","none","2020-11-23 17:27:29","no","si","0","");
INSERT INTO publicaciones VALUES("104","asd","sergio_andia","none","2020-11-23 17:27:39","no","si","0","");
INSERT INTO publicaciones VALUES("105","Subido desde el celular ","sergio_andia","none","2020-11-23 17:39:44","no","si","1","recursos/img/publicaciones/5fbc2c20043aeFB_IMG_1606070319248.jpg");
INSERT INTO publicaciones VALUES("106","hola","sergio_andia","none","2021-01-27 19:47:52","no","si","2","");
INSERT INTO publicaciones VALUES("107","Hello","sergio_andia","none","2021-07-19 20:26:00","no","no","0","");
INSERT INTO publicaciones VALUES("108","Forma más óptima de saber si un número es primo o no.\n","rodrigo_andia","none","2021-09-09 22:20:11","no","no","2","recursos/img/publicaciones/613ac0db0d2afasdadsd.png");
INSERT INTO publicaciones VALUES("109","<br><iframe width=\'420\' height=\'315\' src=\'https://www.youtube.com/embed/zulYWSjrqb0\'></iframe><br>","rodrigo_andia","none","2021-09-29 20:51:05","no","si","0","");
INSERT INTO publicaciones VALUES("110","Alguien que pueda ayudarme en la solucion de este libro diario por favor","rodrigo_andia","none","2021-09-29 21:01:35","no","no","0","recursos/img/publicaciones/61550c6f6ada3libro diario.JPG");



DROP TABLE IF EXISTS questions;

CREATE TABLE `questions` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `frm_option` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `order_by` int(11) NOT NULL,
  `survey_id` int(30) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

INSERT INTO questions VALUES("1","Sample Survey Question using Radio Button","{\"cKWLY\":\"Option 1\",\"esNuP\":\"Option 2\",\"dAWTD\":\"Option 3\",\"eZCpf\":\"Option 4\"}","radio_opt","3","1","2020-11-10 12:04:46");
INSERT INTO questions VALUES("2","Question for Checkboxes","{\"qCMGO\":\"Checkbox label 1\",\"JNmhW\":\"Checkbox label 2\",\"zZpTE\":\"Checkbox label 3\",\"dOeJi\":\"Checkbox label 4\"}","check_opt","2","1","2020-11-10 12:25:13");
INSERT INTO questions VALUES("4","Sample question for the text field","","textfield_s","1","1","2020-11-10 13:34:21");
INSERT INTO questions VALUES("5","Como mejorariamos en nuestros cursos","{\"Xsgml\":\"Mas inovacion\",\"BZJzp\":\"mas material\",\"WDLTF\":\"mas recursos\",\"JFyaV\":\"todo esta bien\"}","check_opt","1","6","2021-08-09 20:57:32");
INSERT INTO questions VALUES("6","Te gusto el curso","{\"PYnAO\":\"Si\",\"QPBRg\":\"No\",\"sVBGR\":\"Mas o menos\"}","radio_opt","3","6","2021-08-09 20:58:07");
INSERT INTO questions VALUES("7","Te fue util el curso","{\"xUaGf\":\"Si\",\"sOdFD\":\"No\"}","radio_opt","2","6","2021-08-09 20:58:37");
INSERT INTO questions VALUES("8","Algo que pudieramos mejorar por favor dinos","","textfield_s","4","6","2021-08-09 20:58:59");
INSERT INTO questions VALUES("9","hello world","","textfield_s","0","35","2021-08-10 17:18:42");
INSERT INTO questions VALUES("10","Le sirvio el curso","{\"FOdtl\":\"Si\",\"gXfjq\":\"No\"}","radio_opt","1","42","2021-08-10 19:50:20");
INSERT INTO questions VALUES("11","Que deberia mejorar en el curso","{\"XyMmq\":\"Mas Material\",\"tqGpi\":\"Mas interaccion\",\"RiLOy\":\"Mejor ensenanza\",\"xlMQp\":\"Mas teoria \"}","check_opt","2","42","2021-08-10 19:51:14");
INSERT INTO questions VALUES("12","Dejanos tu feedback","","textfield_s","3","42","2021-08-10 19:51:34");
INSERT INTO questions VALUES("13","asd","{\"rsXDy\":\"asd\",\"sEjeh\":\"aa\"}","check_opt","0","43","2021-09-07 17:40:34");
INSERT INTO questions VALUES("14","hola","{\"JDcuU\":\"Si\",\"sKhOg\":\"No\",\"BvZgC\":\"Talvez\"}","radio_opt","0","44","2021-09-07 20:42:48");
INSERT INTO questions VALUES("15","hola 1","{\"CnylP\":\"Mas inovacion\",\"OVjGN\":\"Mas Material\",\"roEln\":\"asd\"}","check_opt","0","44","2021-09-07 20:43:02");
INSERT INTO questions VALUES("16","dejano tu comentario ","","textfield_s","0","44","2021-09-07 20:43:16");
INSERT INTO questions VALUES("17","asdasdasda","{\"xKUPg\":\"ada\",\"quSdI\":\"asdasd\"}","check_opt","0","45","2021-09-08 15:29:26");
INSERT INTO questions VALUES("18","Te gusto el curso","{\"xUtAb\":\"Si\",\"gJTbm\":\"No\"}","radio_opt","1","47","2021-10-01 15:35:03");
INSERT INTO questions VALUES("19","Considero Bueno el curso?","{\"mofRF\":\"Si\",\"Oicdb\":\"No\"}","radio_opt","3","47","2021-10-07 14:05:30");
INSERT INTO questions VALUES("20","Dejano un comentario","","textfield_s","4","47","2021-10-07 14:06:22");
INSERT INTO questions VALUES("21","El curso necesito mas...","{\"jeUZB\":\"Archivos\",\"SMCli\":\"Power Point\",\"eOoGN\":\"Mas Interaccion\"}","check_opt","2","47","2021-10-07 14:09:28");



DROP TABLE IF EXISTS secciones;

CREATE TABLE `secciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_aula` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4;

INSERT INTO secciones VALUES("7","9","Introduccion I","<p>Admin hola qwerty</p>\n");
INSERT INTO secciones VALUES("8","9","admin","<p>Admin</p>\n");
INSERT INTO secciones VALUES("26","1","Introduccion","<p>security 1</p>\n");
INSERT INTO secciones VALUES("27","3","Robotica","<p>world</p>\n");
INSERT INTO secciones VALUES("30","9","bladimir","");
INSERT INTO secciones VALUES("31","7","Introduccion","<p>hola</p>\n");
INSERT INTO secciones VALUES("32","9","Prueba Tarea","");
INSERT INTO secciones VALUES("33","7","Seccion de prueba","<p>Esta es una seccion de prueba</p>\n");
INSERT INTO secciones VALUES("41","11","Nueva Seccion","");
INSERT INTO secciones VALUES("45","13","marketing","<p>Esta sera la seccion del primer parcial donde se subiran las tareas y archivos respectivos</p>\n");
INSERT INTO secciones VALUES("47","17","Nueva Seccion","");
INSERT INTO secciones VALUES("49","23","Nueva Seccion","");
INSERT INTO secciones VALUES("50","10","Introduccion","<ul>\n	<li>Requiero&nbsp;</li>\n	<li>estos tres puntos</li>\n</ul>\n");
INSERT INTO secciones VALUES("51","39","Nueva Seccion","");
INSERT INTO secciones VALUES("74","44","Grupal","<p>Este es el curso&nbsp;</p>\n");
INSERT INTO secciones VALUES("75","66","Nueva Seccion","<p>hola</p>\n");
INSERT INTO secciones VALUES("76","77","Nueva Seccion","");
INSERT INTO secciones VALUES("83","71","Nueva Seccion","");
INSERT INTO secciones VALUES("84","70","Nueva Seccion","");
INSERT INTO secciones VALUES("88","44","Nueva Seccion","");
INSERT INTO secciones VALUES("89","81","Introduccion a al progrmacion","");
INSERT INTO secciones VALUES("90","82","Introduccion a al programacion","<p>&nbsp;</p>\n\n<p>Este curso permite que los alumnos adquieran las t&eacute;cnicas, notaciones y la l&oacute;gica para poder programar con el objetivo de que una vez finalizado el curso tengan los conocimientos necesarios para poder especializarse en diversos lenguajes de&nbsp;<strong>programaci&oacute;n</strong>.</p>\n");
INSERT INTO secciones VALUES("91","84","Nueva Seccion","");
INSERT INTO secciones VALUES("94","90","Marketing 1","<ul>\n	<li><em>Est<strong>e </strong>ser</em>a el curso <strong>de mareting&nbsp;</strong></li>\n</ul>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n	<tbody>\n		<tr>\n			<td>dsada</td>\n			<td>&nbsp;</td>\n		</tr>\n		<tr>\n			<td>dadada</td>\n			<td>dada</td>\n		</tr>\n		<tr>\n			<td>asdas</td>\n			<td>dada</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n");
INSERT INTO secciones VALUES("95","85","Nueva Seccion","");
INSERT INTO secciones VALUES("98","82","Nueva Seccion","");



DROP TABLE IF EXISTS semestre;

CREATE TABLE `semestre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `materia` text NOT NULL,
  `semestre` text NOT NULL,
  `id_aula` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

INSERT INTO semestre VALUES("34","Bases Biologicas en Psicologia","Primer Semestre","84");
INSERT INTO semestre VALUES("35","Psicologia General","Primer Semestre","84");
INSERT INTO semestre VALUES("36","Humanismo y Psicoanalisis","Segundo Semestre","84");
INSERT INTO semestre VALUES("37","Psicoestadistica Inferencial","Segundo Semestre","84");
INSERT INTO semestre VALUES("38","Conductismo Y analisis Experimental","Tercer Semestre","84");
INSERT INTO semestre VALUES("39","Investigacionen Pricologia I","Tercer Semestre","84");
INSERT INTO semestre VALUES("41","Marketing I","Primer Semestre","90");
INSERT INTO semestre VALUES("42","Psicometria II","Segundo Semestre","90");
INSERT INTO semestre VALUES("43","Psicometria I","Primer Semestre","82");
INSERT INTO semestre VALUES("44","Psicometria II","Segundo Semestre","82");
INSERT INTO semestre VALUES("45","Programacion I","Primer Semestre","85");
INSERT INTO semestre VALUES("46","Informatica de Sitemas","Primer Semestre","85");
INSERT INTO semestre VALUES("47","Administracion General","Primer Semestre","85");
INSERT INTO semestre VALUES("48","Calculo I","Primer Semestre","85");
INSERT INTO semestre VALUES("49","Algebra I","Primer Semestre","85");
INSERT INTO semestre VALUES("50","Programacion II","Segundo Semestre","85");
INSERT INTO semestre VALUES("51","Base de datos I","Segundo Semestre","85");
INSERT INTO semestre VALUES("52","Calculo II","Segundo Semestre","85");
INSERT INTO semestre VALUES("53","Fisica II","Segundo Semestre","85");
INSERT INTO semestre VALUES("54","Algebra II","Segundo Semestre","85");



DROP TABLE IF EXISTS solicitudes;

CREATE TABLE `solicitudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_docente` int(11) NOT NULL,
  `materia` text NOT NULL,
  `observaciones` text NOT NULL,
  `estado` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

INSERT INTO solicitudes VALUES("10","41","Sistemas Expertos","<p>Se olvidaron de asignarme el aula de Sistemas Expertos por favor seria de mi agrado que me lo dieran</p>\n","Solicitado");



DROP TABLE IF EXISTS solicitudes_amigo;

CREATE TABLE `solicitudes_amigo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_a` varchar(50) NOT NULL,
  `usuario_de` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;




DROP TABLE IF EXISTS survey_set;

CREATE TABLE `survey_set` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(30) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4;

INSERT INTO survey_set VALUES("42","Satisfaccion del curso","Denos una breve descripcion de lo que opina del curso","0","2021-09-08","2021-09-30","2021-08-10 19:49:15");
INSERT INTO survey_set VALUES("47","Encuesta sobre curso","Es una breve encuesta sobre el curso tomado","0","2021-10-01","2021-10-30","2021-10-01 15:34:16");



DROP TABLE IF EXISTS tarea;

CREATE TABLE `tarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL,
  `id_tarea` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `tarea` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

INSERT INTO tarea VALUES("1","0","1","Tarea 1","Vistas/Tareas/-694.jpg");
INSERT INTO tarea VALUES("2","0","15","Prueba Tarea","Vistas/Tareas/-570.doc");
INSERT INTO tarea VALUES("9","0","16","ADM","Vistas/Tareas/-199.pptx");
INSERT INTO tarea VALUES("10","0","16","excel","Vistas/Tareas/-517.xlsx");
INSERT INTO tarea VALUES("11","0","16","PDF","Vistas/Tareas/-271.pdf");
INSERT INTO tarea VALUES("12","0","1","sergio","Vistas/Tareas/-883.jpg");
INSERT INTO tarea VALUES("28","45","43","Ejemplos","Vistas/Tareas/45-43-995.doc");
INSERT INTO tarea VALUES("30","94","79","Archivo de apoyo","Vistas/Tareas/94-79-31.pdf");



DROP TABLE IF EXISTS tareas;

CREATE TABLE `tareas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_limite` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4;

INSERT INTO tareas VALUES("1","7","Examen 1","<p>El examen tendra la duracion de 30 minutos donde cada estudiante tendra un examen diferente</p>\n","10/10/2020");
INSERT INTO tareas VALUES("2","7","hello","","");
INSERT INTO tareas VALUES("3","7","Nueva Tarea","","");
INSERT INTO tareas VALUES("4","8","Nueva Tarea","","");
INSERT INTO tareas VALUES("6","26","Nueva Tarea","","");
INSERT INTO tareas VALUES("7","7","Nueva Tarea","","");
INSERT INTO tareas VALUES("8","7","Nueva Tarea","","");
INSERT INTO tareas VALUES("9","8","Admin","<p>Antecedentes de Administracion</p>\n","10/06/2020");
INSERT INTO tareas VALUES("10","27","Nueva Tarea","","10/10/2020");
INSERT INTO tareas VALUES("12","30","prueba","bladi","");
INSERT INTO tareas VALUES("13","31","Marco Teorico","<p>Deben completar para la fecha dada el marco teorico</p>\n","10/09/2020");
INSERT INTO tareas VALUES("14","8","Nueva Tarea","","");
INSERT INTO tareas VALUES("15","32","Prueba Tarea","<p>Prueba de Tarea</p>\n","10/10/2020");
INSERT INTO tareas VALUES("16","32","Trabajo Admin","<p>Trabajo de los de admin</p>\n","10/13/2020");
INSERT INTO tareas VALUES("17","32","Nueva ","<p>jajajaajajaaja</p>\n","10/05/2020");
INSERT INTO tareas VALUES("43","45","Elaboracion de una propuesta","<p>Deben enviar una elaboracion sitematizada con todos los puntos</p>\n","10/31/2020");
INSERT INTO tareas VALUES("45","47","Nueva Tarea","","");
INSERT INTO tareas VALUES("47","50","Nueva Tarea","","12/31/2020");
INSERT INTO tareas VALUES("53","74","ASD","<p>ASD</p>\n","03/31/2021");
INSERT INTO tareas VALUES("54","75","Nueva Tarea","","02/26/2021");
INSERT INTO tareas VALUES("61","74","Prueba de estados","","04/15/2021");
INSERT INTO tareas VALUES("71","74","Examen final","<p>Este sera ele xamne final de simulaion</p>\n","07/15/2021");
INSERT INTO tareas VALUES("72","89","Programcion","","07/19/2021");
INSERT INTO tareas VALUES("73","90","Practica de la sentencia IF","<p>Hacer un&nbsp;<strong>programa en C++</strong>&nbsp;para una tienda de zapatos que tiene una promoci&oacute;n de descuento para vender al mayor, esta depender&aacute; del n&uacute;mero de zapatos que se compren. Si son m&aacute;s de diez, se les dar&aacute; un 10% de descuento sobre el total de la compra; si el n&uacute;mero de zapatos es mayor de veinte pero menor de treinta, se le otorga un 20% de descuento; y si son m&aacute;s treinta zapatos se otorgar&aacute; un 40% de descuento. El precio de cada zapato es de $80.</p>\n","09/10/2021");
INSERT INTO tareas VALUES("77","91","Nueva Tarea","","08/31/2021");
INSERT INTO tareas VALUES("79","94","Nueva Tarea","<p>esta la tarea</p>\n","12/01/2021");
INSERT INTO tareas VALUES("82","95","Nueva Tarea","","09/09/2021");



DROP TABLE IF EXISTS threads;

CREATE TABLE `threads` (
  `thread_id` int(7) NOT NULL AUTO_INCREMENT,
  `thread_title` varchar(255) NOT NULL,
  `thread_desc` text NOT NULL,
  `thread_cat_id` int(7) NOT NULL,
  `thread_user_id` int(7) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`thread_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

INSERT INTO threads VALUES("9","Crear un test en Javascript","Hola tengo un gran problema estoy intentando realizar un test donde las preguntas incorrectas se marquen en rojo y que cuando lo termines salga una alerta con las cantidades de aciertos que a tenido.\nSe agradece todas las respuestas. Un saludo","17","1","2021-09-29 20:27:34");
INSERT INTO threads VALUES("10","¿Qué asientos contables corresponde hacer en este ejercicio?","Buenas tardes,\nMe surge una duda a la hora de contabilizar el supuesto que explico a continuación:\nA 31/12/2018, los costes de las existencias de los productos en curso de mi empresa son de 600.000 euros (2.000 unidades elaboradas al 50%). No obstante, un problema con la materia prima utilizada para la creación de estos productos, ha supuesto un proceso de transformación adicional que ha supuesto un coste de 150.000 euros, además de los costes iniciales que he indicado para completar la elaboración (600.000 euros).\nCabe destacar que el importe que se espera obtener por el producto terminado es de 700 euros por unidad.\nMe podrían indicar los asientos contables a realizar?","18","1","2021-09-29 20:35:17");



DROP TABLE IF EXISTS trabajos;

CREATE TABLE `trabajos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` int(15) NOT NULL,
  `categoria` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `titulo` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `gestion` int(5) NOT NULL,
  `colegio` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nivel` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `curso` varchar(20) COLLATE utf8mb4_spanish_ci NOT NULL,
  `paralelo` varchar(1) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_final` date NOT NULL,
  `bimestre` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

INSERT INTO trabajos VALUES("1","123456","Literatura","qwer","Publicado","2021","","","","","2021-04-12","1BIM");
INSERT INTO trabajos VALUES("2","123456","Literatura","qwer","Publicado","2021","","","","","2021-04-12","1BIM");



DROP TABLE IF EXISTS trabajos_lista;

CREATE TABLE `trabajos_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ci` int(15) NOT NULL,
  `ap` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `am` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nom` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` int(1) NOT NULL,
  `id_trabajos` int(5) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `puntaje` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;




DROP TABLE IF EXISTS user;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `kind` int(11) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO user VALUES("1","Administrator","","admin","","90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad","","1","1","2021-07-20 17:38:27");
INSERT INTO user VALUES("2","Sergio","Andia Fernandez","","Sergioandia11@gmail.com","adcd7048512e64b48da55b027577886ee5a36350","","1","3","2021-07-20 19:10:22");



DROP TABLE IF EXISTS user_f;

CREATE TABLE `user_f` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `kind` int(11) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO user_f VALUES("1","Administrator","","admin","","90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad","","1","1","2021-07-20 19:06:09");



DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `sno` int(8) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(30) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

INSERT INTO users VALUES("1","sergioandia@gmail.com","$2y$10$EV/Jg07fx50VBScwZCPmAu.J3V/O.zDDexHUf8lFdlxnAxSWmtG5K","2021-07-26 21:15:42");
INSERT INTO users VALUES("2","sergioandia@gmail.com1","$2y$10$VRrWMptgZ4rwN4UJTPrECud0W0xJ9Vmzy.q4VKppumyPLQugDbgKm","2021-07-26 21:17:20");
INSERT INTO users VALUES("3","sergioandia@gmail.com12","$2y$10$a1MDzthCN9JXr6BdY0ZVmOiAH0PbdPElwThtktZ4R2riNct9NKm/q","2021-07-26 21:21:47");
INSERT INTO users VALUES("4","grover@gmail.com","$2y$10$xAq8baOnPTeDuTKonSRVmezY5Ts4VIK5XBsRV0wx19LxtB.w175rG","2021-08-09 17:42:37");
INSERT INTO users VALUES("5","rodrigo@gmail.com","$2y$10$aXeeWcYQPk6d/ZOnnaDyTeoL8GBZJ4DpEp9FCGnpp0R4gNZ8lW3Uy","2021-09-29 20:26:59");



DROP TABLE IF EXISTS usuarios;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` text NOT NULL,
  `clave` text NOT NULL,
  `nombre` text NOT NULL,
  `apellido` text NOT NULL,
  `documento` text NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `foto` text NOT NULL,
  `rol` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4;

INSERT INTO usuarios VALUES("3","admin","123456","Sergio Rodrigo","Andia Fernandez","9318102","0","Vistas/img/Usuarios/U-704.jpg","Administrador");
INSERT INTO usuarios VALUES("32","prueba","123","prueba","prueba","12345","13","","Estudiante");
INSERT INTO usuarios VALUES("33","SIS123","123","Henry","Barboza","123","11","","Estudiante");
INSERT INTO usuarios VALUES("34","SIS3732890","123456","Grover","Andia ","3732890","11","Vistas/img/Usuarios/U-900.jpg","Estudiante");
INSERT INTO usuarios VALUES("39","1","1","Mariana ","Rodriguez","1234567","13","","Estudiante");
INSERT INTO usuarios VALUES("41","SIS1234","123456","Rodrigo","Fernandez","123456","0","Vistas/img/Usuarios/U-916.jpg","Docente");
INSERT INTO usuarios VALUES("42","ICO123","123","Andrea","Jimenez","1234","12","Vistas/img/Usuarios/U-349.jpg","Estudiante");
INSERT INTO usuarios VALUES("44","ICO9318109","123","Laiz","Andia Fernandez","9318109","12","Vistas/img/Usuarios/U-791.jpg","Estudiante");
INSERT INTO usuarios VALUES("62","psicologia","123456","Maria","Fernandez","123","0","Vistas/img/Usuarios/U-181.jpg","Docente");
INSERT INTO usuarios VALUES("66","sistema","123","Sistemas","Andia","9087","11","","Estudiante");
INSERT INTO usuarios VALUES("69","SIS123456","123","Angelo","Andia","123456","0","Vistas/img/Usuarios/U-738.jpg","Docente");
INSERT INTO usuarios VALUES("70","123","123","admin","123","123","83","","Estudiante");



DROP TABLE IF EXISTS usuarios_comunidad;

CREATE TABLE `usuarios_comunidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `fecha_registro` date NOT NULL,
  `foto` varchar(255) NOT NULL,
  `num_posts` int(11) NOT NULL,
  `num_likes` int(11) NOT NULL,
  `cerrar_usuario` varchar(3) NOT NULL,
  `array_amigo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

INSERT INTO usuarios_comunidad VALUES("33","Sergio","Andia","sergio_andia","Sergioandia11@gmail.com","722fdf20a12c48a905bfb0667eae694e","2020-11-14","recursos/img/usuarios/sergio_andiad42d165a3d2a749ddae1028c2aa6dee0n.jpeg","24","8","no",",laiz_andia,grover_andia,rodrigo_andia,");
INSERT INTO usuarios_comunidad VALUES("34","Grover","Andia","grover_andia","Grover@gmail.com","722fdf20a12c48a905bfb0667eae694e","2020-11-14","recursos/img/usuarios/grover_andia4a76232a5b466836c52bfd00879cc410n.jpeg","7","7","no",",laiz_andia,sergio_andia,rodrigo_andia,");
INSERT INTO usuarios_comunidad VALUES("35","Laiz","Andia","laiz_andia","Laiz@gmail.com","722fdf20a12c48a905bfb0667eae694e","2020-11-14","recursos/img/usuarios/default/head_deep_blue.png","1","0","no",",sergio_andia,grover_andia,");
INSERT INTO usuarios_comunidad VALUES("36","Angelo","Andia","angelo_andia","Angelo@gmail.com","722fdf20a12c48a905bfb0667eae694e","2020-11-14","recursos/img/usuarios/default/head_deep_blue.png","0","0","no",",");
INSERT INTO usuarios_comunidad VALUES("37","Rodrigo","Andia","rodrigo_andia","Rodrigo@gmail.com","722fdf20a12c48a905bfb0667eae694e","2020-11-17","recursos/img/usuarios/rodrigo_andiaef8856ffceeef2ae6708ca214d34f946n.jpeg","12","2","no",",grover_andia,sergio_andia,");



SET FOREIGN_KEY_CHECKS=1;